﻿Willkommen zu Kardunias!

Man kann dieses Spiel mit Python und einem Terminal spielen! 
Zudem benötigt man das Paket pandas, das man über die Eingabeaufforderung unter Windows oder über das Terminal in Mac/Linux über den Befehl 'pip install pandas' installieren kann.
   
Um das Spiel zu starten, öffne die Eingabeaufforderung/Terminal und rufe dort 'kardunias.py' über den Befehl:
<python[3] kardunias.py> 
auf.

Stelle bitte sicher, dass das Spielfenster groß genug ist, sodass der Banner zu Beginn des Spiels richtig angezeigt werden kann, und somit auch alles Weitere genug Platz hat.
Viel Vergnügen mit Kardunias!

# Für Entwickler #
Mit den Kommandozeilenoptionen `--debug` und `-v` können die entsprechenden Log-Level aktiviert werden.
