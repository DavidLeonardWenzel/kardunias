#!/usr/bin/env python
# -*- coding: utf-8 -*-
import source.misc as misc
import re


def intro_sequence(fast):
    #Das Intro wird immer vor Beginn des eigentlichen Spiels aufgerufen und ist sozusagen die "login" Seite, in der der Nutzername abgefragt wird
    #Das Intro startet zudem eine kleine "Cutscene"
    #Es muss mit dem Parameter "fast" aufgerufen werden, dieser kriegt einen Bool, ob es schnell ablaufen soll (True: zum debuggen und entwickeln) oder normal
    if fast == False:
        #lässt das Terminal schnell 20 Zeilen hochscrollen, soll einen Übergangseffekt darstellen
        #siehe source/misc.py für eine Erklärung der "countdown" Funktion
        misc.countdown(20, rise=False, fastrise=True)
    scoop_heaven = """
                     (`- ).             
                     (     ).                      
                   _(       '` .             
                .=(`(      .   )         
               ((    (..__.:'-'                      
               `(       ) )                     
                ` __.:'    )                                                           
                        --'                                                 .-~~~-.      
                                                                    .- ~ ~-(       )_ _        
                                                                   /                     ~ -.      
                                      _                 _         |                           \     
                                     /*\               /*\        \                         .'      
                                    /~**\             /**~\        ~- . _____________ . -~      
                                   /M\***\   /\__/\  /***/M\          
                                  /MM/\***\ /      \/***/\MM\        
                                  \_/  \**o( O O   )o*/  \_/                                      
                                            \_____/                     
                                             o    o"""
    banner = """
        ##:::'##::::'###::::'########::'########::'##::::'##:'##::: ##:'####::::'###:::::'######::
        ##::'##::::'## ##::: ##.... ##: ##.... ##: ##:::: ##: ###:: ##:. ##::::'## ##:::'##... ##:
        ##:'##::::'##:. ##:: ##:::: ##: ##:::: ##: ##:::: ##: ####: ##:: ##:::'##:. ##:: ##:::..::
        #####::::'##:::. ##: ########:: ##:::: ##: ##:::: ##: ## ## ##:: ##::'##:::. ##:. ######::
        ##. ##::: #########: ##.. ##::: ##:::: ##: ##:::: ##: ##. ####:: ##:: #########::..... ##:
        ##:. ##:: ##.... ##: ##::. ##:: ##:::: ##: ##:::: ##: ##:. ###:: ##:: ##.... ##:'##::: ##:
        ##::. ##: ##:::: ##: ##:::. ##: ########::. #######:: ##::. ##:'####: ##:::: ##:. ######::
        ..::::..::..:::::..::..:::::..::........::::.......:::..::::..::....::..:::::..:::......:::"""
    if fast == False:
        print("\n\t\tWillkommen in der Welt von ...")
        misc.countdown(1, rise=True)
        print(scoop_heaven)
        misc.countdown(1, rise=True)
        print(banner)
        misc.countdown(2, rise=True)
        misc.write(u"\nEin Text-Adventure von David L. Wenzel")
        misc.countdown(7, fastrise=True)
        misc.write(u"Wie funktioniert dieses Spiel?\nTippe einfach, z.B. in Ich-Form, was du machen möchtest. Das ist alles. \nWenn du mal nicht weiterkommst, tippe Hilfe. Viel Vergnügen mit Kardunias!")
        misc.countdown(10, fastrise=True)
    return name_check()

def name_check():
    print(u"Wie lautet dein Spielername?\t(Wenn du bereits einen Spielstand hast, gib denselben Namen an, um den Speicherstand zu laden!)")
    namechoice=input()
    if not re.match("^[A-Za-z]*$", namechoice):
        #Dieser regex abgleich verhindert ungewollte Zeichen. Da ein file mit Nutzernamen angelegt wird, sollte dieser möglichst einheitlich sein
        print(u"Bitte einen gültigen Namen angeben! (Nur A-Z, a-z und keine Leerzeichen, keine Umlaute, Zahlen oder Sonderzeichen erlaubt)")
        return name_check()
    elif namechoice == "":
        return name_check()
        #bei ungültiger Namensangabe wiederhole die Funktion:
    elif namechoice.lower() in ["aufstehen", "norden", u"süden", "speichern", "ja", "nein", "osten", "westen", "fenster", "berater", "sesam", "rechts", "links", "oben", "unten"]:
        print(u"Der Name ist leider nicht möglich!")
        #wenn man sich nach bestimmten keywords benennt, kann man das Spiel nicht spielen weil man bspw. nicht aufstehen kann (überladung wichtiger keywords)
        return name_check()
    else: 
        namechoice=str(namechoice.strip())
        namechoice=namechoice[0].upper()+namechoice[1:].lower()
        #Nach dem der Name eingegeben wurde, werden spaces entfernt und alles außer dem ersten Buchstabe kleingeschrieben
        #So werden unregelmäßige Eingaben (Peter/peter) vereinheitlicht. Der Spieler kriegt nochmal eine Chance, sich für oder gegen den Nicknamen zu entscheiden
        name_ok = misc.yes_no(u"{x}, bist du dir sicher? (Ja oder Nein)".format(x=namechoice))
        if name_ok == False:
            return name_check()
    misc.countdown(20, fastrise=True)
    return namechoice