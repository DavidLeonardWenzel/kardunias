#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import sys

def typo(word): 
    #typo wird jeweils mit einem Wort aufgerufen, über welches dann iteriert wird, indem zB jeder Buchstabe mit denen ausgetauscht wird, die auf einer Tastatur in der nähe liegen
    #es soll eine Art automatische Schreibkorrektur sein
    #Diese Funktion soll also aus vielleicht falsch geschrieben Wörtern eine Liste an Wörtern erzeugt werden, sodass hoffentlich das richtig geschrieben dabei ist
    #Das funktioniert nur, da schon durch die keyword list bekannt ist, was legale Wörter sind

    schreibweisen=[]
    #Zunächst handelt es sich vielleicht nur um ein # , das ausversehen mit enter gedrückt würde:
    schreibweisen.append(word[:len(word)-1])
    #Im Grunde gibt es 4 Typen von Typos: 
    #Buchstaben vertauscht, daneben getippt, ausgelassen oder einer zu viel. 
    #Für jeden Typ erstelle ich Schreibweisen des Wortes, zuerst: "daneben getippt"-Möglichkeiten
    near_a = ["q", "w", "s", "y", "x"]
    near_b = ["v", "n", "g", "h"]
    near_c = ["x", "d", "f", "v"]
    near_d = ["s", "x", "c", "f", "r", "e"]
    near_e = ["w", "s", "d", "f", "r"]
    near_f = ["e", "d", "c", "v", "g", "t", "r"]
    near_g = ["r", "f", "v", "b", "h", "z", "t"]
    near_h = ["g", "b", "n", "j", "u", "z", "t"]
    near_i = ["u", "j", "k", "l", "o"]
    near_j = ["z", "h", "n", "m", "k", "i", "u"]
    near_k = ["u", "j", "m", ",", "l" "o", "i"]
    near_l = ["i", "k", "o", "p"]
    near_m = ["j", "n", "k"]
    near_n = ["h", "j", "k", "m","b"] 
    near_o = ["i", "k", "l", "p"]
    near_p = ["o","l"]
    near_q = ["w", "s", "a"]
    near_r = ["e", "d", "f", "g", "t"]
    near_s = ["w", "a", "y", "x", "d", "e"]
    near_t = ["r", "f", "g", "h", "z"]
    near_u = ["z", "h", "j", "k", "i"]
    near_v = ["f", "c", "b","g"]
    near_w = ["q", "a", "s", "d", "e"]
    near_x = ["s", "y", "c", "d"]
    near_y = ["a", "s", "x",]
    near_z = ["t", "g", "h", "u"]
    near_1 = ["q", "w"]
    near_2 = ["q","w", "e"]
    near_3 = ["w", "e", "r"]
    near_4 = ["e", "r"]
    near_5 = ["r", "t"]
    near_6 = ["t", "z"]
    near_7 = ["z", "u"]
    near_8 = ["u", "i"]
    near_9 = ["i", "o"]
    near_0 = ["o", "p"]
    #Hier sind wahrscheinlich gemeinte Buchstaben, welche in der Nähe auf Tastaturen liegen
    
    #Als nächstes sollen auch Buchstabendreher erzeugt werden, um falsche Dreher wieder gerade zu drehen:
    cur = 0
    #Zähler für den derzeitigen Buchstaben
    for letter in word:
        if len(word) >= 2: #bei kurzen Worten macht drehen wenig Sinn
            if cur == 0: 
                schreibweisen.append(word[cur+1]+word[cur]+word[cur+2:])
            elif cur < len(word)-1:
                schreibweisen.append(word[:cur]+word[cur+1]+word[cur]+word[cur+2:])
        #Nun habe ich:
        #hey -> ehy, hye    
        #ahllo -> hallo (und noch viel mehr, aber wir suchen hallo)
        
        #die fünf häufigsten Buchstaben im Deutschen laut Wikipedia an jeder stelle hinzufügen, falls diese vergessen wurden:
        schreibweisen.append(word[:cur]+"e"+word[cur:])
        schreibweisen.append(word[:cur]+"n"+word[cur:])
        schreibweisen.append(word[:cur]+"i"+word[cur:])
        schreibweisen.append(word[:cur]+"s"+word[cur:])
        schreibweisen.append(word[:cur]+"r"+word[cur:])
        
        #simpler plural ganz hinten:
        schreibweisen.append(word+"n")

        #an jeder stelle einen buchstaben wegnehmen, falls man auf 2 tasten ausversehen kam:
        schreibweisen.append(word[:cur]+word[cur+1:])

        #Als nächstes noch die Buchstaben mit denen in der Keyboad Umgebung ersetzen:
        if letter == "a":
            for letter in near_a:
                schreibweisen.append(word[:cur]+letter+word[cur+1:])
        elif letter == "b":
            for letter in near_b:
                schreibweisen.append(word[:cur]+letter+word[cur+1:])
        elif letter == "c":
            for letter in near_c:
                schreibweisen.append(word[:cur]+letter+word[cur+1:])
        elif letter == "d":
            for letter in near_d:
                schreibweisen.append(word[:cur]+letter+word[cur+1:])
        elif letter == "e":
            for letter in near_e:
                schreibweisen.append(word[:cur]+letter+word[cur+1:])
        elif letter == "f":
            for letter in near_f:
                schreibweisen.append(word[:cur]+letter+word[cur+1:])
        elif letter == "g":
            for letter in near_g:
                schreibweisen.append(word[:cur]+letter+word[cur+1:])
        elif letter == "h":
            for letter in near_h:
                schreibweisen.append(word[:cur]+letter+word[cur+1:])
        elif letter == "i":
            for letter in near_i:
                schreibweisen.append(word[:cur]+letter+word[cur+1:])
        elif letter == "j":
            for letter in near_j:
                schreibweisen.append(word[:cur]+letter+word[cur+1:])
        elif letter == "k":
            for letter in near_k:
                schreibweisen.append(word[:cur]+letter+word[cur+1:])
        elif letter == "l":
            for letter in near_l:
                schreibweisen.append(word[:cur]+letter+word[cur+1:])
        elif letter == "m":
            for letter in near_m:
                schreibweisen.append(word[:cur]+letter+word[cur+1:])
        elif letter == "n":
            for letter in near_n:
                schreibweisen.append(word[:cur]+letter+word[cur+1:])
        elif letter == "o":
            for letter in near_o:
                schreibweisen.append(word[:cur]+letter+word[cur+1:])
        elif letter == "p":
            for letter in near_p:
                schreibweisen.append(word[:cur]+letter+word[cur+1:])
        elif letter == "q":
            for letter in near_q:
                schreibweisen.append(word[:cur]+letter+word[cur+1:])
        elif letter == "r":
            for letter in near_r:
                schreibweisen.append(word[:cur]+letter+word[cur+1:])
        elif letter == "s":
            for letter in near_s:
                schreibweisen.append(word[:cur]+letter+word[cur+1:])
        elif letter == "t":
            for letter in near_t:
                schreibweisen.append(word[:cur]+letter+word[cur+1:])
        elif letter == "u":
            for letter in near_u:
                schreibweisen.append(word[:cur]+letter+word[cur+1:])
        elif letter == "v":
            for letter in near_v:
                schreibweisen.append(word[:cur]+letter+word[cur+1:])
        elif letter == "w":
            for letter in near_w:
                schreibweisen.append(word[:cur]+letter+word[cur+1:])
        elif letter == "x":
            for letter in near_x:
                schreibweisen.append(word[:cur]+letter+word[cur+1:])
        elif letter == "y":
            for letter in near_y:
                schreibweisen.append(word[:cur]+letter+word[cur+1:])
        elif letter == "z":
            for letter in near_z:
                schreibweisen.append(word[:cur]+letter+word[cur+1:])
        elif letter == "1":
            for letter in near_1:
                schreibweisen.append(word[:cur]+letter+word[cur+1:])
        elif letter == "2":
            for letter in near_2:
                schreibweisen.append(word[:cur]+letter+word[cur+1:])
        elif letter == "3":
            for letter in near_3:
                schreibweisen.append(word[:cur]+letter+word[cur+1:])
        elif letter == "4":
            for letter in near_4:
                schreibweisen.append(word[:cur]+letter+word[cur+1:])
        elif letter == "5":
            for letter in near_5:
                schreibweisen.append(word[:cur]+letter+word[cur+1:])
        elif letter == "6":
            for letter in near_6:
                schreibweisen.append(word[:cur]+letter+word[cur+1:])
        elif letter == "7":
            for letter in near_7:
                schreibweisen.append(word[:cur]+letter+word[cur+1:])
        elif letter == "8":
            for letter in near_8:
                schreibweisen.append(word[:cur]+letter+word[cur+1:])
        elif letter == "9":
            for letter in near_9:
                schreibweisen.append(word[:cur]+letter+word[cur+1:])
        cur +=1

    return schreibweisen #unten ist ein Beispiel des schreibweisen return werts 


def check_input(user_input, goals):
    #nimmt Nutzereingaben und die Ziellisten 
    #Userinput ist was der spieler schreibt, Goals ist eine Liste an legalen Worten, welche gemeint sein dürfen 
    #Damit es egal ist, ob die Worte aus Goals groß- oder kleingeschrieben übergeben wurden, konvertieren wir alle Worte in goals ins kleingeschriebene
    small_goals = [elem.lower() for elem in goals]
    treffer = []
    user_input = user_input.strip()
    small_umlaut_input = re.sub('Ö','ö',user_input)
    small_umlaut_input = re.sub('Ä','ä',small_umlaut_input) 
    small_umlaut_input = re.sub('Ü','ü',small_umlaut_input) 
    small_umlaut_input = re.sub('ß','ss',small_umlaut_input)
    small_umlaut_input = re.sub("@", "at", small_umlaut_input)
    small_umlaut_input = small_umlaut_input.lower()
    #lower() hatte Probleme mit Sonderzeichen und Umlaute, daher werden diese "manuell" ersetzt
    #Für den Abgleich will ich keine @, Großbuchstaben und Spaces/Newlines.
    #Das macht es einfacher, die Ziellisten an zu akzeptierenden Wörtern möglichst kurz zu halten
    if " " in small_umlaut_input: #Falls es also mehrere Wörter sind
        input_list = small_umlaut_input.split() #aufteilen der einzelnen Wörter, für jedes einzelne wird typos aufgerufen werden
        for wort in input_list:
            if wort in small_goals: #wenn es sogar direkt richtig geschrieben wurde, brauchen wir typos nicht, und können direkt den legalen Treffer hinzufügen
                treffer.append(wort)
            else:
                typo_try=typo(wort)
                for elem in typo_try:
                    if elem in small_goals:
                        treffer.append(elem)
    else: #Ein Wort, selbe Logik nur ohne Iteration
        if small_umlaut_input in small_goals:
            treffer.append(small_umlaut_input)
        else:
            typo_try = typo(small_umlaut_input)
            for elem in typo_try:
                if elem in small_goals:
                    treffer.append(elem)
    treffer = set(treffer) #falls mehrere "korrigierte schreibweisen" dasselbe richtige Ergebnis erzielen, lösche ich hier doppelte ergebnisse
    treffer = list(treffer)
    return treffer
    #Beispiel des return-Werts von typo, aufgerufen mit: asuversehen
    #['asuversehe', 'sauversehen', 'easuversehen', 'nasuversehen', 'iasuversehen', 'sasuversehen', 'rasuversehen', 'suversehen', 
    #'qsuversehen', 'wsuversehen', 'ssuversehen', 'ysuversehen', 'xsuversehen', 'ausversehen', 'aesuversehen', 'ansuversehen', 
    #'aisuversehen', 'assuversehen', 'arsuversehen', 'auversehen', 'awuversehen', 'aauversehen', 'ayuversehen', 'axuversehen', 
    #'aduversehen', 'aeuversehen', 'asvuersehen', 'aseuversehen', 'asnuversehen', 'asiuversehen', 'assuversehen', 'asruversehen', 
    #'asversehen', 'aszversehen', 'ashversehen', 'asjversehen', 'askversehen', 'asiversehen', 'asuevrsehen', 'asueversehen', 
    #'asunversehen', 'asuiversehen', 'asusversehen', 'asurversehen','asuersehen', 'asufersehen', 'asucersehen', 'asubersehen',
    #'asugersehen', 'asuvresehen', 'asuveersehen', 'asuvnersehen', 'asuviersehen', 'asuvsersehen', 'asuvrersehen', 'asuvrsehen', 
    #'asuvwrsehen', 'asuvsrsehen', 'asuvdrsehen', 'asuvfrsehen', 'asuvrrsehen', 'asuvesrehen', 'asuveersehen', 'asuvenrsehen', 
    #'asuveirsehen', 'asuvesrsehen', 'asuverrsehen', 'asuvesehen', 'asuveesehen', 'asuvedsehen', 'asuvefsehen','asuvegsehen', 
    #'asuvetsehen', 'asuvereshen', 'asuveresehen', 'asuvernsehen', 'asuverisehen', 'asuverssehen', 'asuverrsehen', 'asuverehen',
    #'asuverwehen', 'asuveraehen', 'asuveryehen', 'asuverxehen', 'asuverdehen', 'asuvereehen', 'asuversheen', 'asuverseehen', 
    #'asuversnehen', 'asuversiehen','asuverssehen', 'asuversrehen', 'asuvershen', 'asuverswhen', 'asuversshen', 'asuversdhen', 
    #'asuversfhen', 'asuversrhen', 'asuverseehn', 'asuverseehen', 'asuversenhen', 'asuverseihen', 'asuverseshen', 'asuverserhen', 
    #'asuverseen', 'asuversegen', 'asuverseben', 'asuversenen', 'asuversejen', 'asuverseuen','asuversezen', 'asuverseten', 
    #'asuversehne', 'asuverseheen', 'asuversehnen', 'asuversehien', 'asuversehsen', 'asuversehren', 'asuversehn', 'asuversehwn', 
    #'asuversehsn', 'asuversehdn', 'asuversehfn', 'asuversehrn', 'asuverseheen', 'asuversehenn', 'asuversehein', 'asuversehesn', 
    #'asuversehern', 'asuversehe','asuverseheh', 'asuversehej', 'asuversehek', 'asuversehem', 'asuverseheb']

    #Man erkennt, das "ausversehen" korrigiert wurde und returned wird, wenn es in goals ist (da muss es von mir natürlich richtig geschrieben übergeben worden sein)