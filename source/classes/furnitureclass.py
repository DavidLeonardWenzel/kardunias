#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# furnitureclass.py

# Die furniture class soll die Logik der Türen, Truhen und mehr ermöglichen, in dem diese übergeben, womit sie angesprochen werden können und was durch sie nicht angesprochen werden kann
# Diese .py beinhaltet zusätzlich alle Texte, die von den Furnitureobjekten geprintet werden können, wenn mit ihnen interagiert wird

from source.dev_tools import log
from source.classes.roomclass import Room
from source.classes.playerclass import Player
from .. import misc
from .. import type_parser as parse
import random
import sys

class Furniture():
    def __init__(self, myname, myplayer):    
        self.name = myname
        self.player = myplayer
        self.furn_keywords = []
        self.subtract = []          #Enthält die Liste der Wörter, die von der engine keyword liste entfernt werden        

    def compute_keywords(self):
        #Diese Funktion wird beim laden der Furnitures (also allen NPCs und Gegenständen die in Räumen stehen und interagierbar sein sollen) aufgerufen
        #Für die einzelnen Gegenstände kann mit "substract" entfernt werden, was nicht möglich sein soll
        #Beispielsweise entfernt es die Möglichkeit, nach Norden zu gehen solange die Starttür noch abgeschlossen ist, und ersetzt es mit eigenen keywords
        #die eigenen "furn_keywords" pro Objekt werden genutzt, um, falls es vom Nutzer mit einem dieser Worte aufgerufen wird, 
        #sich auch "angesprochen fühlt", und handle_input weiß, es handelt sich um Furniture
        
        ###Schlafzimmer des Königs      
        #Dämontür
        if self.name == u"daemontuer":    
            if self.player.events["daemontuer_offen"] == False:
                self.subtract = [u"norden", u"nördlich", u"oben", u"vorne"]
                self.furn_keywords = [u"dämontür", u"dämonentür", u"tür", u"dämon", u"maske", u"norden", u"nördlich", u"sicherheitstür", "oben", "vorne", u"vorwärts"]    
            else:
                self.subtract = []
                self.furn_keywords = [u"dämontür", u"dämonentür", u"tür", u"dämon", u"maske", u"sicherheitstür", u"vorwärts"]    
        #Truhe unterm Bett 
        elif self.name == u"truhe_62":  
            if self.player.events["truhe_gesichtet"] == False:
                #Dass unter dem Bett eine Truhe ist, soll man per "umschauen" herausfinden
                self.furn_keywords = []
            else:
                self.furn_keywords = [u"truhe", u"schatz", u"schatztruhe", u"kiste", u"schatzkiste"]
        #Kleiderschrank
        elif self.name == u"kleiderschrank":    
            self.furn_keywords = [u"kleiderschrank", u"schrank", u"notiz", u"anziehen"] 
        #Fenster
        elif self.name == u"fenster":
            self.furn_keywords = [u"fenster",u"rausschauen",u"herausschauen", u"aussicht", u"hinausschauen", u"rausgucken", u"herantreten"]
        #Bett
        elif self.name == u"bett":
            if self.player.events["ist_aufgestanden"] == False:
                self.subtract = [u"norden", u"fenster",u"rausschauen",u"herausschauen", u"aussicht", u"hinausschauen", u"rausgucken", u"herantreten", u"dämontür", u"dämonentür", u"tür", u"dämon", u"maske",u"kleiderschrank", u"schrank",u"sicherheitstür", u"oben", u"vorne", u"hoch", u"nördlich",u"vorwärts", u"notiz", u"anziehen"]
                self.furn_keywords = [u"schlafen", u"weiterschlafen", u"aufstehen", u"licht", u"heraus", u"raus", u"stehe", u"auf", u"diener","klingeln","rufen","wache","bardin","untergebene",u"brülle","frage"]
            else:
                self.substract = []
                self.furn_keywords = [u"bett", u"himmelsbett", u"schlafen", u"legen", u"hinlegen", u"hineinlegen"]
        ###Westflügel
        elif self.name == u"steintuer":
            if self.player.events["steintuer_offen"] == False:
                self.subtract = [u"norden", u"nördlich", u"oben", u"vorne"]
                self.furn_keywords = [u"steintür", u"tür", u"beten", u"norden", u"nördlich", u"vorne", "oben", "hoch", "treppenhaus", "bibliothek"]
            else:
                self.subtract = []
                self.furn_keywords = [u"steintür", u"tür"]        
        elif self.name==u"npc_beratertür":
            if self.player.events["gab_hummus"] == False:
            #der berater hat seine Tür abgeschlossen, weswegen die Möglichkeit dorthin zu gehen entfernt wird
                self.subtract=[u"süden", u"südlich", u"s", u"unten", u"hinten"]
                self.furn_keywords = [u"berater", u"süden", u"südlich", u"unten", u"hinten", u"nabu", "kammer", u"kämmerchen"]         
        elif self.name==u"kueche":
            self.furn_keywords=[u"küche"]
        #Küche
        elif self.name == u"schrank": 
            self.furn_keywords = [u"schrank", u"öffnen", "koch"]
        elif self.name == u"anrichte":
            self.furn_keywords = [u"anrichte", u"hummus", u"nehmen"]
        #nabus raum
        elif self.name=="truhe_52":
            self.furn_keywords=[u"truhe", u"schatz", u"schatztruhe", u"kiste", u"schatzkiste"]
        elif self.name=="npc_berater":
            if self.player.events["nabu_vertrieben"] == False:
                self.furn_keywords=[u"nabu", "berater"]
            else:
                self.furn_keywords=[]
        elif self.name == "bett":
            self.furn_keywords = [u"bett", u"schlafen", u"legen", u"hinlegen", u"hineinlegen"]
       #die Bücherei:
        elif self.name == "buecherschrank_move_a":
            #diese schränke sollen bestimmte Wege blockieren, bis ein Schalter betätigt wird
            if self.player.events["schalter_66_flipped"] == False:
                self.subtract = [u"westen", u"westlich", u"links"]
                self.furn_keywords=[u"geräusche", "lauschen", u"hören", u"hinhören", "westen", "links"]
            else: 
                self.furn_keywords=[]

        elif self.name == "buecherschrank_move_b":
            if self.player.events["schalter_46_flipped"]==False:
               self.subtract = [u"süden",u"südlich", u"unten", u"hinten"]
               self.furn_keywords=[]
            else: 
                self.furn_keywords=[]
        #die beiden Schalter, welche das Bücherschrank "move event" auslösen:
        elif self.name == "schalter_66" or self.name =="schalter_46":
            self.furn_keywords = ["schalter", "ziehen", "mechanismus", "schaltermechanismus", "unterste", u"drücken", "hebel"]

        #die drei Truhen in der bibliothek:
        #übernimmt auch für eine Truhe in map 2 das hinzufügen der relevanten keywords, da sie den gleichen Namen trägt 
        elif self.name == "truhe_33" or self.name == "truhe_37" or self.name == "truhe_66":
            self.furn_keywords=[u"truhe", u"schatz", u"schatztruhe", u"kiste", u"schatzkiste"]
        
        #npc, der den weg nach süden blockiert:
        elif self.name == "wache":
            if self.player.events["spoken_to_blockway"] == False:
                self.subtract = [u"süden",u"südlich", u"unten", u"hinten"]
                self.furn_keywords = ["wache", "typ", "kerl", "spreche", "ansprechen", "reden", "unterhalten", u"süden", u"südlich", "unten"]
            else: #falls man schonmal geredet hat, ist Süden durch das normale Movement erlaubt, man kann die Wache weiterhin normal ansprechen
                self.furn_keywords = ["wache", "typ", "kerl", "spreche", "ansprechen", "reden", "unterhalten"]
       #Gesamte Bücherei
        elif self.name == "buecherschrank":
            self.furn_keywords=[u"bücherschrank" , u"buch", u"bücher", u"schrank" ,"lesen", u"regal", u"bücherwand", u"bücherschränke"]
        
        #Auf der zweiten Karte, Map2: Das Banquet
        elif self.name == "banketttisch":
            self.furn_keywords=["tisch", "bankett", "banket", "essen", "probieren", "banquet", "festessen", "essenstisch", "hinsetzen", "erholen"]
        #Die Wache, die wegrennt in x4y6
        elif self.name == "npc_verwirrte_wache":
            if self.player.events["wache_weg"]==False:
                self.furn_keywords=["wache", "typ", "kerl", "spreche", "ansprechen", "reden", "unterhalten"]
            else:
                #da sie nach dem Reden verschwindet, soll man die Wache nicht anreden können, wenn daa event True ist, daher hier eine leere liste
                self.furn_keywords=[]
        #x1y3, der NPC weibliche Wache, zusätzlich die Truhe, eine Vase und ein Tisch
        elif self.name == "npc_weibliche_wache":
            self.furn_keywords = ["wache","spreche","ansprechen", "reden", "unterhalten", "dame", "frau"]
        elif self.name == "vase":
            self.furn_keywords = ["vase", "blume", "blumenvase", "tisch", "nachttisch"]
        elif self.name == "kerze":
            self.furn_keywords = ["kerze", "halter", "kerzenhalter"]
        elif self.name == "truhe_13":
            if self.player.events["truhe_13_gesichtet"] == False:
                #Dass unter dem Bett eine Truhe ist, soll man per "umschauen" herausfinden
                self.furn_keywords = []
            else:
                self.furn_keywords = [u"truhe", u"schatz", u"schatztruhe", u"kiste", u"schatzkiste"]
        elif self.name== "bett":
            self.furn_keywords= [u"bett", u"schlafen", u"legen", u"hinlegen", u"hineinlegen"]
        #der Waffenhalter in x3y3
        elif self.name =="waffenhalter":
            self.furn_keywords=["waffenhalter", "halter", "nehmen", "hinter", "dahinter"]
        #die Leiter in x5y3 und x6y3:
        elif self.name=="leiterl":
        #Nur falls der Schalter noch nicht gedrückt wurde, sieht man die Leiter in diesem Raum, daher passiert je nach Event etwas anderes
            if self.player.events["schalter_15_flipped"]== False:
                #hoch ist normalerweise immer auch movement nach Norden, was an dieser stelle entfernt werden soll, da man die Leiter evtl. 'hoch' klettern möchte
                self.subtract=["hoch"]
                self.furn_keywords=["leiter", "schienen", "boden", "klettern", "hoch", "hochgehen", "schieben", "verschieben", u"drücken", "ziehen"]
            else:
                #wenn der Schalter "geflipped" wurde, sieht man hier nur noch die Schienen, daher:
                self.furn_keywords=["schienen", "boden"]
        elif self.name=="decke":
            self.subtract=["oben", "schaue", "gucke"]
            self.furn_keywords=["decke", "oben", "schaue", "gucke"]
        elif self.name == "leiterr":
            #falls die Leiter noch nicht per Schalter umgelegt wurde, sieht man hier nur Schienen
            if self.player.events["schalter_15_flipped"] == False:
                self.furn_keywords=["schienen", "boden"]
            elif self.player.events["schalter_15_flipped"] == True:
            #falls die Leiter per Schalter bewegt wurde, kann man sie von hier aus sehen und hoch klettern
                self.subtract=["hoch"]
                self.furn_keywords=["leiter", "schienen", "boden", "klettern", "hoch", "hochgehen", "schieben", "verschieben", u"drücken", "ziehen"]
        elif self.name=="schild":
            self.furn_keywords = ["schild", "lesen"]
        #die Wachkammer mit Schalter und der fanatischen Wache in x1y5
        elif self.name =="schalter_15":
            self.furn_keywords= ["schalter", "ziehen", "mechanismus", "schaltermechanismus", u"drücken", "hebel"]
        elif self.name =="npc_fanatische_wache":
            self.furn_keywords=["wache", "typ", "kerl", "spreche", "ansprechen", "reden", "unterhalten"]
        #die seltsame wache in x7y5     
        elif self.name=="npc_bayer":
            self.furn_keywords=["wache", "typ", "kerl", "spreche", "ansprechen", "reden", "unterhalten"]
        #überall Spinnweben
        elif self.name=="spinnweben":
            self.furn_keywords= ["spinnweben"]
        #die letzte Barriere im Spiel:
        elif self.name =="barriere":
            #Die Barriere soll verhindern, dass man nach Süden gehen kann, bis ein Passwort genannt wird
            if self.player.events["barriere_down"] == False:
                self.subtract=[u"süden",u"südlich", u"unten", u"hinten"]
                self.furn_keywords=[u"süden",u"südlich", u"unten", u"hinten", "barriere", "lila", "dunkel", "gesicht", "verzerrt", "sprechen", "ansprechen", "unterhalten", "hallo"]
            else:
                self.furn_keywords=[]
        #überall Türen, die keine spezifische Richtungen haben (erst in der Engine lokalisiert, in welche Richtung diese jeweils führen)
        elif self.name == u"tuer":
            self.furn_keywords=[u"tür", u"ausgangstür"]

        else:
            log(u"Didn't find a room we belong to in compute_keyword()", "error")
            log("... " + str(self.name), "error")




##################### ALLE TEXTE DER FURNITURE OBJEKTE ######################

#Schlafsaal des Königs

def text_daemontuer(phase):
    if phase == u"begin":
        misc.write(u"Während ich auf meine Sicherheitsschlafsaaltür mit der dämonischen Maske zugehe, beobachtet sie mich stetig und grinst vor sich hin.")
        misc.write(u'"Öffne die Tür, Dämon!"')
        misc.write(u"Die Maske schreckt plötzlich auf und schaut etwas konfus. Anscheinend war sie bis gerade eben mental abwesend.")
        print(u"DÄMON: Hast du was gesagt? Und falls ja, was?")
    elif phase == u"stop":
        print(u"DÄMON: Was wolltest du mir sagen? Ach, sag's mir beim nächsten Mal!")
    elif phase == u"solved":
        misc.write(u"DÄMON: OKAY! Ist ja gut, ich bin doch nicht schwerhörig... Ich öffne ja die Tür, Chef!")
        misc.write(u"Der Dämon schaut ein wenig angestrengt, kurz darauf hörst du ein leichtes Knacken. Die Tür ist aufgegangen! ")
        print(u"DÄMON: Beim nächsten Mal brüll nicht so laut! Ist ja unmöglich hier ein Nickerchen zu halten.\n")
    elif phase == u"fail":
        pos_fail=[u"DÄMON: Waaas? Was hast du gesagt?", u"DÄMON: Wie bitte? Das sollte das Passwort sein?", u"DÄMON: Wie bitte? Sollte das etwa das Passwort sein?"]
        mynum=random.randint(0,len(pos_fail)-1)
        print(pos_fail[mynum])
    elif phase == u"fail2":
        print(u"DÄMON: Sprich mal LAUTER, ich verstehe dich gar nicht! SPRICH SO LAUT WIE ICH!")
    elif phase == "sleep":
        print(u"DÄMON: Zzzzz... ich bin offen.... Zzzzz...\nIn Richtung Norden kannst du den Raum verlassen...")
    else:
        log(u"text daemontuer", "error")
                        
def text_bett(phase):
    if phase == "look":
        print(u"Mein ach so schönes Bett! Ich könnte mich glatt wieder hineinlegen.")
    elif phase == "sleep":
        print(u"Nur für ein paar Minuten die Augen zu machen...")
        misc.countdown(1.5)
        print(u"...")                
        misc.countdown(1.5)
        print(u"Ich fühle mich direkt deutlich besser! Ein König muss auf seinen Schlaf achten.")
    else:
        log(u"text bett", "error")

def text_rufen(username):
    print("{x} ruft laut!".format(x=username))
    misc.countdown(0.5)
    print("...")
    misc.countdown(0.5)
    print("Doch niemand kommt...") 
    misc.countdown(0.5)
    
def text_fenster():
    print(u"Durch mein Fenster weht eine warme Brise in mein Schlafgemach. Einige hundert Meter tief jagt ein Falke im Tal! Es scheint, als zieht ein Gewitter auf...")

def text_truhe(phase):
    if phase == u"unfound":
        misc.write(u"Mal nachschauen wieviel Gold sich in meiner Truhe befindet...")
        misc.write(u"Ach was?")
        misc.write(u"Kein einziger Pfennig?")
        misc.write(u"MEIN GANZES GOLD IST WEG!?")
        misc.write(u"Stattdessen nur diese Steinplatte??? Wer hat die denn da rein gelegt?\nHier steht drauf: \"TAL DER FALKE JAGT\"")
        misc.write(u"Ich habe den Falkenstein erhalten!\n")
    elif phase == u"found":
        print(u"Die Truhe ist noch leerer als zuvor. Wo zum Geier ist mein Geld?")
    else:
        log(u"text truhe", "error")

def text_schlafzimmer_schrank(phase):
    if phase == u"look":
        print(u"In meinem Kleiderschrank")
        misc.countdown(0.3)
        print(u" - ich öffne ihn mal komplett - ")
        misc.countdown(0.3)
        print(u"befinden sich einige Papyrusstreifen.")
        if misc.yes_no(u"Die Papyrusstreifen ansehen? (Ja oder nein?)"):
            print(u"Auf denen steht: ")
            misc.countdown(0.3)
            print(u" - ich les es mal vor - ") 
            misc.countdown(0.3)
            misc.write(u"Es lohnt sich, sich in jedem Raum einmal UMZUSCHAUEN.") 
            misc.write(u"Am besten für die Navigation durch Räume eignet sich eine Beschreibung per HIMMELSRICHTUNGEN.")
            misc.write(u"Items, die man besitzt, kann man mit dem ITEM NAMEN untersuchen.")
            misc.write(u"Man kann gefundene Items KOMBINIEREN. Einfach 'verbinden' oder 'kombinieren' eintippen.")
            misc.write(u"Man kann jederzeit SPEICHERN, das Spiel BEENDEN.")
            misc.write(u"Das Menü erscheint, indem man seinen eigenen NAMEN, MENÜ oder INVENTAR eintippt.")
            misc.write(u"Und wenn das alles nicht hilft, dann hilft es, sich aufzumalen, wo man langgeht oder 'hilfe' einzutippen!")
            misc.write(u"Bei Krankheit oder schlicht mangelnder GESUNDHEIT empfiehlt der Bader BETTruhe.")
            misc.write(u"Mehr steht nicht auf den Papyrusstreifen. Das klingt ja wie eine Anleitung für ein albernes Text-Abenteuer! Die werden doch erst in 2000 Jahren erfunden! Falls das hier eins ist, bin ich hoffentlich der Hauptcharakter.")
        else:
            print(u"Nah, eigentlich ist es mir egal, was auf den Streifen drauf steht.")
    elif phase == u"robe_und_sandalen_finden":
        misc.write(u"Glücklicherweise befinden sich meine Robe und meine Sandalen im Kleiderschrank! Ein König kann ja nicht nackig durch sein Anwesen stolzieren.")
        misc.write(u"Ich habe meine Robe und meine Sandalen angezogen!\n")
    elif phase == u"found":
        misc.write(u"Es befindet sich sonst nichts Interessantes mehr in meinem Kleiderschrank.\n")
    else:
        log(u"text schrank", "error")        
                        
#Ostflügel

def text_steinfenster(phase):
    if phase == u"first":
        misc.write(u"Ich schaue aus dem Fenster und sehe, dass es bis ins Tal tief nach unten geht und einige Wolken aufziehen. In hundert Metern Tiefe jagt ein Falke!")
        misc.write(u"... Und das Fenster muss aber mal dringend wieder saniert werden, eine einzelne Steinplatte kann man ja sogar schon herausnehmen!")
        misc.write(u"Na warte, Steinplatte, dich nehm ich erstmal mit bis ich einen Untergebenen finde, der dich wieder anbringen wird!")
        misc.write(u"Und dann hat auch noch jemand etwas in dich eingemeißelt!?")
        misc.write(u"\"WENN MORGENS IM\"?")
        misc.write(u"Wenn ich den erwische...!")
        misc.write(u"Ich habe den Morgenstein erhalten!\n")
    elif phase == u"else":
        misc.write(u"Die Angelegenheit mit der Steinplatte macht mich immer noch rasend!\n")
    else:
        print(u"ERROR: text steinfenster")

#Küche

def text_anrichte(phase):
    if phase == u"hummus":
        misc.write(u"Diesen leckeren Hummus muss ich doch sofort mitnehmen! Ein Geschmackstest mit einem meiner Finger verrät mir, dass dem Hummus noch der letzte Schliff fehlt.")
        misc.write(u"Ich habe Hummus erhalten!\n")
    elif phase == u"rezept":
        misc.write(u"""Auf dem Platz, wo vorher der Hummus stand, ist ein kleiner Papyrusschnipsel auf dem Folgendes steht: 

        Willst du eine leckere Speise,
        so verhalte dich ganz weise,
        denn willst du Hummus nach Omas Art,
        dann wird an Sesam nicht gespart
        - Oma\n""")
        print(u"Was das angeht hat Oma wohl recht, vermute ich.")
    else:
        log(u"text anrichte", "error")

def text_kueche_schrank(phase):
    if phase == "sesam":
        misc.write(u"Ich öffne den Schrank und eine würzige Geruchsexplosion durchdringt den Raum und ummantelt meine Nase.")
        misc.write(u"Die Gewürze nehmen meinen Geist mit auf eine Reise des unendlichen Genusses und für einen kurzen Moment verschmilzt die Welt mit der wohligen Duftwolke.")
        misc.write(u"Dann merke ich, dass eine Dose Sesam im Schrank liegt.")
        misc.write(u"Ich habe Sesam erhalten!\n")
    elif phase == u"else":
        print(u"Der Schrank ist jetzt zwar leer, aber immer noch wohlig duftend.")
    else:
        log(u"text schrank", "error")
                        
#Westflügel

def text_steintuer(phase):
    if phase == u"teilweise_vorhanden":
        misc.write(u"Ich versuche mal, die Steinplatte in die wie dafür vorgesehe Lücke hier zu schieben... hmm..und mit einem Ruck!")
        misc.countdown(0.5)
        misc.write(u"Von der Form her scheint die Steinplatte, die ich in den Händen halte, zu passen. Allerdings ist sie zu kurz. Vielleicht fehlt ja noch ein Stück der Steinplatte.\n")
    elif phase == u"versperrt":
        misc.write(u"Eine große Steintür versperrt den Weg zur Bibliothek. In ihr ist ein länglicher rechteckiger flacher Hohlraum an dessen Ende \"GÖTTER NAH ÜBER UNS RAGT\" steht.")
        misc.write(u"Es muss doch einen Weg geben an dieser Tür vorbei zu kommen...\n")
    elif phase == u"komplett_vorhanden":
        misc.write(u"Es sieht so aus als ob man die zusammengesetzte Steinplatte in diesen Hohlraum einsetzen könnte.")
        misc.write(u"...")
        misc.countdown(0.5)
        misc.write(u"""Voilà! Zusammengesetzt ergibt der Satz:

                WENN MORGENS IM TAL DER FALKE JAGT, DAS ZEICHEN DER GÖTTER NAH ÜBER UNS RAGT\n""")
        misc.write(u"Unter großem Knarzen geht die Tür auf! Endlich kann's weiter gehen!")
    elif phase == u"offen":
        print(u"Die Steintür ist offen. Mir steht nichts mehr im Weg! Außer alles war vor mir liegt.")
    else:
        log(u"text steintuer", "error")

def text_nabu(username):
    misc.write(u"Der Berater schließt die Tür auf und nimmt das Essen mit dankendem Blick entgegen.")
    misc.write(u"{x} geht derweil in die Kammer Nabus.".format(x=username))

#Nabus Kammer:
def text_truhe_52(phase):
    if phase == "take":
        print("Ha! Er hat eine dieser seltsamen Steinplatten! Hier steht drauf: \"DAS ZEICHEN DER\"... Nabu guckt gerade nicht...")
        steal = False
        while steal == False:
            #man muss die platte hier einfach mitnehmen
            steal = misc.yes_no("Die Steinplatte mitnehmen?")
            if steal == True:
                print("Besser mitnehmen.")
            if steal == False:
                print("Jetzt keine falsche Bescheidenheit...")
        misc.write("Ich habe den Zeichenstein erhalten!")
    elif phase == "empty":
        print("Mehr hat Nabu wohl nicht.")

#die ganze Library:
def text_buecherschrank(username):
    book_texts = [
        u"Bewegen kann man diese Schränke ja nicht! Ich schau mir mal ein Buch an... \n’Lokale Echsen - ein Wunder der Natur! Kapitel 1 ...\n Afrikanische Dornschwanzagame. Familie: Agamidae. Größe: Bis zu 41 cm. Der Körper dieser Unterart ist stämmig gebaut.\nDie Grundfarbe schwankt zwischen grau und braun mit dunkler Marmorierung.'\n{x}:\t\"Spannend! Ich hätte doch Biologe und nicht König werden sollen.\"".format(x=username),
        u"Bewegen kann man diese Schränke ja nicht! Ich schau mir mal ein Buch an... \n’Beliebte Geckos - Spezialisten oder Generalisten? Kapitel 2 ...\n Helmkopfgecko. Familie: Gekkonidae. Größe: Gesamtlänge bis 10 cm.\n Die Grundfarbe der Tiere ist beigebraun, wobei helle und dunkle Flecken im Längsverlauf erkennbar sind. \nDie Fettreserven lassen diese Tiere oft fettbäuchig erscheinen’.\n\"Spannend! Ich hätte doch Biologe und nicht König werden sollen.\"".format(x=username),
        u"Bewegen kann man diese Schränke ja nicht! Ich schau mir mal ein Buch an... \n’Allgemeine Zoologie - Kapiel 41 ... \nDie so außerordentliche Organisation des Tierreichs ist wohl eine Folge einer glücklichen Verkettung von Zufällen...\n...und sich somit die Fähigkeit zu mannigfacher höherer Entwicklung bewahrt hat.’\n{x}:\t\"Spannend! Ich hätte doch Biologe und nicht König werden sollen.\" ".format(x=username),
        u"Bewegen kann man diese Schränke ja nicht! Ich schau mir mal ein Buch an... \n’Verloren und Vergessen - Kapitel 13. ... \n'Wie lange war ich krank?'\n’Fünf Sonnenumdrehungen'\nWie habe ich das überlebt? Manche Menschen stecken sich gar nicht erst an.\nSie scheinen eine Art Abwehrkraft in ihnen zu haben.\nDiejenigen, die erkrankten, starben immer. Aber ich erwachte.'\n{x}:\t\"Spannend! Ich hätte doch Schriftsteller und nicht König werden sollen.\" ".format(x=username),
        u"Bewegen kann man diese Schränke ja nicht! Ich schau mir mal ein Buch an... \n’Im Namen des Windes - Kapitel 2...\n’Warum konnte er mich nicht leiden? Nach allem was ich für ihn getan hab, liebte er noch immer eine andere.\nWie ich diese Schande Tag für Tag überleben kann... \nWenn mich die Götter doch erhöhrten und meinen Geliebten zu mir schickten!\nWas gäbe ich für nur einen Kuss?\n{x}:\t\"Spannend! Ich hätte doch Schriftsteller und nicht König werden sollen.\"".format(x=username),
        u"Bewegen kann man diese Schränke ja nicht! Ich schau mir mal ein Buch an... \n'Kuchen fast ohne Teig' - Kapitel 7...\nDie Eier mit dem Zucker aufschlagen. \nDie zerlassene Butter, dann das Mehl kräftig einarbeiten, bis ein glatter Teig entstanden ist. \nPortionsweise mit der Milch verdünnen. Den Dattelwein angießen.'\n{x}:\t\"Spannend! Ich hätte doch Kochbuchautor und nicht König werden sollen.\"".format(x=username),
        u"Bewegen kann man diese Schränke ja nicht! Ich schau mir mal ein Buch an... \n'Märchen aus einem fernen Land - Kapitel 52...\nDer mächtigste unter diesen Flusskönigen ist der goldene Drachenkönig. \nEr erscheint häufig in der Gestalt einer kleinen goldenen Schlange mit vereckigem Kopfe, \nniedriger Stirn und roten Punkten über den Augen.'\n{x}:\t\"Spannend! Ich hätte doch Schriftsteller und nicht König werden sollen.\"".format(x=username),
        u"Bewegen kann man diese Schränke ja nicht! Ich schau mir mal ein Buch an... \n'Die drei Semikolons - Kapitel 3...\n'Justine, Pete und Rob machten sich auf den Weg in ihrer Pferdekutsche. \nWährend Pete und Rob sich um den frischen Blaubeermuffin stritten, überlegte Justine angestrengt. \n'Wer könnte der Täter sein?'.'\n{x}:\t\"Spannend! Ich hätte doch Jugendbuchautor und nicht König werden sollen.\"".format(x=username),
        u"Bewegen kann man diese Schränke ja nicht! Ich schau mir mal ein Buch an... \n'Dunkle Organisationen - Die heimlichen Retter der Gesellschaft? Kapitel 3.5.7 ... \n'Black Sky. \nAußer ihrem Namen und ihrer angeblichen Existenz ist nichts bekannt. \nEs ranken sich unzählige Mythen um sie. Sind sie wirklich die Gechtigkeitskämpfer, die sie vorzugeben scheinen?'\n{x}:\t\"Spannend! Ich hätte Verschwörungstheoretiker und nicht König werden sollen.\"".format(x=username),
        ]
    print(book_texts[random.randint(0,len(book_texts)-1)])

def text_schalter(phase, username):
    if phase == "unflipped":
        print(u"Mit einem Ruck und einem hohen, quietschenden Ton legt {x} den Schalter um.".format(x=username))
        misc.countdown(0.5)
        print("...  KRAWUMMS ... grschhhhhzzt ... TSCHINK! ...")
        misc.countdown(1.5)
        misc.write(u"Nanu? Das klang so, als wäre weit entfernt etwas Schweres über den Boden geschoben worden?")
    elif phase == "unflipped_46": #bei einem schalter soll ein hinweis der richtung kommen
        print(u"Mit einem Ruck und einem hohen, quietschenden Ton legt {x} den Schalter um.".format(x=username))
        misc.countdown(0.5)
        print("...  KRAWUMMS ... grschhhhhzzt ... TSCHINK! ...")
        misc.countdown(1.5)
        misc.write(u"Nanu? Das klang so als wäre aus süd-westlicher Richtung etwas Schweres über den Boden geschoben worden. War da nicht eine Person?")
    elif phase == "flipped":
        misc.write("Da passiert gar nichts mehr - der Schalter scheint sich festgezogen haben.")

def text_truhe_66(phase):
    if phase == "take":
        print(u"Die schwere Holzkiste öffnet sich nur sehr schwer...")
        print(u"... *knarz* ...")
        misc.countdown(1)
        misc.write("In der Kiste liegt ein Buch mit dem Titel \"Rhetorik für Dummys und Fortgeschrittene\".") 
        misc.write(u"Ich habe das Rhetorikbuch bekommen! Deine Rhetorik Fähigkeiten sind um 5 Punkte gestiegen!\n")
    elif phase == "empty":
        misc.write(u"Hier gibt es leider nichts mehr zu finden.")

def text_truhe_33(phase):
    if phase == "take":
        misc.write(u"Wie lecker! In der Kiste ist ein Glückskeks!")
        print("*knurps*")
        misc.countdown(0.5)
        print("*krümmel*")
        misc.countdown(0.5)
        print("*knurps*")
        misc.countdown(0.5)
        misc.write("Auf dem Zettel steht: Wir lernen Chinesisch! [...]")
        print("- falsche Seite, umdrehen - ")
        misc.countdown(0.5)
        misc.write("\"Schon sehr bald wirst du ein Gebäck essen.\"\nDiese Dinger können ja wirklich die Zukunft vorhersagen.")
        misc.write("Du hast den Kekszettel erhalten! Deine Weisheit Fähigkeiten sind um 5 Punkte gestiegen! ")
    elif phase == "empty":
        misc.write(u"Hier gibt es leider nichts mehr zu finden.")

def text_truhe_37(phase):
    if phase == "take":
        misc.write(u"In der Kiste liegt das Buch '100 schlagfertige Antworten für den Alltag'. Damit weiß man sich auf der Straße zu behaupten!")
        misc.write(u"Du hast das Dissbuch erhalten! Deine Schikane Fähigkeiten sind um 5 Punkte gestiegen!")
    elif phase == "empty":
        misc.write(u"Hier gibt es leider nichts mehr zu finden.")

def text_voices(phase):
    if phase == "flipped":
        misc.write(u"Ja, da vorne ist ganz sicher jemand, und jetzt kann ich weiter in den Westen und nachschauen wer hier in meiner Bibliothek so ein Chaos angerichtet hat!")
    elif phase =="unflipped":
        misc.write(u"Da sind doch Schritte in der Ferne? Ob es wohl irgendwo anders eine Möglichkeit gibt hinter diese Wand zu kommen? Hier führt jedenfalls kein Weg vorbei.")

def text_bankett(username):
    misc.write("Ein bisschen hiervon, ein bisschen davon! Und ganz viel von allem. Ich liebe gutes Essen!")
    misc.write("{x} hat sich erholt!".format(x=username))

def text_siegesl(phase):
    if phase == "take":
        misc.write(u"Ich öffne die Kiste und finde... ")
        print("...")
        misc.countdown(1)
        misc.write("Mein Siegeslächeln! Mit dem auf meinen Lippen kommen alle meine Reden besser an!")
        misc.write("Du hast dein Siegeslachen erhalten! Deine Rhetorik Fähigkeiten sind um 10 Punkte gestiegen!")
    elif phase == "empty":
        print("Hier gibt's wohl nichts mehr.")

def text_vase():
    misc.write(u"Auf die Unterseite des Tisches mit der Blumenvase wurde besimmt ein geheimer Hinweis versteckt. Mal lesen")
    print("...")
    misc.countdown(1)
    misc.write(u"Nö, nichts. Nur ein Tisch mit einer Vase.")

def text_kerze():
    misc.write(u"Autsch, heiß!\nDie lass ich besser hier.")

def text_truhe_13(phase):
    if phase == "take":
        misc.write("Die Kiste öffnet sich unter lautem Knarzen")
        print("krzzzzzzt")
        misc.countdown(1)
        misc.write(u"Da ist ja mein Ass im Ärmel!")
        misc.write(u"Damit ist der Sieg auf meiner Seite!")
        misc.write(u"Du erhältst das 'Ass'. Deine Schikane Fähigkeit sind um 10 Punkte gestiegen!")
    elif phase == "empty":
        misc.write(u"Die wurde bereits geleert. Am besten geh ich nochmal weg und schau dann rein, ob sie wieder aufgefüllt wurde.")

def text_waffenhalter():
    misc.write(u"Schade, dass der leer ist, sonst hätte ich ein eigenes Schwert oder eine Axt. Wie soll denn ohne mittelalterliche Waffen richtige RPG Atmosphäre aufkommen? Hinter dem Waffenhalter ist...")
    print("*drumroll*")
    misc.countdown(1)
    misc.write(u"Genau garnichts.")

def text_leiter(username, phase):
    if phase=="falsch":
        misc.write(u"Die Leiter lässt sich wohl auch mit ganzer Kraft nicht eine Elle bewegen. Mal hochklettern...")
        print("...")
        misc.countdown(1)
        misc.write(u"Seltsam, die Leiter führt direkt an die Decke.")
    elif phase == "schienen":
        misc.write(u"Ein paar Schienen, auf denen nichts zu stehen scheint. Ihr Sinn an dieser Stelle ist unbegreiflich. Ist das eine Metapher für die Sinnlosigkeit unserer Existenz? Oder schlechtes Game-Design?")
    elif phase == "richtig":
        misc.write(u"Ich klettere die Leiter hoch und finde... einen versteckten Hohlraum mit einer kleinen Schatzkiste! Super!")
        misc.write(u"In der Schatzkiste...mhm, wie das duftet..")
        print("...")
        misc.countdown(1)
        misc.write(u"Befindet sich Koffein! Der heilige Gral unter den Weisen! Herrlich!")
        misc.write(u"Du erhältst Koffein. Deine Weisheit Fähigkeiten sind um 10 Punkte gestiegen!")
    elif phase == "empty": #wird aufgerufen, wenn die leiter zwar an der richtigen stelle ist, aber die truhe bereits geleert wurde
        misc.write(u"Ich klettere die Leiter hoch und finde... eine wohlduftende Holzkiste. Lagerte hier etwa jemand Koffein? Hmhmh!")

def text_decke():
    misc.write(u"Abgesehen von einigen Flecken an der Decke sehe ich nichts Besonderes. Momentchen! Die verdreckten Deckenflecken haben die Form eines... Drachen!?")

def text_schild():
    misc.write(u"Richtung Süden: “Frauenquartiere, Dach”, und in die andere Richtung “Männerquartiere, Bibliothek”.")

def text_wachfenster():
    misc.write(u"Ein grausiger Sturm wütet draußen. Ist das etwa das Zeichen der Götter nah über uns?")

def barriere_text():
    misc.write(u"Das verzerrte Gesicht in der Barriere beginnt plötzlich laut schallend zu reden:")
    misc.write(u"Du solltest mehr auf deine Wachen achten, König.")
    misc.write(u"Wenn du auf deine Untertanen gehört hast, kennst du die drei Worte.")
    misc.write(u"Du musst sie nur noch in die richtige Reihenfolge bringen, bevor du zu deiner letzten Herausforderung kommst.")
    print(u"Was ist das erste Wort?")
    word1=input()
    print(u"Was ist das zweite Wort?")
    word2=input()
    print(u"Was ist das dritte Wort?")
    word3=input()
    word1p = parse.check_input(word1, ["alle", "marduk"])
    word2p = parse.check_input(word2, ["lieben"])
    word3p = parse.check_input(word3, ["marduk", "alle"])
    if (word1p == ["alle"] and word2p == ["lieben"] and word3p==["marduk"]) or (word1p == ["marduk"] and word2p == ["lieben"] and word3p==["alle"]) :
        print("ALLE LIEBEN MARDUK")
        solved = True
        misc.write(u"Du hast gelernt, auf deine Wachen zu achten. Das war der erste Schritt. Wenn du bereit für den letzten bist, gehe hinaus und stelle dich deinem Schicksal.")
        print("*klick*")
        misc.countdown(1)
        misc.write("Die Barriere scheint verschwunden zu sein.")
    else:
        print(word1.upper()+" "+word2.upper()+" "+word3.upper())
        misc.countdown(1)
        print("...")
        misc.countdown(1)
        misc.write(u"Du musst genauer hinhören. Die Wachen bieten dir ihre Hilfe an! Du musst sie nur annehmen.")
        solved = False
    return solved