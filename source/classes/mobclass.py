#!/usr/bin/env python
# -*- coding: utf-8 -*-

class Mob():
    #Zur Struktur und Nutzung der einzelnen Mob Eigenschaften habe ich eine Übersicht in dev (s. template.txt) erstellt 
    #make_attribute_list und give_info funktionieren entpsrechend der gleichnamigen Funktionen des Spielerobjektes, siehe source/classes/playerclass.py
    def __init__(self, myname, myatk, myrh_def, mysc_def, mywh_def, myexp, mydesc, mywk, myab_sc, mymi_sc, mysch_sc, myart, myspecial):
        self.name = myname
        self.atk = myatk                    #Liste von Namen der Angriffe
        self.rh_def = myrh_def
        self.sc_def = mysc_def
        self.wh_def = mywh_def
        self.exp = myexp
        self.desc = mydesc
        self.wk = mywk
        self.noneffective_sc = myab_sc      #Uneffektive Beleidigungen
        self.okeffective_sc = mymi_sc       #Normal wirkende Beleidigungen
        self.effective_sc = mysch_sc        #Sehr affektiv eingreifende Beleidigungen
        self.art = myart
        self.special = myspecial            #Slot zum Speichern besonderer Effekte

    def make_attribute_list(self):
        mylist = []
        mylist.append(self.name)
        mylist.append(self.atk)
        mylist.append(self.rh_def)
        mylist.append(self.sc_def)
        mylist.append(self.wh_def)
        mylist.append(self.exp)
        mylist.append(self.desc)
        mylist.append(self.wk)
        mylist.append(self.noneffective_sc)
        mylist.append(self.okeffective_sc)
        mylist.append(self.effective_sc)
        mylist.append(self.art)
        #hilft, in anderen Funktionen auf die einzelnen Werte zuzugreifen
        return mylist

    def give_info(self):
        listinfo= self.make_attribute_list()            
        print("Name: ", listinfo[0])
        print("Attacken: ", listinfo[1])
        print("RHDEF: ", listinfo[2])
        print("SCDEF: ", listinfo[3])
        print("WHDEF: ", listinfo[4])
        print("EXP die es gibt:: ", listinfo[5])
        print("Beschr: ", listinfo[6])
        print("HP:", listinfo[7])
        print("nichteffektive angriffe:: ", listinfo[8])
        print("mitteleffektive angriffe: ", listinfo[9])
        print("effektive angriffe: ", listinfo[10])
        #wurde während der Entwicklung genutzt, um zu überprüfen ob die Mob Werte korrekt eingelesen werden