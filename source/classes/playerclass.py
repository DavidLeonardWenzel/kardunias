#!/usr/bin/env python
# -*- coding: utf-8 -*-

from source.dev_tools import log
import sys
import collections
class Player():
    #Zur Struktur und Nutzung der einzelnen Player Eigenschaften habe ich eine Übersicht in dev (s. template.txt) erstellt
    def __init__(self, myname, mymap, myx, myy, mylvl, myexp, mycurhp, mymaxhp, mystat, myrh, mysc, mywh, myequip, invlist, myrh_off, myrh_def, mysc_off, mysc_def, mywh_off, mywh_def, mytries, myevents):
       self.name = myname
       self.map = int(mymap)
       self.x = int(myx)
       self.y = int(myy)
       self.lvl = int(mylvl)
       self.exp = int(myexp)
       self.hpcur = int(mycurhp)
       self.hpmax = int(mymaxhp)
       self.stat = mystat
       self.rh = int(myrh)
       self.sc = int(mysc)
       self.wh = int(mywh)
       self.equip = myequip
       self.invlist = invlist
       self.rh_off = int(myrh_off)
       self.rh_def = int(myrh_def)
       self.sc_off = int(mysc_off)
       self.sc_def = int(mysc_def)
       self.wh_off = int(mywh_off)
       self.wh_def = int(mywh_def)
       self.failed_tries = mytries
       self.events = myevents

    # Eine Funktion, die das iterieren über alle Attribute erleichtern soll, indem eine Liste daraus erzeugt wird
    def make_attribute_list(self):
       myList = []
       myList.append(str(self.name))
       myList.append(str(self.map))
       myList.append(str(self.x))
       myList.append(str(self.y))
       myList.append(str(self.lvl))
       myList.append(str(self.exp))
       myList.append(str(self.hpcur))
       myList.append(str(self.hpmax))
       myList.append(str(self.stat))
       myList.append(str(self.rh))
       myList.append(str(self.sc))
       myList.append(str(self.wh))
       myList.append(self.equip)
       myList.append(self.invlist)
       myList.append(str(self.rh_off))
       myList.append(str(self.rh_def))
       myList.append(str(self.sc_off))
       myList.append(str(self.sc_def))
       myList.append(str(self.wh_off))
       myList.append(str(self.wh_def))
       myList.append(self.failed_tries)
       myList.append(self.events)
       return myList

    def save(self):
        # Speichert alle Attribute in einer savefile.txt
        path = "savefiles/"+str(self.name)+".txt"
        #der path ist relativ von wo er aufgerufen wird, nicht, wo diese Funktion selbst liegt 
        #würde sie von hier aus aufgerufen werden, müsste man zuerst /../ hochgehen, aber das Spiel wird immer von kardunias.py ganz oben aufgerufen
        #zunächst wird ein savfile im "write" Modus geöffnet (existiert keine txt mit dem Namen, würde sie an dieser Stelle erstellt werden)
        target = open(path, "w")
        myList = self.make_attribute_list()
        #die make_attribute_list Funktion sammelt alle Werte in einer Liste, sodass nun über diese iteriert werden kann
        for elem in myList:
            #Die meisten Attribute sind simple Strings/unicodes/Zahlen, die einfach geschrieben werden können und jeweils durch newline getrennt werden
            if type(elem) == type(elem)==int or type(elem) == str:
                target.write(str(elem))
                target.write("\n")
            #Wenn es eine Liste ist, muss sichergestellt werden, das zwischen den einzelnen Listenelementen dasselbe Trennzeichen in der .txt ist
            elif type(elem)==list:
                for item in elem:
                    #wenn es eine Liste ist, soll alles mitsamt Umlauten in die txt geschrieben werden und durch ein space getrennt
                    target.write(item)
                    target.write(" ")
                #unabhängig davon werden alle Attribute durch ein newline getrnnet, um die einzelnen Elemente im Speicherstand auseinanderhalten zu halten
                target.write("\n")
            #Das einzige dict in Speicherständen/Spielerobjekten sind die Events. Für diese ist nur der Wert relevant, also
            #ob das Event schon passierte oder nicht ("wurde Rätsel x schon gelöst oder nicht?")
            elif type(elem) == dict:
                for num, event in enumerate(sorted(elem.keys())):
                    #indem Fall soll das dict nach dem Alphabet seiner keys sortiert werden
                    #Das ist notwendig, da dicts ungeordnet sind und sichergestellt werden muss, dass die Reihenfolge in der .txt immer gleich ist
                    if elem[event]==False:
                        target.write("0") #eine 0 als String repräsentiert im Speicherstand den bool false
                    else:
                        target.write("1") #und umgekehrt
                    target.write(" ") #getrennt werden die events durch space
                target.write("\n") #getrennt wird das gesamte event-dict durch newlines (wie alle Attribute)
            else: #Fehlermeldung, falls noch etwas übrig ist (sollte nicht der Fall sein)
                log(str(type(elem)), "error")
                log("... " + str(elem), "error")
                log("... " + "versuche etwas nicht erwartetes zu speichern, beende programm", "error")
                quit()
        #zuletzt wird die Datei geschlossen und eine Meldung erscheint, dass das Spiel erfolgreich speicherte
        target.close()
        print("Das Spiel wurde gespeichert!")

    #Gibt eine liste mit allen verfügbaren Skills zurück, in dem aus die dafür vorgesehen Attribute des Spielerobjektes überprüft
    def possible_skills(self):
        all_skills = []
        if self.rh_off == 1:
            all_skills.append(u"Suggerieren")
        elif self.rh_off == 2:
            all_skills.append(u"Überzeugen")
        elif self.rh_off == 3:
            all_skills.append(u"Rede halten")
          
        if self.rh_def == 1:
            all_skills.append(u"Intuition")
        elif self.rh_def == 2:
            all_skills.append(u"Nachschlagen")
        elif self.rh_def == 3:
            all_skills.append(u"Wikipedia nutzen")

        if self.sc_off == 1:
            all_skills.append(u"Trick-17")
        elif self.sc_off == 2:
            all_skills.append(u"Tyrannisieren")
        elif self.sc_off == 3:
            all_skills.append(u"Wutrede halten")

        if self.sc_def == 1:
            all_skills.append(u"32-Heb-Auf")
        elif self.sc_def == 2:
            all_skills.append(u"Posieren")
        elif self.sc_def == 3:
            all_skills.append(u"Sibirischer Doppelbluff")

        if self.wh_off == 1:
            all_skills.append(u"Demoralis")
        elif self.wh_off == 2:
            all_skills.append(u"Anima Vincere")
        elif self.wh_off == 3:
            all_skills.append(u"Leckerli hergeben")

        if self.wh_def == 1:
            all_skills.append(u"Abrakapflaster")
        elif self.wh_def == 2:
            all_skills.append(u"3xSchwarzer-1.-Hilfe-Kasten")
        elif self.wh_def == 3:
            all_skills.append(u"Simsala-Wundheilzauber wirken")

        #all_string ist ein String, der schön dargestellt aufgezählt alle Skills anzeigt (daher wird mit num hochgezählt und alle Skills durch ein Komma getrennt)
        all_string = u""
        num=0
        for elem in all_skills:
            num+=1
            all_string = all_string+"["+str(num)+"] "+elem+", "
        return all_string[:-2], all_skills

    def give_info(self):
       #Eine Funktion, die eine Art Übersichtsmenu für den Spieler geben soll
       #gibt im Grunde nur alle Attribut Werte, in die der Spieler Einsicht haben soll, wieder
       #Diese Funktion ist somit das "inventar" oder "menü"
       print(u"Du bist also {x}, Level {y} ({q} EXP)".format(x=self.name,y=self.lvl, q=self.exp))
       print ("{x}/{y} HP".format(x=self.hpcur, y=self.hpmax))
       if self.stat == "Gesund":
         print(u"Du fühlst dich gesund!")
       else: 
         print(u"Du bist derzeit: {x}".format(x=self.stat))
       print(u"Rhetorik:\tSchikane:\tWeisheit:")
       print(u"{x}\t\t{y}\t\t{z}".format(x=self.rh, y=self.sc, z=self.wh))
       for elem in self.equip:
         print(u"Du hast {x}".format(x=elem.title()))
       for elem in self.invlist:
         print(u"Du hast {x}".format(x=elem.title()))
       print(u"Du hast folgende Skills erlernt:")
       if self.rh_off == 0 and self.rh_def == 0:
         print(u"Du hast noch keine Fähigkeit der Rhetorik erlangt")
       elif self.rh_off == 1:
         print(u"Du kannst Suggerieren")
       elif self.rh_off == 2:
         print(u"Du kannst Überzeugen")
       elif self.rh_off == 3:
         print(u"Du kannst eine Rede halten")
      
       if self.rh_def == 1:
         print(u"Du kannst Intuition nutzen")
       elif self.rh_def == 2:
         print(u"Du kannst Nachschlagen")
       elif self.rh_def == 3:
         print(u"Du kannst Wikipedia nutzen")

       if self.sc_off == 0 and self.sc_def == 0:
         print(u"Du hast noch keine Fähigkeit der Schikane")
       elif self.sc_off == 1:
         print(u"Du kannst Trick-17")
       elif self.sc_off == 2:
         print(u"Du kannst Tyrannisieren")
       elif self.sc_off == 3:
         print(u"Du kannst Wutrede halten")

       if self.sc_def == 1:
         print(u"Du kannst 32-Heb-Auf")
       elif self.sc_def == 2:
         print(u"Du kannst Posieren")
       elif self.sc_def == 3:
         print(u"Du kannst den sibirischen Doppelbluff")

       if self.wh_off == 0 and self.wh_def == 0:
         print(u"Du hast noch keine Fähigkeiten in Weisheit und Magie")
       elif self.wh_off == 1:
         print(u"Du kannst Demoralis wirken")
       elif self.wh_off == 2:
         print(u"Du kannst Anima Vincere wirken")
       elif self.wh_off == 3:
         print(u"Du kannst einen Leckerli hergeben - unwiderstehlich!")

       if self.wh_def == 1:
         print ("Du kannst Abrakapflaster!...wirken oder aufkleben?")
       elif self.wh_def == 2:
         print(u"Du kannst 3xSchwarzer-1.-Hilfe-Kasten...wirken oder anwenden?")
       elif self.wh_def == 3:
         print(u"Du kannst Simsala-Wundheilzauber! wirken - endlich mal ein echter Zauber")