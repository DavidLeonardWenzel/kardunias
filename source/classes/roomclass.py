#!/usr/bin/env python
# -*- coding: utf-8 -*-

class Room():
    #soll die einzelnen, zu einem Raum gehörenden Dinge wie Gegenstände, NPCs und Koordinaten festhalten
    def __init__(self,myx,myy,mytype,myfurniture,mymobs,mymobchance,myumschauen,mystatus,mybeschreibung,myaltbeschreibung):    
        self.x = myx
        self.y = myy
        self.type = mytype 
        self.furniture = myfurniture        #eine Liste aller "furnitures" pro Raum, enthält die Objekte
        self.mobs = mymobs    
        self.mobchance = mymobchance  
        self.beschreibung = mybeschreibung 
        self.status = mystatus    
        self.umschauen = myumschauen     
        self.alt_beschreibung = myaltbeschreibung

    def change_status(self):  
        #Diese Funktion ändert den "status" des Raumes (meist 1, wenn der Raum alternative beschreibende Strings haben soll, weil sich etwas verändert hat, wird diese Funktion benutzt)
        if self.status == "1":
            self.status = "2"
        else:
            self.status = "1"
            
    def print_desc(self):
        #Gibt entweder die normale Raumgeschreibung oder die alternative je nach Status 
        if self.status == "1":
            print(self.beschreibung)
        else:
            print(self.alt_beschreibung)