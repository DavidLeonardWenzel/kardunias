#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# itemclass.py
# 
# Diese Klasse soll items darstellen und kann die Strings aus der CSV für Items ziehen (nutzt wie roomclass auch pandas)
# Was mit was kombiniert werden kann, wird aus den einzelnen Spalten klar, die dabei erzeugten Strings werden auch von hier geprintet
from source.dev_tools import log
import pandas as pd
import random
from .. import misc

class Item():

    def __init__(self, myname, myid, mydescription, mycombines_with_to):
        self.name = myname
        self.id = myid
        self.description = mydescription
        self.combines_with_to = mycombines_with_to  #Ein dict bestehend aus "kombinier_mit_item_id" : "kombinationsprodukt_id"

    def print_default_text(self):
        #Die Standard Texte, wenn eine Kombination zweier Items nicht möglich ist (für manche Kombis gibt es spezifische lustige Texte in der CSV)
        default1 = u"Die Idee war gut, aber die Arme waren zu kurz."
        default2 = u"Das geht so nicht."
        default3 = u"Vielleicht klappt das mit anderen Gegenständen besser..."
        #Aus den default texten wird eine Liste erstellt, dann mit dem random modul eine zufällige ausgesucht
        default_texts = [default1, default2, default3]
        chosen = random.randint(0,len(default_texts)-1)
        print(default_texts[chosen])

    def check_combination(self,item1,item2,username):
        #Lade inventory informationen
        inv_data = pd.read_csv('data/inventory.csv', index_col=0, dtype={'key':"unicode"}, sep='\t')
        #Gib einen Text aus, der aus der inventory.csv tabelle entnommen wurde       
        text = (inv_data[item1.name][item2.name])
        if text == "x":
            #der Text sollte nie so in der CSV herausgelesen werden, da die Kombination nicht möglich sein sollte (z.B. item eig. schon verbraucht sein sollte)
            log(u"Diese Kombi sollte nich möglich sein", "error")
        elif text == "DEFAULT":
            #für alles kann man sich keinen lustigen text überlegen, daher gibt es defaults
            self.print_default_text()   
            #da die Reihenfolge des kombinierens in beide Richtungen erlaubt wird, steht "-" in der csv für: schaue in der entsprechenden anderen Zelle
        elif text == "-":
            text = inv_data[item2.name][item1.name]
            if text == "-" or text == "x": #Wenn in der anderen Zelle auch ein - ist, ist in der CSV ein Fehler
                log("Check combination error!", "error")
            elif text == "DEFAULT":
                self.print_default_text()
            else:
                text = text.replace("USER", str(username))
                #ich ersetze USER in den Strings mit .replace mit dem tatsächlichen Nutzernamen des Spielers
                print(text)
        else:
            text = text.replace("USER", str(username))
            print(text)

        #Prüfe ob Kombination beider items zu einem Produkt führt, returne den Namen des Produkts
        product_id = 0 #zunächst auf 0 setzen, falls es kein Produkt gibt
        for elem in item1.combines_with_to.keys():
            if item2.id == elem:
                product_id = item1.combines_with_to[elem]
        if product_id != 0:
            product_row = inv_data[inv_data["ID"] == product_id]
            product_name = product_row.index[0]
            return product_name
        else:
            return ""

        