#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Die Engine soll alle Objekte enthalten und manipulieren können. Es soll die Möglichkeiten des Spielers dynamisch erstellen. 
# Es soll die Strings printen, welche für Raum und Objektbeschreibungen dem "/data" Ordner entnommen sind (und dort in CSV, XML oder TXT Datei vorliegen)
# Spieler, Raum und Monsterobjekte werden aus den Klassen in dem "/source/classes" Ordner erstellt

from source.dev_tools import log
#ein simpler Parser, welcher Schreibfehler und Buchstabendrehen "korrigiert". Wird bei fast jeder Eingabe einmal verwendet. 
import source.type_parser as parse
#helfende Texte, falls der Spieler feststeckt, sind in helptexts.py
import source.helptexts as helper
#in misc sind häufig benutzte Funktionen welche z.B. Ja/Nein Fragen abhandeln, 'ENTER' nach print verlangen u.Ä.
import source.misc as misc
#Das Spiel Outro wird nach besiegem des Bosses abgespielt und von der Engine aus geladen, daher wird diese hier importiert.
import source.outro as outro
#Alle Kämpfe und das Leveling wird über "battle.py" abgehandelt. Die py-Datei liegt in "/source". 
import source.battle as bt
#die benutzten Klassen:
import source.classes.playerclass as playerfuncs
import source.classes.roomclass as roomfuncs
from source.classes.itemclass import Item
import source.classes.furnitureclass as Furniture

import codecs
import os
#default packages die ich nutze:
import random
import pandas as pd
#sys erlaubt, das default encoding des System zu sehen. Hierdurch können Umlaute unter 
#verschiedenen Betriebssystemen im Terminal/CMD angenommen und als unicode verglichen werden.
#random wird für Zufallswerte und Entscheidungen genutzt, re für reguläre Ausdrücke um z.B. nur Zahlen zu erlauben. 
#os wird genutzt, um im savefiles Ordner nach Speicherständen zu suchen, pandas wird genutzt, um die csv Tabellen einfach einzulesen.

#Zunächst erstelle ich eine Klasse, und setze die gewünschten Attribute
class Engine():
    def __init__(self, myname, mykeyword_list, myuser, myroom):
        
        # Name des Spielers 
        self.name = myname
        #die keyword_list soll immer alle Möglichkeiten des Spielers enthalten, sodass man überprüfen kann, was in dem Moment möglich ist (z.B., in welche Himmelsrichtung man gehen kann)
        #sie besteht aus einer Liste an listen, wobei die einzelnen Listen sich an thematischen Kategorien richten:
        #[0] general    (speichern, menü)
        #[1] inventory  (die Items des Spielers)
        #[2] location   (in welche Richtungen kann man von hier gehen)
        #[3] skill      (die verschiedenen Fähigkeiten des Spielers abfragen)
        #[4] furniture  (enthält die Interaktionsmöglichkeiten mit Gegenstände und NPCs des Raums, wobei pro Gegenstand nochmal eine Unterliste an Ansprechweisen exisitiert)
        #Der Vorteil dieser Modellierung ist, das man bei einem Raumwechsel nicht die gesamte Möglichkeiten neu generieren muss, sondern nur Himmelsrichtungen und Furniture austauschen kann
        self.keyword_list = [[],[],[],[],[]]      
        #Das Spielerobjekt wird hier gespeichert, sodass man von überalls aus auf HP, Inventar etc. zugreifen kann
        self.user = myuser
        #Das Raumobjekt wird hier gespeichert, sodass man den Status des Raumes abrufen kann (z.B. mit offener und verschlossener Tür)
        self.room = myroom

########################### Die wichtigsten Funktionen - Objekte erstellen und Input entgegennehmen ###########################
    def handle_input(self):
        #Die Funktion flattenedkeywordlist sorgt dafür, dass aus den kleinen Liste eine einzelne (flache) Liste mit allen Keywords entsteht
        flattened_keyword_list = self.flatten_irregular_list(self.keyword_list)
        #Als nächstes wird der Spieler nach einer Eingabe gefragt
        print(u"\n>>> Was möchtest du tun?")
        user_said=input()
        #Diese Eingabe wird zusammen mit den Möglichkeiten an den "input parser" übergeben, welcher als Liste die Wörter returned, 
        #die in der keyword liste enthalten sind (oder ähnlich genug geschrieben sind, das er korrekt rät, es fehlte z.B. ein A, und dann die richtige Version returned)
        user_choices = parse.check_input(user_said, flattened_keyword_list)
        print("") #Es sieht besser, nach jedem input eine Lücke zu lassen. 
        if user_choices == []: #wenn die Liste leer ist, wurde kein Wort erkannt, welches auch einer Möglichkeit aus der keyword list entspricht
            #indem fall möchte ich, falls es eine Richtungsangabe war, hier sagen dass diese Richtung gerade nicht begehbar ist:
            directions = [
            u"norden",u"nördlich",u"oben",u"vorne",
            u"süden",u"südlich", u"unten", u"hinten",
            u"westen",u"westlich",u"links", 
            u"osten",u"östlich",u"rechts"]
            for elem in directions:
                if elem == user_said:
                    if self.user.events["ist_aufgestanden"] == False: #Wenn man noch im Bett liegt und eine Richtung angibt, kommt eine kleine Erinnerung:
                        print(u"Vielleicht stehe ich besser erst auf.")
                    else: #und ansonsten die Warnung, dass diese Richtung nicht möglich ist
                        print(u"Da geht es leider gerade garnicht entlang.")
                    #Unabhängig davon, soll die Funktion hiernach abbrechen. Die große while in "kardunias.py" ruft dann erneut handle_input auf, sodass weiter gespielt werden kann
                    return 0 
            #in jedem Fall soll einer der "Versuch es nochmal, es wurde nichts erkannt" Strings aufgerufden werden
            self.try_again_text()
            #Zusätlich werden alle ungültigen Eingaben im Spielerobjekt, und später in dem Speicherstand Textfile gespeichert, sodass diese eventuell eingebaut werden können
            self.user.failed_tries.append(user_said)
        else:
            #Wenn die Eingabe einer der Möglichkeiten matcht, ist user input nicht leer
            #in dem Fall wird durch die Liste der akzeptierten Wörter iteriert und geprüft, welcher Kategorie an Möglichkeiten das Wort angehört
            #Abhängig davon können die Worte dann an Inventarspezifische, Umgebungsspezifische oder Objektspezifische "handle input" Funktionen geleitet werden
            #falls multiple wörter als Eingabe verlangt werden sollen (Stehe+Auf) sind diese als einzelne listenelemente in choice und werden bei handle eingabe einzeln überprüft
            for choice in user_choices:
                if choice in self.keyword_list[0]:
                    #Wenn es in den generellen Möglichkeiten war:
                    self.handle_general_input(user_choices)
                    return 0 
                elif choice in self.keyword_list[1]:
                    #Wenn es eine Inventar Möglichkeit war:
                    self.handle_inv_input(user_choices)
                    return 0 
                elif choice in self.keyword_list[2]:    
                    #Wenn es eine Richtungsmöglichkeit war:
                    self.handle_location_based_input(user_choices)
                    return 0 
                elif choice in self.keyword_list[3]:
                    #Wenn es einer der Skills war:
                    self.handle_skill_input(user_choices)
                    return 0 
                    #In jedem Fall wird returned, sodass nicht weiter über den Rest der choices iteriert wird (fungiert als break)
                else:
                    #Da Objekte Ansprechmöglichkeiten nochmal in Unterlisten liegen, wird hier noch mal iteriert:
                    for furn_list in self.keyword_list[4]:  
                        if choice in furn_list:
                            self.handle_furniture_based_input(user_choices)
                            return 0 
                    #wenn handle input jetzt noch läuft, wurde etwas erlaubt, was nirgendwo abgefangen wird, daher hier eine Fehlermeldung:
                    log("Es kam ein Wort durch was akzeptiert, aber nicht einsortiert wurde: " + str(choice), "error")
            else:
                log("Es kam ein Wort durch was akzeptiert, aber nicht einsortiert wurde: " + str(choice), "error")

    def flatten_irregular_list(self, l):
        #Diese Funktion ändert eine Liste mit Unterlisten in eine flache, einzelne Liste mithilfe von ".extend", um sie in handle_input nutzen zu können
        result = []
        for elem in l:
            if type(elem) != list:
                result.append(elem)
            else:
                result.extend(self.flatten_irregular_list(elem))
        return result

    def try_again_text(self):
        #Eine Sammlung an lustigen "Bitte erneut oder anders angeben" Texten
        failed_input_answers = [
        u"Versuch bitte, es anders auszudrücken.",
        u"Der Erzähler ist perplex und hat seinen Faden verloren. Gebe erneut etwas ein, damit der Erzähler seine Geschichte fortführen kann.",
        u"Es gibt viele Dinge, die der Mensch nicht versteht. Der Computer versteht noch weniger, unter anderem das, was als letztes eingetippt wurde.",
        u"Es gibt sicherlich ein gutes Synonym für das was du tun wolltest.",
        u"Ja! Gute Idee! Vielleicht aber ein anderes Mal. Versuch doch jetzt erstmal was anderes."]
        if self.user.events["ist_aufgestanden"] == False:
            print(u"Vielleicht sollte ich erst einmal aufstehen, bevor ich etwas anderes versuche...")
        else:
            #Der Listenindex für den Text wird mit dem random Modul zufällig ausgewählt
            print(failed_input_answers[random.randint(0,len(failed_input_answers)-1)])

    def build(self, step):
        #Neben handle_input die zweite Hauptfunktion, welche alle "aufbauende" Funktion in einer Kette aufruft
        #Step ist die "Schrittzahl" mit der Funktion, an der jewelis die Objekte und Keywordmöglichkeiten generiert werden sollen
        if step <= 0:
            self.add_general_options_to_keylist()
            #Fügt generelle keywords hinzu    
        if step <= 1:
            self.add_inv_options_into_keylist()
            #Fügt Inventar keywords hinzu    
        if step <= 2:
            self.generate_loc()
            #erstellt das Raumobjekt und fügt entsprechende Richtungskeywords hinzu    
        if step <=3:
            self.check_battle()
        if step <= 4:
            self.add_skill_options_to_keylist()
            #Fügt skill keywords hinzu       
        if step <=5:
            self.handle_room_description()
            #Dieser Schritt printet je nach Events die Raumbeschreibung
        if step <= 6:
            self.generate_roomobjects()
            #Fügt keywords der Objekte (Fenster, Türen u.Ä.) und NPCs (Wachen und mehr) hinzu    
        else:
            log("Build error.", "error")
            
########################### FUNKTIONEN WELCHE VON BUILD AUFGERUFEN WERDEN UM OBJEKTE ZU LADEN UND KEYWORDS ZU GENERIEREN#############################################

    def create_events(self):
        #dies ist eine Liste welche alle Events enthält, die für den Speicherstand relevant sind. 
        #mit diesen Werten wird überprüft, in welchem Zustand sich das Spiel befindet, z.B. ob eine Truhe
        #bereits geöffnet wurde, wenn man wieder zu ihr geht, welche Türen geöffnet und Bösse besiegt wurden
        #Der Wert False steht für "ist noch nicht passiert/erreicht worden"    
        events ={
        #Schlafsaal
            'ist_aufgestanden':             False,
            'daemontuer_offen':             False,
            'robe_und_sandalen_gefunden':   False,
            'truhe_gesichtet':              False,
        #Steinrätsel
            'falkenplatte_gefunden':        False,
            'morgenplatte_gefunden':        False,
            'zeichenplatte_gefunden':       False,
            'steintuer_offen':              False,
        #Hummusrätsel
            'hummus_gefunden':              False,
            'sesam_gefunden':               False,
            'gab_hummus':                   False,  
        #bibliothek
            'tutorial_done':                False,
            'nabu_vertrieben':              False, #ändert bspw. Nabus Raumbeschreibung
            'schalter_66_flipped':          False, #ändert den Bücherschrank in dem Raum x4y5
            'schalter_46_flipped':          False, 
            'truhe_33_taken':               False, #beinhaltet das Weisheitsitem Glückskeks
            'truhe_37_taken':               False, #beinhaltet das Schikaneitem 100 schlagfertige Antworten für den Alltag
            'truhe_66_taken':               False, #beinhaltet das Rhetorikitem Rhetorik fürDummys und Fortgeschrittene
            'spoken_to_blockway':           False,
        #wachkammer
            'truhe_13_gesichtet':           False, #verhindert, dass man die Truhe ansprechen kann bevor man sie sichtet
            'truhe_13_taken':               False,
            'truhe_63_taken':               False, 
            'truhe_372_taken':              False,
            'barriere_down':                False,
            'schalter_15_flipped':          False,
            'wache_weg':                    False}
        #Damit beim speichern der Events in ein Textfile die Reihenfolge gesichert wird (dicts sind ungeordnet), sortiere ich sie beim Speichern nach Alphabet.
        #Beim einlesen der dicts muss daher die alphabetische Reihenfolge der Events lklar sein, daher:
        save_order=[
            "barriere_down",
            "daemontuer_offen",
            "falkenplatte_gefunden",
            "gab_hummus",
            "hummus_gefunden",
            "ist_aufgestanden",
            "morgenplatte_gefunden",
            "nabu_vertrieben",
            "robe_und_sandalen_gefunden",
            "schalter_15_flipped",
            "schalter_46_flipped",
            "schalter_66_flipped",
            "sesam_gefunden",
            "spoken_to_blockway",
            "steintuer_offen",
            "truhe_13_gesichtet",
            "truhe_13_taken",
            "truhe_33_taken",
            "truhe_372_taken",
            "truhe_37_taken",
            "truhe_63_taken",
            "truhe_66_taken",
            "truhe_gesichtet",
            "tutorial_done",
            "wache_weg",
            "zeichenplatte_gefunden"]
        return events, save_order

    def generate_player(self):
        #Diese Funktion lädt ein Spielerobjekt mit Hilfe des Namens, welcher im Intro festgestellt wird. 
        #Abhängig davon, ob schon ein Speicherstand existiert oder nicht wird dieser geladen
        #die einzelnen Daten für einen Spieler entnehme ich aus seinem Speicherstand-.txt oder erstelle neue (Start)Werte
        savestate="unfound" #setzt den aktuellen Status, ist also "nicht gefunden", solange kein matchender Speicherstand in dem "savefile" Ordner gefunden wurde
        mypath = 'savefiles/' #hier befinden sich die Speicherstände .txt Dateien
        #falls kein saveflies ordner entsteht, wird er mit os makedirection erstellt
        if not os.path.exists(mypath):
            os.makedirs(mypath)
        #Diese for-Schleife schaut, solange keine gefunden wird, ob es einen savestate gibt:
        for filename in os.listdir(mypath):
            if savestate=="unfound":
                splitname = os.path.splitext(filename)
                if splitname[0] == self.name:
                    savestate="found"
                    print(u"\nSpeicherstand gefunden!\nWillkommen zurück, {x}!\n".format(x=self.name))
                    #die Countdown Funktion benutzt das "time" Modul, um eine kurze künstliche Wartezeit einzufügen. Sie kann mit einem Parameter, welcher
                    #Sekundenzahlen entspricht, aufgerufen werden
                    misc.countdown(0.5)
                    #wenn der Username gefunden wird, soll er das Spielerobjekt hier laden
                    path=mypath+self.name+".txt"
                    #mithilfe des gefundenen Speicherstandes kann die Funtion aus dem Speicherstand, am newline getrennt, 
                    #die einzelnen Werte entnehmen, die für das Objekt benötigt sind
                    f = codecs.open(path, encoding="utf-8")
                    data = f.read()
                    f.close()
                    splitted = data.split("\n")
                    username=splitted[0]
                    loadedmap = int(splitted[1])
                    x=int(splitted[2])
                    y=int(splitted[3])
                    lvl=int(splitted[4])
                    exp=int(splitted[5])
                    hpcur=int(splitted[6])
                    hpmax=int(splitted[7])
                    stat=splitted[8]
                    rh=int(splitted[9])
                    sc=int(splitted[10])
                    wh=int(splitted[11])
                    #equipment und inventar sind im einzelnen nochmals durch  space getrennt, daher splittet und iteriert die Funktion in dem Fall erneut über die Elemente
                    equip=[]
                    for item in splitted[12].split():
                        if item !=" ":
                            equip.append(item.lower())
                    invlist=[]
                    for item in splitted[13].split():
                        if item != " ":
                            invlist.append(item.lower())
                    rh_off=int(splitted[14])
                    rh_def=int(splitted[15])
                    sc_off=int(splitted[16])
                    sc_def=int(splitted[17])
                    wh_off=int(splitted[18])
                    wh_def=int(splitted[19])
                    failed_tries=[]
                    #dasselbe gilt für die Events (siehe auch "create_events" oben)
                    for item in splitted[20].split():
                        if item != "":
                            failed_tries.append(item)
                    eventlist = splitted[21].split()
                    as_bool=[]
                    for elem in eventlist:
                        #eventstati werden intern als bool abgefragt, in der txt aber als 0 oder 1 gespeichert, daher wird hier konvertiert
                        if elem == "0":
                            as_bool.append(False)
                        else:
                            as_bool.append(True)
                    #wenn die Länge gleich ist, wurde richtig eingelesen und die bool Werte können mit den events "gezipped", also zu einem dictionary, zusammen gebunden werden
                    if len(as_bool) == len(self.create_events()[1]):
                        event_dict = dict(zip(self.create_events()[1], as_bool))
                    else:
                        1
                        #log("Generating player from savefile failed at event ordering...", "error")
        if savestate=="unfound":
            #wenn kein savestaste gefunden wurde handelt es sich um einen neuen Spieler
            print(u"\nWillkommen in Kardunias, {x}!\n".format(x=self.name))
            misc.countdown(0.5)
            #Startcharacter playerobjekt wird erstellt:
            #die einzelnen Attribute (im Folgenden auch stats) sind in "dev/player_obj_template.txt" erklärt und werden alle benötigt, um ein playerobjekt zu laden
            username = self.name
            loadedmap = 1
            x = 6
            y = 2
            lvl = 1
            exp = 0 
            hpcur = 15
            hpmax = 15
            stat = "Gesund"
            rh = 6
            sc = 6
            wh = 6 
            equip = []
            invlist = []
            rh_off = 0
            rh_def = 0 
            sc_off = 0 
            sc_def = 0
            wh_off = 0
            wh_def = 0 
            failed_tries = []
            event_dict = self.create_events()[0]
        self.user = playerfuncs.Player(self.name, loadedmap, x, y, lvl, exp, hpcur, hpmax, stat, rh, sc, wh, equip, invlist, rh_off, rh_def, sc_off, sc_def, wh_off, wh_def, failed_tries, event_dict)

    def add_general_options_to_keylist(self):
        #Dinge wie speichern, spiel beenden, sich umschauen oder das Menü zu öffnen sollen immer möglich sein und werden hier in die keyword liste der engineklasse hinzugefügt
        general_keyword_list = [u"speichern", u"beenden", u"menü",u"inventar",u"inventory","inv", u"umschauen", "schaue", "umgucken", "gucke", self.user.name.lower(), u"anschauen", u"untersuchen", u"inspizieren", u"umsehen", u"umsehe", u"sehe", "hilfe"]
        self.keyword_list[0] = general_keyword_list
        #Die 'ausführende' Logik, wenn einer dieser Begriffe erkannt wird, befindet sich in handle_general_input
        #als nächstes baut build inventar optionen auf

    def add_inv_options_into_keylist(self):
        #fügt inventar keywords hinzu, so dass diese angenommen werden und eine "legale" Eingabe sind 
        inv_keyword_list = [u"kombinieren", u"kochen", u"verbinden"]
        #ursprünglich sollte equip und inventar getrennt sein, wird aber zusammengefasst, genauer: equip wird derzeit garnicht verwendet
        equip_inv_list = self.user.equip + self.user.invlist   
        if len(equip_inv_list) != 0:
            #pro gefundenem Item soll die Liste verlängert werden
            for item in equip_inv_list:
                try:
                    inv_keyword_list.append(item).lower()
                except AttributeError:
                    log("an item was wrong attribute " + str(type(item)) + " " + str(item), "error")
        self.keyword_list[1] =inv_keyword_list
        #Die 'ausführende' Logik, wenn einer dieser Begriffe erkannt wird, befindet sich in handle_inv_input
        #als nächstes baut build Raumobjekte und entsprechende Himmelsrichtungsptionen auf

    
    def generate_loc(self):
        #Schaut sich die Koordinatenposition des Spielers an und lädt den korrespondierenden Raum aus den Mapdaten
        #Hier werden die Mapdaten (in tsv Format) mit dem Panda Modul geladen. Die einzelnen Zellen sind Unicode-Daten, getrennt durch ein Tab, und wurden händisch 
        #in libreoffice erstellt
        #da es drei Maps gibt, wird die korrespondiernde Map geladen, indem auf das zuvor erstelle user objekt geschaut wird
        map_data = pd.read_csv('data/map_{x}.csv'.format(x=self.user.map), sep='\t')
        #als nächstes wird die aktuell relevante Zeile gefunden, also die in der sich der Spieler befindet
        relevant_x_rows = map_data[map_data["x"] == self.user.x]
        relevant_row = relevant_x_rows[relevant_x_rows["y"] == self.user.y]
        #Nun können alle wichtigen Information wie Ausgangstypen, Beschreibung und Objekte gefunden werden
        room_type=str(relevant_row["typ"]).split()[1]
        for elem in relevant_row["furniture"]:
            furniture = []
            for decor in elem.split():
                #wenn decor "-" ist gibt es keine eintrag, und furniture bleibt leer
                if decor != "-":
                    #wenn nicht, wird aus jedem Element in dieser Spalte ein Furniture Objekt erstellt (siehe auch furniture.py)
                    decor_obj = Furniture.Furniture(decor.lower(), self.user)
                    furniture.append(decor_obj)
                    #nun exisstiert eine Liste an Schränken, Fenstern, NPCs u.Ä. für den Raum in dem der Spieler ist
        #Als nächstes werden Monster, die erscheinen können, ihre Wahrscheinlichkeitschance in diesem Raum und string texte für die Beschreibung und das Umschauen geladen:
        for mob in relevant_row["mobs"]:
            mobs=mob.split()
        for elem in relevant_row["mob_chance"]:
            chance=elem
        for elem in relevant_row["umschauen"]:
            umschauen = str(elem)
        for elem in relevant_row["status"]:
            status = elem
        for elem in relevant_row["beschreibung"]:
            desc = elem
        for elem in relevant_row["alt_beschreibung"]:
            alt_beschreibung = elem            
        #es wird jeweils über die Zelle iteriert und nur das letzte Listenelement letztendlich zugewiesen
        #obwohl nur ein Status/String relevant ist: so konnte ich sicher stellen, dass nur noch der string in bspw. "umschauen" ist
        #und nicht das dataframeobjekt
        possible_actions=[]
        #der Raumtyp enthält immer die Großbuchstaben der Himmelsrichtungen, die von diesem Raum aus möglich sein sollen
        #hier füge ich pro möglicher Himmelsrichtung verschiedene Beschreibungen dieser Richtung an die keywordlist
        if "N" in room_type:
            wohin=[u"norden", u"nördlich", u"oben", u"vorne", u"hoch", u"vorwärts"]
            possible_actions.extend(wohin)
        if "S" in room_type:
            wohin=[u"süden",u"südlich", u"unten", u"hinten"]
            possible_actions.extend(wohin)
        if "W" in room_type:
            wohin=[u"westen", u"westlich", u"links"]
            possible_actions.extend(wohin)
        if "O" in room_type:
            wohin=[u"osten", u"östlich", u"rechts"]
            possible_actions.extend(wohin)
        #nun da alle relevenaten Informationen vorliegen, wird das Raumobjekt in einer praktischen Raumklasse gelagert
        self.room = roomfuncs.Room(self.user.x, self.user.y, room_type, furniture, mobs, chance, umschauen,status,desc,alt_beschreibung)
        #und die legalen Möglichkeiten werden erweitert:
        self.keyword_list[2] = possible_actions
        #Die ausführende Logik, wenn eine Richtung angegeben und erkannt wird, befindet sich in handle_location_based_input
        #Als nächstes soll, wenn ein Raum geladen wird, testen ob Monster kommen können (nächster build step)

    def check_battle(self):
        #Mithilfe des random Moduls wird eine zufällige Zahl ausgesucht, falls diese geringer ist als die nun geladene Chance, spawnt ein Gegner und der Kampf beginnt
        if random.randint(0,99) < self.room.mobchance:
            enemy = self.room.mobs[random.randint(0,len(self.room.mobs)-1)]
            #Da meist drei verschiedene Gegner pro Ort möglich sind, wird hier per Zufall entschieden, welcher Gegner es ist. 
            #Da die Länge höher als der Listenindex ist (len beginnt bei 1 im Gegensatz zu listindex), ist die maximale zufällige Zahl die Länge minus eins
            #Der User wird andere EXP, HP, LVL und Fähigkeiten nach dem kampf haben, daher returned die Battle Funktion Pipeline in jedem Fall ein verändertes Spieler Objekt
            
            #Der Dialog mit den Bössen soll vor dem Kampf passieren, daher wird der Dialog in diesen Fällen hier schon aufgerufen bevor Furniture und NPCs geladen werden.
            if enemy == "nabu":
                if self.user.events["nabu_vertrieben"] == False:
                    #Wenn der Gegner Nabu nocht nicht vertrieben wurde, wird die Dialog Funktion aufgerufen, und der Kampf daraufhin gestartet
                    misc.write(u"Mitten in dem Raum blockiert ein bekanntes Gesicht den Weg aus der dunklen Bibliothek heraus.\n Es ist Nabu, mit einem finsteren Lächeln auf den Lippen.\nEs sieht aus, als sei er von etwas besessen!")
                    misc.dialog(self.user.name, "Besessener_Nabu", 0, [])
                    misc.write("")
                    bt.battle(self.user, enemy)    
                    #Nach dem Kampf wird dieses Event true gesetzt, falls das Spielerobjekt nicht mit der "dead" Eigenschaft zurückkommt
                    if self.user.stat != "dead":
                        self.user.events["nabu_vertrieben"] = True  
                    else:
                        #wenn der spieler starb, wird er auf gesund zurückgesetzt und mit build in neuer Umgebung "respawned"
                        self.user.stat ="Gesund"
                        self.build(2)
                        #als nächstes sollte build ab den Skillkeywords nochmal keywords generieren, falls durch ein Level Up etwas neues hinzukam
                        #in dem Fall könnte auch wieder ein Gegner erscheinen
            #Bei diesem Boss passiert dasselbe, nur mit anderem Text
            elif enemy =="Marduk":
                misc.write(u"Der Sturm wütete um Marduk und {x} herum. Es began...".format(x=self.user.name))
                misc.countdown(2)
                bt.battle(self.user, enemy)
                if self.user.stat != "dead":
                    outro.outro(self.user)
                else:
                    self.user.stat ="Gesund"
                    self.build(2)  
            #Bei default Gegnern kommt der default Text:
            else:
                misc.write(u"Doch während das passierte, tauchte plötzlich ein Gegner auf!")
                self.user = bt.battle(self.user, enemy)
                if self.user.stat == "dead": 
                    self.user.stat ="Gesund"
                    self.build(2)  
            
    def add_skill_options_to_keylist(self):
        #Liest am Spielerobjekt, welche Fähigkeiten möglich sind, und fügt diese den Möglichkeiten zu
        #Die Skillbeschreibungen liegen in der Datei "data/skills.txt"
        #die "Formeln", was die Skills können, befindet sich in "dev/ideas.txt", wird in "battle.py" ausgeführt
        #die Logik wird in Kämpfen einzeln abgehandelt. Wenn Skill Keywords außerhalb von Kämpfen aufgerufen werden, geben sie eine Beschreibung. 
        skill_list = []
        rh_off_dict = { 1:u"Suggerieren", 2:u"Überzeugen", 3:[u"Rede",u"halten"]}
        skill_list.append(rh_off_dict) #Die offensiven Fähigkeiten des Rhetorik Baumes
        rh_def_dict = { 1:u"Intuition", 2:u"Nachschlagen", 3:u"Wikipedia"}
        skill_list.append(rh_def_dict) #Die defensiven Fähigkeiten des Rhetorik Baumes

        sc_off_dict = {1:[u"Trick-17", "Trick", "17"], 2:u"Tyrannisieren", 3:u"Wutrede"}
        skill_list.append(sc_off_dict) #Die offensiven Fähigkeiten des Schikane Baumes
        sc_def_dict = { 1:["32-Heb-Auf","32","heb","auf", "32-heb-auf"], 2:u"Posieren", 3:[u"Sibirischer", "Doppelbluff"]}
        skill_list.append(sc_def_dict) #Die defensiven Fähigkeiten des Schikane Baumes

        wh_off_dict = {1:u"Demoralis", 2:[u"Anima", "Vincere"], 3:u"Leckerli"}
        skill_list.append(wh_off_dict) #Die offensiven Fähigkeiten des Weisheits Baumes
        wh_def_dict = { 1:u"Abrakapflaster", 2:[u"3xSchwarzer-1.-Hilfe-Kasten","hilfe","Kasten"], 3:[u"Simsala-Wundheilzauber", "Simsala", "Wundheilzauber"]}
        skill_list.append(wh_def_dict) #Die defensiven Fähigkeiten des Weisheit Baumes
        skill_keyword_list = []
        position = 14
        for skill_dict in skill_list:
            skill_lvl = int(self.user.make_attribute_list()[position]) #value ist jewelis 0,1,2,3, je nach Stufe
            if skill_lvl in skill_dict:
                #zunächst wird überprüft, ob es ein-wort skills sind, wenn ja, werden diese an die Skill Möglichkeiten hinzugefügt
                if type(skill_dict[skill_lvl]) == str:
                    skill_keyword_list.append(skill_dict[skill_lvl].lower())
                elif type(skill_dict[skill_lvl]) == list:
                    #in dem Fall, dass es eine Liste ist, sollen alle einzeln möglich und legal als Keyword hinzugefügt werden
                    #beim input der inventar worte wird die korrekte "zusammenfassung" verlangt werden (bsp "Rede"+"halten" müssen beide im input vorgekommen sein)
                    for word in skill_dict[skill_lvl]:
                        skill_keyword_list.append(word.lower())
                else:
                    log("Unabgefragter Typ beim kreieren der Skill Keywods gefunden..", "error")
            #in der nächsten position, also an der stelle 14, 15, dann 16, stehen in der spieler attribute_list die Werte, wie hoch der jeweilige Skill beherrscht wird
            position = position+1
        self.keyword_list[3] = skill_keyword_list
        #die ausführende Logik befindet sich in handle_skill_input
        #nach dem die skill basierten keywords als Möglichkeit hinzugefügt wurden, kann build als vorletztes immer die Raumbeschreibung aufrufen:

    def handle_room_description(self):
        #Überprüft die Events um ggf. noch etwas am Raum zu verändern: Wenn z.B. ein Objekt nicht mehr in dem Raum steht, soll eine andere Beschreibung passieren:
        #Schlafzimmer des Königs
        if self.room.x == 6 and self.room.y == 2 and self.user.map == 1 and self.user.events["ist_aufgestanden"] == True:
            self.room.change_status()
        #Küche
        elif self.room.x == 6 and self.room.y == 4 and self.user.map == 1 and self.user.events["hummus_gefunden"] == True:
            self.room.change_status()
        #Westflügel
        elif self.room.x ==5 and self.room.y == 3 and self.user.map == 1 and self.user.events["steintuer_offen"] == True:
            self.room.change_status()
        #Nabus Zimer:
        elif self.room.x==5 and self.room.y == 2 and self.user.map == 1 and self.user.events["nabu_vertrieben"] == True:
            self.room.change_status()
        #bibliothek:
        #truhen:
        elif self.room.x== 3 and self.room.y==7 and self.user.map == 1 and self.user.events["truhe_37_taken"] == True:
            self.room.change_status()
        elif self.room.x ==6 and self.room.y==6 and self.user.map == 1 and self.user.events["truhe_66_taken"] == True:
            self.room.change_status()
        #schalter: #ändern etwas NICHT in dem Raum in dem sie selbst sind
        elif self.room.x== 4 and self.room.y==5 and self.user.map == 1 and self.user.events["schalter_66_flipped"] == True:
            self.room.change_status()
        elif self.room.x ==3 and self.room.y==5 and self.user.map == 1 and self.user.events["schalter_46_flipped"] == True:
            self.room.change_status()
        #npc:
        elif self.room.x==3 and self.room.y==4 and self.user.map == 1 and self.user.events["spoken_to_blockway"] == True:
            self.room.change_status()
        #die schienen in der wachkammer:
        elif self.room.x==5 and self.room.y==3 and self.user.map == 2 and self.user.events["schalter_15_flipped"]==True:
            self.room.change_status()
        elif self.room.x==6 and self.room.y==3 and self.user.map == 2 and self.user.events["schalter_15_flipped"]==True:
            self.room.change_status()
        elif self.room.x==4 and self.room.y==6 and self.user.map == 2 and self.user.events["wache_weg"]==True:
            self.room.change_status()
        elif self.room.x==4 and self.room.y==2 and self.user.map == 2 and self.user.events["barriere_down"]==True:
            self.room.change_status()
        #everywhere: print desc gibt die Raumgeschreibung, falls Status durch change auf 2 oder 1 gesetzt wurde, einen altenativen String aus den Mapdaten/CSV:
        self.room.print_desc()
        #als letztes wird build immer generate_roomobjects, also die "Furnitures" und NPCs generieren

    def generate_roomobjects(self):
        #für alle Furniture Objekte erstellt diese Funktion Objekte, welche dann Beschreibungen und Synonyme an legalen Keywords enthalten
        #pro furniture (z.B. eine verschlossene Tür) werden alle Synonyme an die "all furniture keyword list" weitergeleitet, 
        #damit diese dann an die generelle keyword liste, welche der Spieler benutzen kann, appended wird

        all_furniture_keyword_list = []
        for decor in self.room.furniture:
            #die Furniturelklasse enthält die Funktion "compute keywords" welche Möglichkeiten im Sinne von keywords hinzufügen und abziehen kann
            decor.compute_keywords()
            #Die List Comprehension fügt für alle keywords, die durch compute nun decor attribut sind, die kleingeschriebene Version in die Liste
            furniture_keyword_list = [word.lower() for word in decor.furn_keywords]

            #zusätlich schaue ob in decor.subtract eine liste vorhanden ist; falls ja: entferne alle dort erwähnten elemente aus der globalen keyword_list
            #somit kann beispielsweise eine abgeschlossene Tür im Norden das bereits hinzugefügte "Norden" wieder unmöglich machen, bis ihr event "True" ist
            if decor.subtract != []:
                for to_subtract in decor.subtract:
                    for keyword_sublist in all_furniture_keyword_list:
                        #mit einem try und except Block werden die dabei entstehenden ValueErrors, wenn der String also garnicht in der Liste war, abgefangen und dank "pass"
                        #einfach weiteriteriert
                        try:
                            keyword_sublist.remove(to_subtract)
                        except ValueError:
                            #log("Keyword {x} konnte nicht aus keyword_list entfernt werden.".format(x=to_subtract.encode('utf-8')), "error")
                            pass 
                    for keyword_sublist in self.keyword_list:
                        try:
                            keyword_sublist.remove(to_subtract)
                        except ValueError:
                            #log("Keyword {x} konnte nicht aus keyword_list entfernt werden.".format(x=to_subtract.encode('utf-8')), "error")
                            pass 

            #Hinzufügen der Listen an die große Engine Liste              
            all_furniture_keyword_list.append(furniture_keyword_list)

        #Wenn ein Raum keine Furniture hat, ist es einfach eine leere Liste        
        self.keyword_list[4] = all_furniture_keyword_list             
    #Nun ist build fertig und es kann der nächste input entgegen genommen werden!

########################### FUNKTIONEN ZUM USER INPUT VERARBEITEN#############################################

    def handle_general_input(self, choice):
        # 1.der speicherstand wird mit "speichern" gespeichert, die Funktion befindet sich in classes/playerclass.py
        if u"speichern" in choice:
            self.user.save()
        # 2.Mit dem eigenen Namen, Menü oder Inventar wird eine Übersicht des Spielers aufgerufen. Die Funktion befindet sich in classes/playerclass.py
        elif self.user.name.lower() in choice or u"menü" in choice or u"inventar" in choice or u"inventory" in choice or "inv" in choice:
            self.user.give_info()
        # 3.Wenn das Spiel beendet werden soll, wird sicherheitshalber eine Ja Nein Frage vorhergeschoben, falls ein positiver Input kommt, wird das Programm mit quit beendet
        elif u"beenden" in choice:
            answer = misc.yes_no("Wirklich Kardunias beenden? Ungespeicherter Fortschritt geht verloren!")
            if answer == True:
                quit()
            else:
                print("Gut, dann nicht!")
        # 4. Überall soll eine konkrete, neue Beschreibung des Raumes kommen, wenn sich umgeschaut wird:
        elif u"umschauen" in choice or u"umsehen" in choice or u"umsehe" in choice or "gucke" in choice or "sehe" in choice or "umgucken" in choice or "schaue" in choice:
            #Im Schlafsaal des Königs soll das Umschauen das Finden der Truhe ermöglichen
            if self.room.x == 6 and self.room.y == 2 and self.user.map == 1:
                if self.user.events["truhe_gesichtet"] == False:
                    self.user.events["truhe_gesichtet"] = True
                    self.build(6)
            #In der Wachkammer soll eine Truhe erst nach dem umschauen auffindbar sein
            elif self.room.x == 1 and self.room.y==3 and self.user.map == 2:
                if self.user.events["truhe_13_gesichtet"]==False:
                    self.user.events["truhe_13_gesichtet"]=True
                    self.build(6)

            print(self.room.umschauen)
        # 5. Wenn "x" anschauen o.Ä. gesagt wird, und dann nochmal nachgefragt wird, tippt der Spieler hoffentlich den gemeinten Gegenstand, sodass handle_input dahin leitet
        #    und im handle_furniture_input die gewünschte Interaktion startet
        elif u"anschauen" in choice or u"untersuchen" in choice or u"inspizieren" in choice:
            print("Was wollte ich denn nochmal genau {x}...?".format(x=choice[0]))
        elif u"hilfe" in choice:
            helper.give_help(self.user.events)

    def handle_skill_input(self, choice):
        #Jeder Skill hat eine eigene Beschreibung in data/skills.txt, welche durch Absätze getrennt sind und somit hier bei Ansprache aufgerufen werden können
        #diese Aufrufe beziehen sich auf out-of-combat Nennungen der Fähigkeiten (für Aufrufe und Logik im Kampf siehe "source/battle.py")
        #zunächst wird die Datei mit codecs / utf-8 geöffnet, um Umlaute wie gewünscht vorliegen zu haben 
        my_f = codecs.open('data/skills.txt', "r", "utf-8")
        f = my_f.read().split("\n")
        my_f.close()
        #wenn in der choice liste, die der user eintippte, die relevanten Worte vorkommen und der Spieler den Skill auf der Stufe besitzt, printe die jeweilige Beschreibung
        if "suggerieren" in choice and self.user.rh_off == 1:             
            print(f[0])
        elif u"überzeugen" in choice and self.user.rh_off == 2:   
            print(f[1])
        elif u"rede" in choice and u"halten" in choice and self.user.rh_off == 3:
            #hier wird auch noch verlangt, dass das 2. word vorkam
            print((f[2]))
        elif u"intuition" in choice and self.user.rh_def == 1:
            print((f[3]))
        elif u"nachschlagen" in choice and self.user.rh_def == 2:
            print((f[4]))
        elif u"wikipedia" in choice and self.user.rh_def == 3:
            print(f[5])

        elif u"trick-17" in choice and self.user.sc_off == 1:
            print((f[6]))
        elif u"trick" in choice and u"17" in choice and self.user.sc_off == 1:
            print((f[6]))
        elif u"tyrannisieren" in choice and self.user.sc_off == 2:
            print(f[7])
        elif u"wutrede" in choice and self.user.sc_off == 3:
            print((f[8]))

        elif u"32-heb-auf" in choice and self.user.sc_def == 1:
            print((f[9]))
        elif u"32" in choice and u"heb" in choice and u"auf" in choice and self.user.sc_def == 1:
            print((f[9]))
        elif u"posieren" in choice and self.user.sc_def == 2:
            print((f[10]))
        elif u"sibirischer" in choice and u"doppelbluff" in choice and self.user.sc_def == 3:
            print((f[11]))

        elif u"demoralis" in choice and self.user.wh_off == 1:
            print((f[12]))
        elif u"anima" in choice and u"vincere" in choice and self.user.wh_off == 2:
            print((f[13]))
        elif u"leckerli" in choice and self.user.wh_off == 3:
            print(f[14])

        elif u"abrakapflaster" in choice and self.user.wh_def == 1:
            print(f[15])
        elif u"3xschwarzer-1.-hilfe-kasten" in choice and self.user.wh_def == 2:
            print(f[16])
        elif u"hilfe" in choice and "kasten" in choice and self.user.wh_def == 2:
            print(f[16])
        elif u"simsala-wundheilzauber" in choice and self.user.wh_def == 3:
            print(f[17])
        elif "wundheilzauber" in choice and self.user.wh_def == 3:
            print(f[17])
        else:
            return self.try_again_text()
            #Möglicherweise landete handle_input in dieser Funktion, da einer der Keywords (z.B. heb) akzeptiert wurde, aber auf kam nicht vor #
            #man braucht heb und auf, um die Skill funktion zu schreiben, da sonst wahrscheinlich etwas anderes gewünscht war, die Kombination wird aber erst hier verlangt

    def handle_inv_input(self, choice):
        #Aus handle_input() wissen wir, dass das Item in "choice" in der inventory keyword_list enthalten sein muss
        #d.h. man braucht nicht mehr zu prüfen ob choice ein legales Wort ist
        #Wenn etwas kombiniert werden soll, wird zuerst geprüft, ob ein Kombinier-Keyword und 2 weitere Worte vorkommen, die dann hoffentlich Items sind , und nichts anderes
        if len(choice) == 3 and ((u"kombinieren" in choice and not "kochen"  in choice and not "verbinden" in choice) or
                                (u"kombinieren" not in choice and "kochen" in choice and not "verbinden" in choice) or 
                                (u"kombinieren" not in choice and not "kochen" in choice and "verbinden" in choice)):
            #wenn das der Fall ist, dann wurde versucht, zwei Dinge zu kombinierenden (items direkt angegeben), es interessieren fortan nur noch die Items, daher wird der Rest entfernt
            if u"kombinieren" in choice:
                choice.remove("kombinieren")
            if "kochen" in choice:
                choice.remove("kochen")
            if "verbinden" in choice:
                choice.remove("verbinden")
            #jetzt sollten sich nur noch Items in Chioce befinden, so dass Objekte der Items erstellt werden können (s. classes/itemclass.py)
            if choice[0] in self.user.invlist and choice[1] in self.user.invlist:
                item1_obj = self.load_item(choice[0])
                item2_obj = self.load_item(choice[1])
                #Wenn die Items als Objekt vorliegen, geht aus der CSV der einzelnen Items hervor, ob und mit was und zu was kombiniert werden kann 
                product = item1_obj.check_combination(item1_obj, item2_obj, self.user.name)
                #Das Produkt ist ein leerer String, wenn es keines gibt. Falls doch, wird beschrieben, was erzeugt wurde und was verloren ging
                if product != "":
                    misc.write("Du erhältst {x}! Dabei sind {y} und {z} verbraucht worden!".format(x=product.title(), y=choice[0].title(), z=choice[1].title()))
                    #Das muss nicht nur gesagt werden, sondern auch in der Inventarliste des Spielers passieren:
                    self.user.invlist.remove(choice[0])
                    self.user.invlist.remove(choice[1])
                    self.user.invlist.append(product)
                    #Damit die Inventar Keywords neu generiert werden, wird build ab step 1 ausgeführt
                    self.build(1)
                else: #der inhaltliche Fehlertext, falls die Kombination zu nichts führte:
                    print(u"Das hat wohl nichts verändert. . .")
            else: #Der Fehlertext, falls die Funktion in dieser Bedingung landete; die Dinge in "choice" aber keine Items sind (z.B. kombiniere norden süden)
                print(u"Das war nicht im Inventar und wurde akzeptiert. Bitte items angeben!")

        elif u"kombinieren" in choice or "kochen" in choice or "verbinden" in choice:
        	#Wenn eine andere Anzahl an Dingen akzeptiert wird, aber auf jeden Fall ein Verbindungswort gefunden wird, soll gefragt werden welche Items genommen werden sollen
            if len(self.user.invlist) > 1:
                print("Du kannst folgende Dinge ganz beliebig kombinieren:")
                #Damit das Inventar übersichtlich dargestellt wird, füge ich jedes Inventar Item mit Komma und Space einem leeren String an
                itemlist=""
                for elem in self.user.invlist:
                    #die items sollten dem spieler schön angezeigt werden, daher:
                    elem  = elem.replace("_", " ")
                    elem  = elem[0].upper()+elem[1:]
                    itemlist = itemlist+elem+", "
                print(itemlist[:-2]) #das letzte Komma soll nicht angezeigt werden, daher -2                
                print("Welche beiden Items willst du kombinieren? Gib zwei der Gegenstände an: (Groß/Kleinschreibung und Reihenfolge sind egal!)")
                raw_is = input()
                #keywordlist[1] ist das inventar, die ersten 3 keywords sind immer "kombinieren/kochen/verbinden, die sollen an dieser stelle nich mehr matchen"
                #falls die Eingabe etwa nochmal ein "Koche" enthält, daher wird erst ab dem dritten Objekt der key_word liste der Parser nach Itemnamen suchen
                item1 = parse.check_input(raw_is, self.keyword_list[1][3:]) 
				#Mit dieser if/elif/elif wird unterschieden, ob zwei Items angegeben und akzeptiert wurden (dann kann direkt wie oben kombiniert werden)
				#eine gefunden wurde, oder gar keine
                if len(item1) == 2:
                    if item1[0] in self.user.invlist and item1[1] in self.user.invlist:
                        item1_obj = self.load_item(item1[0])
                        item2_obj = self.load_item(item1[1])
                        product = item1_obj.check_combination(item1_obj, item2_obj, self.user.name)
                        if product != "":
                            misc.write("Du erhältst {x}! Dabei sind {y} und {z} verbraucht worden!".format(x=product.title(), y=item1[0].title(), z=item1[1].title()))
                            self.user.invlist.remove(item1[0])
                            self.user.invlist.remove(item1[1])
                            self.user.invlist.append(product)
                            self.build(1)
                        else:
                            print(u"Das hat wohl nichts verändert. . .")
                    else:
                        log("Das war nicht im Inventar und wurde akzeptiert.", "error")
                elif item1 == []: #bei keinem akzeptiertem Item wird nachgefragt, ob überhaupt kombiniert werden soll, falls ja wird diese Funktion wiederholt 
                    continue_combining = misc.yes_no(u"Das hat leider nicht geklappt. Möchtest du etwas kombinieren? (Ja oder Nein?)")
                    if continue_combining == True:
                        return self.handle_inv_input(choice)
                elif len(item1) == 1: # bei einem Gegenstand wird folglich nach dem zweiten gefragt:
                    print("Nun gib den Namen des zweiten Gegenstandes ein:")
                    raw_is_now = input()
                    item2 = parse.check_input(raw_is_now, self.keyword_list[1][3:])
                    #Wenn kein zweites Item gefunden wird, wird gefragt ob von vorne kombiniert werden soll und die Funktion wiederholt
                    if item2 == []:
                        continue_combining = misc.yes_no(u"Das besitzt du leider nicht oder wurde nicht erkannt. Willst du nochmal von vorne versuchen, etwas zu kombinieren (Ja oder Nein)?")
                        #Als Entscheidungshilfe wird nochmal die Itemliste geprinted
                        print(itemlist)
                        if continue_combining == True:
                            return self.handle_inv_input(choice)
                    #Ein Item plus zwei wären drei, es können allerdings immer nur 2 Dinge auf einmal kombiniert werden, daher wird die Funktion erneut aufgerufen
                    elif len(item2) >=2:
                        print("Eins nach dem anderen bitte - wir versuchen's nochmal")
                        misc.countdown(0.5)
                        return self.handle_inv_input(choice)
                    elif len(item2) == 1: #Wenn eines gefunden wird, kann wie oben Objekte erstellt werden, und check_combination aufgerufen
                        #Als nächstes sollen die Items zu ihren Objekten werden
                        item1_obj = self.load_item(item1[0])
                        item2_obj = self.load_item(item2[0])
                        product = item1_obj.check_combination(item1_obj, item2_obj, self.user.name)
                        if product != "":
                            misc.write("Du erhältst {x}! Dabei sind {y} und {z} verbraucht worden!".format(x=product, y=item1[0], z=item2[0]))
                            self.user.invlist.remove(item1[0])
                            self.user.invlist.remove(item2[0])
                            self.user.invlist.append(product)
                            self.build(1)
                        else:
                            print(u"Das hat wohl nichts verändert. . .")
                elif len(item1) >=3: #drei Sachen funktionieren nicht, die Funktion wird erneut gestartet
                    print("Das waren drei Sachen - bitte immer zwei Sachen nennen")
                    misc.countdown(0.5)
                    return self.handle_inv_input(choice)
                else:
                    log("Über die akzeptierte Anzahl an zu kombinierenden Sachen wurde nicht erkannt wie fortgefahren werden soll, Länge ist: " + len(item1) + item1, "error")
            else:
                print("Ich hab leider zu wenig Gegenstände vorhanden um etwas zu kombinieren!")
        else:
            #wenn kombinieren garnicht gewollt ist, wird einfach eine item description angegeben, da auf jeden fall ein item erkannt wurde wenn diese Funktion aufgerufen wurde
            choice = choice[0] #ich gehe einfach mal vom ersten genannten Item aus
            item1_obj = self.load_item(choice)
            print(item1_obj.description)


    def load_item(self,given_name):
    	#Diese Funktion wird durch handle_inv_input aufgerufen, und ein itemname wird als parameter übergeben
    	#dieser Parameter muss in der csv Datei aller Items gefunden werden, sodass aus den dort liegenden Strings ähnlich wie bei den Räumen ein Itemobjekt erstellt wird
        inv_data = pd.read_csv('data/inventory.csv', index_col=0,  sep='\t')
        name = given_name
        #Falls das nicht der Fall ist, soll eine genaue Fehlermeldung erkennen lassen können, was geändert werden muss
        if name not in [elem for elem in inv_data.index]:
            log(str(name), "error")
            log("... " + str(type(name)), "error")
            log("... " + str(inv_data.index), "error")
            log("... " + str(inv_data.index[0]), "error")
            log("... " + str(type(inv_data.index[0])), "error")
            log("... " + "Item name not found!", "error")
        myname = given_name
        mycombines_with = []
        mycombines_to = []
        for col in inv_data:
            if col == "ID":
                myid = int(inv_data[col][name])
            elif col == "Beschreibung":
                mydescription = inv_data[col][name]
            elif col == "Kombination_mit":
                if inv_data[col][name] == "-":
                    mycombines_with = []
                else: 
                    for elem in inv_data[col][name].split():
                        if elem != " ":
                            mycombines_with.append(int(elem))
            elif col == "Kombination_zu":
                if inv_data[col][name] == "-":
                    mycombines_to = []
                else: 
                    for elem in inv_data[col][name].split():
                        if elem != " ":
                            mycombines_to.append(int(elem))
        #combines with könnten 2 Zahlen sein, in dem Fall muss combines to zwei entsprechende Ziele haben, was aus diesen beiden jeweils wird, daher werden diese 
        #eingelesen, auf gleiche Länge überprüft und gezipped 
        if len(mycombines_with) == len(mycombines_to):
            zipped = zip(mycombines_with, mycombines_to)
            mycombines_with_to = dict(zipped)
        else:
            log("mycombines_with has length: " + str(len(mycombines_with)) + " mycombines_to has length: " + str(len(mycombines_to)), "error")
            mycombines_with_to = {}
        #am ende wird das Item Objekt returned, sodass es in handle_inv_input als item1 item1 o.Ä. genutzt werden kann
        return Item(myname, myid, mydescription, mycombines_with_to)



    def handle_location_based_input(self, choice):
        #Gehen in entsprechende, gemeinte Himmelsrichtung (oder anderen Formen) funktioniert, in dem die x oder y Koordinate des Spielers entsprechend
        #hoch oder runter gerechnet wird, und am ende build() mit dem neuen Ort aufgerufen wird und die neue Umgebung lernt
        if u"norden" in choice or u"nördlich" in choice or u"oben" in choice or u"vorne" in choice or "hoch" in choice or u"vorwärts" in choice:
            #an dieser stelle soll die standardmäßige Bewegung beschrieben werden, 
            #allerdings gibt es manche Orte an denen Treppen gegangen wird; oder die map gewechselt wird; daher werden diese Sonderfälle abgefangen:
            if self.room.x == 5 and self.room.y == 3 and self.user.map == 1:
                self.user.y += 1
                print(u"{x} beschreitet die Treppen nach oben in die royale Bibliothek.\n".format(x=self.user.name))
                misc.countdown(0.5) #Nach jeder Bewegungsbeschreibung soll eine halbe Sekunde Pause das Spielerlebniss geschmeidiger gestalten
            elif self.room.x == 4 and self.room.y == 7 and self.user.map == 1:
                self.user.map = 2
                print(u"{x} beschreitet die Treppen nach oben in die Kammer der Wachen.\n".format(x=self.user.name))
                misc.countdown(0.5)            
            elif self.room.x == 4 and self.room.y == 7 and self.user.map == 2:
                self.user.map = 1
                print(u"{x} beschreitet die Treppen nach unten in die royale Bibliothek.\n".format(x=self.user.name))
                misc.countdown(0.5)
            else:
                self.user.y += 1
                print(u"{x} geht nach Norden\n".format(x=self.user.name))
                misc.countdown(0.5)
        elif u"süden" in choice or u"südlich" in choice or u"unten" in choice or u"hinten" in choice:
            if self.room.x == 5 and self.room.y == 4 and self.user.map == 1:
                self.user.y -= 1
                print(u"{x} beschreitet die Treppen nach unten zum royalen Schlafsaal.\n".format(x=self.user.name))
                misc.countdown(0.5)
            elif self.room.x == 5 and self.room.y == 1 and self.user.map == 3:
                self.user.map = 2
                self.user.x = 4
                self.user.y = 2
                print(u"{x} beschreitet die Treppen nach unten in die Kammer der Wachen.\n".format(x=self.user.name))
                misc.countdown(0.5)
            elif self.room.x == 4 and self.room.y == 2 and self.user.map == 2:
                print(u"{x} beschreitet die Treppen nach oben zur Spitze des Turms.\n".format(x=self.user.name))   
                self.user.map = 3
                self.user.x = 5
                self.user.y = 1
                misc.countdown(0.5)       
            else:
                self.user.y -= 1
                print(u"{x} geht nach Süden\n".format(x=self.user.name))
                misc.countdown(0.5)
        elif u"westen" in choice or u"westlich" in choice or u"links" in choice:
            self.user.x -= 1
            print(u"{x} geht nach Westen\n".format(x=self.user.name))
            misc.countdown(0.5)
        elif u"osten" in choice or u"östlich" in choice or u"rechts" in choice: 
            self.user.x += 1
            print(u"{x} geht nach Osten\n".format(x=self.user.name))
            misc.countdown(0.5)
        else:
        	#Die Fehlermeldung, falls ein Keyword generiert und akzeptiert wurde, von handle_input als location-based erkannt wurde, und dann nicht einsortiert wurde
            log(u"Das Keyword wurde generiert, als bewegungsbased erkannt, hat aber keine Bedeutung zugeschrieben bekommen " + str(choice), "error")
        #In jedem Fall wird ab "Umgebung" build aufgerufen:
        self.build(2)

    def handle_furniture_based_input(self, choice):
        #Eine sehr lange input handling Funktion aller Furnitures/
        #1.Zuerst wird abgefragt, in welchem Raum man sich befindet 8damit z.B. zwei Fenster in unterschiedlichen Räumen andere beschreibende Texte beinhalten können)
        #2.Dannach wird immer die Logik aller Gegenstände im Raum bearbeitet, sodass die gewünschten Texte beim interagieren aufgerufen werden 
        #Alle Furniture spezifischen Texte sind in 'classes/furnitureclass.py' lokalisiert und werden von hier aufgerufen

        #Schlafzimmer des Königs:
        if self.room.x == 6 and self.room.y == 2 and self.user.map == 1:
            if self.user.events["ist_aufgestanden"] == False:
            	#wenn man im Bett liegt, soll keine der regulären Möglichkeiten funktionieren, deshalb wird man hier direkt an eine spezifische Funktion weitergeleitet wird
                self.handle_input_if_in_bed(choice)     
            if u"dämontür" in choice or u"dämonentür" in choice or u"tür" in choice or  u"dämon" in choice or u"maske" in choice or u"norden" in choice or u"sicherheitstür" in choice or u"nördlich" in choice or u"oben" in choice or u"vorne" in choice:
                if self.user.events["daemontuer_offen"] == False:
                    Furniture.text_daemontuer("begin")
                    failcount = 0
                    while self.user.events["daemontuer_offen"] == False:
                        #solange die Tür also nicht auf ist, soll nach input gefragt werden, bis das korrekte Passwort gesagt wird
                        user_said=input()
                        user_choices = parse.check_input(user_said, [u"abbrechen", u"stop", "hilfe"])
                        if len(user_choices) > 0:
                            if user_choices[0] == u"abbrechen" or user_choices[0] == u"stop":
                                Furniture.text_daemontuer("stop")
                                break
                            if user_choices[0] == "hilfe":
                                helper.give_help(self.user.events)
                        #Die Lösung ist, die Tür anzuschreien, da sie schwer hört. Das wird überprüft, indem der Input gleich dem upper() input sein soll (also all caps)
                        if user_said.upper() == user_said and user_said != "":
                            Furniture.text_daemontuer("solved")
                            self.user.events["daemontuer_offen"] = True
                            self.user.y += 1
                            print(u"{x} geht nach Norden in den Ostteil des Flures.\n".format(x=self.user.name))
                            self.build(2)
                        else:
                        	#Wenn der Nutzer drei mal nicht darauf kommt (was wahrscheinlich ist), kommt ein zweiter Text, der deutlich macht was passiert:
                            if failcount < 3:
                                Furniture.text_daemontuer("fail")
                                failcount += 1
                            else:
                                Furniture.text_daemontuer("fail2")
                else: #Wenn die Dämontür bereits geöffnet wurde, kommt der "ich schlafe" Text (die Tür schläft nach Rätsellösung ein)
                    Furniture.text_daemontuer("sleep")
            elif u"fenster" in choice or u"rausschauen" in choice or u"herausschauen" in choice or u"aussicht" in choice or u"hinausschauen" in choice or u"rausgucken" in choice or u"herantreten" in choice:
                Furniture.text_fenster()
            elif u"bett" in choice or u"himmelsbett" in choice:
                Furniture.text_bett("look")
            elif u"schlafen" in choice or u"hinlegen" in choice or u"legen" in choice or u"hineinlegen" in choice:
                Furniture.text_bett("sleep")
                self.user.hpcur = self.user.hpmax 
            elif u"truhe" in choice or u"schatz" in choice or u"schatztruhe" in choice or u"kiste" in choice or  u"schatzkiste" in choice:
                if self.user.events["falkenplatte_gefunden"] == False:
                    Furniture.text_truhe("unfound")
                    self.user.events["falkenplatte_gefunden"] = True
                    self.user.invlist.append(u"falkenstein")
                    self.build(1)
                else:
                    Furniture.text_truhe("found")
            elif u"kleiderschrank" in choice or u"schrank" in choice or  "notiz" in choice or "anziehen" in choice:
                Furniture.text_schlafzimmer_schrank("look")
                if self.user.events["robe_und_sandalen_gefunden"] == False:
                    Furniture.text_schlafzimmer_schrank("robe_und_sandalen_finden")
                    self.user.events["robe_und_sandalen_gefunden"] = True                    
                    self.user.invlist.append(u"sandalen")
                    self.user.invlist.append(u"robe")
                    self.build(1)
                else:
                    Furniture.text_schlafzimmer_schrank("found")                    

        #Ostflügel
        elif self.room.x == 6 and self.room.y == 3 and self.user.map == 1:
            if u"fenster" in choice or u"rausschauen" in choice or u"herausschauen" in choice or u"aussicht" in choice or u"hinausschauen" in choice or u"rausgucken" in choice or u"herantreten" in choice:
                if self.user.events["morgenplatte_gefunden"] == False:
                    Furniture.text_steinfenster("first")
                    self.user.invlist.append(u"morgenstein")
                    self.user.events["morgenplatte_gefunden"] = True
                    self.build(1)
                else:
                    Furniture.text_steinfenster("else")
            #Zugang zur Küche
            elif u"küche" in choice:
                self.user.y += 1
                print(u"{x} geht nach Norden in die Küche\n".format(x=self.user.name))
                misc.countdown(0.5)
                self.build(2)
        #Küche
        elif self.room.x == 6 and self.room.y == 4 and self.user.map == 1:
            if u"anrichte" in choice or u"hummus" in choice or u"nehmen" in choice:
                if self.user.events["hummus_gefunden"] == False:
                    Furniture.text_anrichte("hummus")
                    self.user.events["hummus_gefunden"] = True
                    self.user.invlist.append(u"hummus")
                    self.build(1)
                else:
                    Furniture.text_anrichte("rezept")   
            elif u"koch" in choice:
                Furniture.text_rufen(self.user.name)
            elif u"schrank" in choice or u"öffnen" in choice:    
                if self.user.events["sesam_gefunden"] == False:
                    Furniture.text_kueche_schrank("sesam")
                    self.user.events["sesam_gefunden"] = True
                    self.user.invlist.append(u"sesam")
                    self.build(1)                 
                else:
                    Furniture.text_kueche_schrank("else")
        #Westflügel
        elif self.room.x == 5 and self.room.y == 3and self.user.map == 1:
            #Steintür
            if u"steintür" in choice or u"tür" in choice or u"norden" in choice or u"nördlich" in choice or "treppenhaus" in choice or "bibliothek" in choice:
                if self.user.events["steintuer_offen"] == False:
                    if (u"falken-morgen-stein" or u"falken-zeichen-stein") in self.user.invlist:
                        Furniture.text_steintuer("teilweise_vorhanden")                        
                    elif u"morgen-falken-zeichen-stein" not in self.user.invlist:
                        Furniture.text_steintuer("versperrt")                        
                    else:
                        try:
                            self.user.invlist.remove(u"morgen-falken-zeichen-stein")
                        except:
                            log("stein war nicht vorhanden", "error")    
                            quit()
                        Furniture.text_steintuer("komplett_vorhanden")                        
                        self.user.events["steintuer_offen"] = True 
                        misc.write(u"Du erhältst 50 Erfahrungspunkte!")
                        self.user.exp = self.user.exp + 50
                        bt.check_lvl_up(self.user)
                        self.build(1)
                else:
                    Furniture.text_steintuer("offen")
            elif u"beten" in choice:
                misc.write(u"Der König gedenkt den Göttern.")
        #Zugang zum Berater Nabu
            elif u"berater" in choice or u"süden" in choice or u"südlich" in choice or u"unten" in choice or u"hinten" in choice or u"nabu" in choice or "kammer" in choice or u"kämmerchen" in choice:
                print(u"{x} versucht, in die Kammer seines Berater Nabu zu gehen, doch die Tür ist verschlossen!\n*KLOPF KLOPF*\n".format(x=self.user.name))
                misc.countdown(1)
                if not (u"spezialhummus") in self.user.invlist:
                    misc.dialog(self.user.name, "Nabu", 0, [])
                else:
                    misc.dialog(self.user.name, "Hungriger_Nabu", 0, [])
                    self.user.invlist.remove(u"spezialhummus")
                    Furniture.text_nabu(self.user.name)
                    self.user.events["gab_hummus"] = True
                    self.user.y -= 1
                    self.build(1)
        #Nabus Kammer:
        elif self.room.x == 5 and self.room.y == 2 and self.user.map == 1:
            if "nabu" in choice or "berater" in choice:
                misc.dialog(self.user.name, "Schmatzender_Nabu", 0, [])
            elif "bett" in choice or u"schlafen" in choice or u"legen" in choice or u"hinlegen" in choice or u"hineinlegen" in choice:
                print("Iih! Niemals.")
            elif u"truhe" in choice or u"schatz" in choice or u"schatztruhe" in choice or u"kiste" in choice or u"schatzkiste" in choice:
                if self.user.events["zeichenplatte_gefunden"] == False:
                    Furniture.text_truhe_52("take")
                    self.user.invlist.append("zeichenstein")
                    self.user.events["zeichenplatte_gefunden"] = True
                    self.build(1)
                else:
                    Furniture.text_truhe_52("empty")
            elif u"tür" in choice or u"ausgangstür" in choice:
                self.user.y += 1
                print(u"\n{x} geht nach Norden durch die Tür in den Westteil des Flures.\n".format(x=self.user.name))
                misc.countdown(0.5)
                self.build(2)
        #Bibliothek mit dem erstem Schalter und Truhe_66
        elif self.room.x==6 and self.room.y==6 and self.user.map==1:
            if "schalter" in choice or "ziehen" in choice or "mechanismus" in choice or "schaltermechanismus" in choice or "unterste" in choice or u"drücken" in choice or "hebel" in choice:
                if self.user.events["schalter_66_flipped"]== False:
                    self.user.events["schalter_66_flipped"] = True
                    Furniture.text_schalter("unflipped", self.user.name)
                else: #falls der schalter schon betätigt wurde, wird der "flipped" Text aufgerufen
                    Furniture.text_schalter("flipped", self.user.name)
            elif u"truhe" in choice or u"schatz" in choice or u"schatztruhe" in choice or u"kiste" in choice or u"schatzkiste" in choice:
                if self.user.events["truhe_66_taken"]== False:
                    self.user.events["truhe_66_taken"] = True
                    Furniture.text_truhe_66("take")
                    self.user.invlist.append("rhetorikbuch")
                    self.user.rh = self.user.rh + 5 
                    bt.battle(self.user, None) #Wenn battle ohne Gegner aufgerufen wird, wird nur überprüft, ob neue Fähigkeiten erlernt wurden (siehe battle.py)
                    self.build(1)
                else: #falls die Truhe schon geleert wurde:
                    Furniture.text_truhe_66("empty")
                    self.build(1)
            elif u"bücherschrank" in choice or u"bücherschränke" in choice or u"buch" in choice or u"schrank" in choice or "lesen" in choice or u"regal" in choice or u"bücherwand" in choice or u"bücher" in choice:
                Furniture.text_buecherschrank(self.user.name)
        #Bibliothek mit zweitem Schalter:
        elif self.room.x==4 and self.room.y==6 and self.user.map==1:
            if "schalter" in choice or "ziehen" in choice or "mechanismus" in choice or "schaltermechanismus" in choice or "unterste" in choice or u"drücken" in choice or "hebel" in choice:
                if self.user.events["schalter_46_flipped"]== False:
                    self.user.events["schalter_46_flipped"] = True
                    Furniture.text_schalter("unflipped_46", self.user.name)
                else: #falls der Schalter schon betätigt wurde:
                    Furniture.text_schalter("flipped", self.user.name)
            elif u"bücherschrank" in choice or u"buch" in choice or u"schrank" in choice or "lesen" in choice or u"regal" in choice or u"bücherwand" in choice or u"bücher" in choice or u"bücherschränke" in choice:
                Furniture.text_buecherschrank(self.user.name)
        #Truhe in dem Raum x3y3 
        elif self.room.x== 3 and self.room.y==3 and self.user.map == 1:
            if u"truhe" in choice or u"schatz" in choice or u"schatztruhe" in choice or u"kiste" in choice or u"schatzkiste" in choice:
                if self.user.events["truhe_33_taken"]== False:
                    self.user.events["truhe_33_taken"] = True
                    Furniture.text_truhe_33("take")
                    self.user.invlist.append(u"kekszettel")
                    self.user.wh = self.user.wh + 5 
                    misc.write(u"Wie erfrischend! Ich fühle mich gleich sehr erhohlt!")
                    self.user.hpcur = self.user.hpmax
                    bt.battle(self.user, None) #Battle überprüft ob, nachdem Weisheit erhöht wurde, neue Skills möglich sind
                    self.build(1)
                else: #falls die Truhe schon geleert wurde:
                    Furniture.text_truhe_33("empty")
            elif u"bücherschrank" in choice or u"buch" in choice or u"schrank" in choice or "lesen" in choice or u"regal" in choice or u"bücherwand" in choice or u"bücher" in choice or u"bücherschränke" in choice:
                Furniture.text_buecherschrank(self.user.name)
        #Truhe in dem Raum x3y7
        elif self.room.x== 3 and self.room.y==7 and self.user.map == 1:
            if u"truhe" in choice or u"schatz" in choice or u"schatztruhe" in choice or u"kiste" in choice or u"schatzkiste" in choice:
                if self.user.events["truhe_37_taken"]== False:
                    self.user.events["truhe_37_taken"] = True
                    Furniture.text_truhe_37("take")
                    self.user.invlist.append(u"dissbuch")
                    self.user.sc = self.user.sc + 5 
                    bt.battle(self.user, None) #Battle überprüft ob, nachdem Schikane erhöht wurde, neue Skills möglich sind
                    self.build(1)
                else: #falls die Truhe schon geleert wurde:
                    Furniture.text_truhe_37("empty")
            elif u"bücherschrank" in choice or u"buch" in choice or u"schrank" in choice or "lesen" in choice or u"regal" in choice or u"bücherwand" in choice or u"bücher" in choice or u"bücherschränke" in choice:
                Furniture.text_buecherschrank(self.user.name)
        #die Geräusche in dem Raum x4y5
        elif self.room.x == 4 and self.room.y == 5:
            if u"geräusche" in choice or "lauschen" in choice or  u"hören" in choice or u"hinhören" in choice or "westen" in choice or "links" in choice:
                if "schalter_66_flipped"==True: 
                    Furniture.text_voices("flipped")
                else: #falls der Spieler hier hinhört und der Weg noch versperrt ist:
                    Furniture.text_voices("unflipped") #vermutlich kann das garnicht passieren, da er dann in furnitureclass anders aufgerufen (garnicht erst geladen) wird
            elif u"bücherschrank" in choice or u"buch" in choice or u"schrank" in choice or "lesen" in choice or u"regal" in choice or u"bücherwand" in choice or u"bücher" in choice or u"bücherschränke" in choice:
                Furniture.text_buecherschrank(self.user.name)
        #Der NPC der den Weg in den Süden blockiert:
        elif self.room.x == 3 and self.room.y==4 and self.user.map==1:
            if "wache" in choice or "typ" in choice or "kerl" in choice or "spreche" in choice or "ansprechen" in choice or "reden" in choice or "unterhalten" in choice or u"süden"in choice or u"südlich" in choice or "unten" in choice:
                if self.user.events["spoken_to_blockway"] == False:
                    self.user.events["spoken_to_blockway"] = True
                    misc.dialog(self.user.name, "Wache", 0, [])
                    #damit der Raum neu lädt, das man nach Süden gehen kann:
                    self.build(1)
                else: #falls schon mit ihm gesprochen wurde, soll ein anderer Text kommen
                    misc.dialog(self.user.name, "Freundliche_Wache", 0, [])
            elif u"bücherschrank" in choice or u"buch" in choice or u"schrank" in choice or "lesen" in choice or u"regal" in choice or u"bücherwand" in choice  or u"bücher" in choice or u"bücherschränke" in choice:
                Furniture.text_buecherschrank(self.user.name)
        
        #Alle Bibliotheksteile ohne spezifischen Raumaufruf sollen trotzdem den Buchtext printen lassen:
        elif u"bücherschrank" in choice or u"buch" in choice or u"schrank" in choice or "lesen" in choice or u"regal" in choice or u"bücherwand" in choice  or u"bücher" in choice or u"bücherschränke" in choice:
            Furniture.text_buecherschrank(self.user.name)
        #Das banquet auf Map 2 und die dortige Schatztruhe:
        elif self.room.x == 3 and self.room.y==7 and self.user.map==2:
            if "tisch" in choice or "bankett" in choice or "banket" in choice or"essen" in choice or "probieren" in choice or "banquet" in choice or "festessen" in choice or "essenstisch" in choice or "hinsetzen" in choice or "erholen" in choice:
                Furniture.text_bankett(self.user.name)
                self.user.hpcur = self.user.hpmax 
            #Schatztruhe:
            elif u"truhe" in choice or u"schatz" in choice or u"schatztruhe" in choice or u"kiste" in choice or  u"schatzkiste" in choice:
                if self.user.events["truhe_372_taken"] == False:
                    self.user.events["truhe_372_taken"] = True
                    self.user.invlist.append(u"siegeslachen")
                    self.user.rh = self.user.rh + 10 
                    #da die Truhen-Text Namenskonvention (Truhe+x und y Koordinaten) nicht mapspezifisch ist, und in Map 1 37 schon ein Schatz ist, 
                    #breche ich hier die Konvention und gebe einen spezifischen Namen:
                    Furniture.text_siegesl("take")
                    bt.battle(self.user, None) #Battle überprüft ob, nachdem Rhetorik erhöht wurde, neue Skills möglich sind
                    self.build(1)
                else: #falls die truhe also schon leer ist, anderer Text
                    #print("truhe 372 taken ist true inhalt weg")
                    Furniture.text_siegesl("empty")
        #Weiterhin Banqueträume (ein 3 Räume breiter Tisch)
        elif self.room.x == 4 and self.room.y==7 and self.user.map==2:
            if "tisch" in choice or "bankett" in choice or "banket" in choice or"essen" in choice or "probieren" in choice or "banquet" in choice or "festessen" in choice:
                Furniture.text_bankett(self.user.name)
                self.user.hpcur = self.user.hpmax 
        #weiterhin Banquet, hier sind Spinnweben und Fenstertext zu finden
        elif self.room.x == 5 and self.room.y==7 and self.user.map==2:
            if "tisch" in choice or "bankett" in choice or "banket" in choice or "essen" in choice or "probieren" in choice or "banquet" in choice or "festessen" in choice:
                Furniture.text_bankett(self.user.name)
                self.user.hpcur = self.user.hpmax
            elif "spinnweben" in choice:
                print(u"Iiih, Spinnweben! Besser fernhalten.")
            elif u"fenster" in choice or u"rausschauen" in choice or u"herausschauen"in choice:
                Furniture.text_wachfenster()
        #Zugang zu den Wachkammern über eine generische Nennung von 'Tür':
        elif self.room.x == 2 and self.room.y==3 and self.user.map==2:
            if u"tür" in choice or u"ausgangstür" in choice:
                self.user.x -= 1
                print(u"\n{x} geht nach Westen\n".format(x=self.user.name))
                misc.countdown(0.5)
                self.build(2)
        #Die verrückte Wache, welche vor dem spieler wegrennt
        elif self.room.x == 4 and self.room.y==6 and self.user.map==2:
            if "wache" in choice or "typ" in choice or "kerl" in choice or "spreche" in choice or "ansprechen" in choice or "reden" in choice or "unterhalten" in choice or u"süden"in choice or u"südlich" in choice or "unten" in choice:
                if self.user.events["wache_weg"] == False:
                    self.user.events["wache_weg"] = True
                    misc.dialog(self.user.name, "Verwirrte_Wache", 0, [])
                    #damit die "wache" keywords zum ansprechen verschwinden, hier noch mal build ab schritt 6, den Furnitures und NPCs
                    self.build(6)
                else: 
                    log("Wache, die garnicht mehr da ist, konnte angesprochen werden", "error")
        #Die weibliche Wachkammer in x1y3:
        elif self.room.x==1 and self.room.y == 3 and self.user.map==2:
            if "wache" in choice or "spreche" in choice or"ansprechen" in choice or "reden" in choice or "unterhalten" in choice or "dame" in choice or "frau" in choice:
                misc.dialog(self.user.name, "Weibliche_Wache", 0, [])
            elif "vase" in choice or "blume" in choice or "blumenvase" in choice or "tisch" in choice or "nachttisch" in choice:
                Furniture.text_vase()
            elif "kerze" in choice or "halter" in choice or "kerzenhalter" in choice:
                Furniture.text_kerze()
            elif u"truhe" in choice or u"schatz" in choice or u"schatztruhe" in choice or u"kiste" in choice or u"schatzkiste" in choice:
                if self.user.events["truhe_13_taken"]==False:
                    self.user.events["truhe_13_taken"] = True
                    Furniture.text_truhe_13("take")
                    self.user.invlist.append("Ass")
                    self.user.sc = self.user.sc + 10
                    bt.battle(self.user, None) #Battle überprüft ob, nachdem Schikane erhöht wurde, neue Skills möglich sind
                    self.build(1)
                else:
                    Furniture.text_truhe_13("empty")
            elif "bett" in choice or u"schlafen" in choice or u"legen" in choice or u"hinlegen" in choice or u"hineinlegen" in choice:
                print("Iih! Niemals. Nur im eigenen Himmelsbett! ")
        elif self.room.x==3 and self.room.y==3 and self.user.map==2:
            if "waffenhalter" in choice or "halter" in choice or "nehmen" in choice or "hinter" in choice or "dahinter" in choice:
                Furniture.text_waffenhalter()
        #Das Schienenrätsel, erst wenn der Schalter gedrückt wurde, verschiebt sich die Leite, also kann ein Raum weiter eine Kiste gefunden werden
        elif self.room.x==5 and self.room.y==3 and self.user.map==2:
            if "leiter" in choice or "schienen" in choice or "klettern" in choice or "hoch" in choice or "hochgehen" in choice or "schieben" in choice or "verschieben" in choice or u"drücken" in choice or "ziehen" in choice or "boden" in choice:
                if self.user.events["schalter_15_flipped"]==False:
                    Furniture.text_leiter(self.user.name, "falsch")
                else: #falls es schon geflipped wurde, sieht man hier nur noch Schienen ohne Leiter:
                    Furniture.text_leiter(self.user.name, "schienen")
            #Da die leere Leiter an die Decke zeigt, soll bei aufruf/nachgucken hier ein fehlleitender Text der Deckenbeschreibung kommen
            elif "decke" in choice or "oben" in choice or "schaue" in choice or "gucke" in choice:
                Furniture.text_decke()
        elif self.room.x==6 and self.room.y==3 and self.user.map==2:
            #die Leiter in diesem Raum, mit der man nach dem verschieben einen Schatz finden soll:
            if "leiter" in choice or "schienen" in choice or "klettern" in choice or "hoch" in choice or "hochgehen" in choice or "schieben" in choice or "verschieben" in choice or u"drücken" in choice or "ziehen" in choice or "boden" in choice:
                #hier verhält es sich genau andersherum, man sieht nur Schienen wenn der schalter noch nicht gedrückt wurde, und kann ansonsten hochklettern
                if self.user.events["schalter_15_flipped"]==False:
                    Furniture.text_leiter(self.user.name, "schienen")
                else: #falls die Leiter an der richigen Stelle ist, soll überprüft werden ob die 
                    #dort versteckte truhe schon geleert wurde oder nicht und dementsprechende Texte/Events passieren:
                    if self.user.events["truhe_63_taken"] == False:
                        Furniture.text_leiter(self.user.name, "richtig")
                        self.user.events["truhe_63_taken"] = True
                        self.user.invlist.append("Koffein")
                        self.user.wh = self.user.wh + 10 
                        bt.battle(self.user, None) #Battle überprüft ob, nachdem Weisheit erhöht wurde, neue Skills möglich sind
                        self.build(1)
                    elif self.user.events["truhe_63_taken"] == True:
                        #falls sie schon geleert wurde und man hochklettert, kommt ein anderer Text
                        Furniture.text_leiter(self.user.name, "empty")
            #Die Tür in die Kammer:
            elif u"tür" in choice or u"ausgangstür" in choice:
                self.user.x += 1
                print(u"\n{x} geht nach Osten durch die Tür in die Wachkammer\n".format(x=self.user.name))
                misc.countdown(0.5)
                self.build(2)
            #Die Decke, wie im vorherigen Raum:
            elif "decke" in choice or "oben" in choice or "schaue" in choice or "gucke" in choice:
                Furniture.text_decke()
        elif self.room.x==4 and self.room.y==4 and self.user.map==2:
            if "schild" in choice or "lesen" in choice:
                Furniture.text_schild()
        #Die Wachkammer mit Schalter und der fanatischen Wache in raum x1y5:
        elif self.room.x==1 and self.room.y==5 and self.user.map==2:
            if "bett" in choice or u"schlafen" in choice or u"legen" in choice or u"hinlegen" in choice or u"hineinlegen" in choice:
                misc.write("Iih! Niemals. Nur im eigenen Himmelsbett!")
            elif "kerze" in choice or "halter" in choice or "kerzenhalter" in choice:
                Furniture.text_kerze()
            elif "schalter" in choice or "ziehen" in choice or "mechanismus" in choice or "schaltermechanismus" in choice or u"drücken" in choice or "hebel" in choice:
                if self.user.events["schalter_15_flipped"]==False:
                    Furniture.text_schalter("unflipped", self.user.name)
                    self.user.events["schalter_15_flipped"]=True
                else: 
                    Furniture.text_schalter("flipped", self.user.name)
            elif "wache" in choice or "typ" in choice or "kerl" in choice or "spreche" in choice or "ansprechen" in choice or "reden" in choice or "unterhalten" in choice:
                misc.dialog(self.user.name, "Fanatische_Wache", 0, [])
        #Die seltsame Wache in x7y5:
        elif self.room.x==7 and self.room.y==5 and self.user.map==2:
            if "bett" in choice or u"schlafen" in choice or u"legen" in choice or u"hinlegen" in choice or u"hineinlegen" in choice:
                print("Iih! Niemals. Nur im eigenen Himmelsbett!")
            elif "wache" in choice or "typ" in choice or "kerl" in choice or "spreche" in choice or "ansprechen" in choice or "reden" in choice or "unterhalten" in choice:
                misc.dialog(self.user.name, "Seltsame_Wache", 0, [])
            elif "kerze" in choice or "halter" in choice or "kerzenhalter" in choice:
                Furniture.text_kerze()
            elif "bett" in choice or u"schlafen" in choice or u"legen" in choice or u"hinlegen" in choice or u"hineinlegen" in choice:
                misc.write("Iih! Niemals. Nur im eigenen Himmelsbett! ")
            elif u"tür" in choice or u"ausgangstür" in choice:
                self.user.x -= 1
                print(u"\n{x} geht nach Westen durch die Tür in die Wachkammer\n".format(x=self.user.name))
                misc.countdown(0.5)
                self.build(2)
        #die Barriere in x4y2
        elif self.room.x==4 and self.room.y==2 and self.user.map==2:
            if u"süden" in choice or u"südlich" in choice or  u"unten" in choice or u"hinten" in choice or "barriere" in choice or "lila" in choice or "dunkel" in choice or "gesicht" in choice or "verzerrt" in choice or "sprechen" in choice or "ansprechen" in choice or "unterhalten" in choice or "hallo" in choice:
                #der Barriere Text fragt nach den 3 Keywords und liefert einen bool ob es gelöst wurde oder nicht
                raetsel=Furniture.barriere_text()
                if raetsel==True:
                    self.user.events["barriere_down"] = True
                    #Der Raum wird mit entfernter Barriere neu geladen
                    self.build(1)
        #Die Tür, um aus Raum x3y5 einfacher rauszukommen:
        elif self.room.x==2 and self.room.y==5 and self.user.map==2:
            if u"tür" in choice or u"ausgangstür" in choice:
                    self.user.x -= 1
                    print(u"\n{x} geht nach Westen durch die Tür in die Wachkammer\n".format(x=self.user.name))
                    misc.countdown(0.5)
                    self.build(2)
        elif self.room.x==6 and self.room.y==5 and self.user.map==2:
            if u"tür" in choice or u"ausgangstür" in choice:
                    self.user.x += 1
                    print(u"\n{x} geht nach Osten durch die Tür in die Wachkammer\n".format(x=self.user.name))
                    misc.countdown(0.5)
                    self.build(2)
            elif "spinnweben" in choice:
                misc.write(u"Iiih, Spinnweben! Besser fernhalten.")
        #Spinnweben unabhängig von Räumen (ähnlihc wie die Bücher in Map 1)
        elif "spinnweben" in choice:
            misc.write(u"Iiih, Spinnweben! Besser fernhalten.")
        else:
            log("Da kam was in handle_furn_input an, was nich abgearbeitet wird " + str(choice), "error")
            
    def handle_input_if_in_bed(self, choice):
        #Wird aufgerufen, wenn man noch im Bett liegt (also ganz zu Beginn des Spiels)
        #hier soll nur aufstehen, umsehen, und nach Hilfe rufen möglich sein
        if "aufstehen" in choice or "heraus" in choice or "raus" in choice or ("stehe" in choice and "auf" in choice):
            print(u"Zeit aufzustehen und der Sache auf den Grund zu gehen!\n")
            self.user.events["ist_aufgestanden"] = True
            self.build(0)
        elif u"schlafen" in choice or u"weiterschlafen" in choice:
            print("...")
            misc.countdown(2)
            self.build(6)
        elif u"licht" in choice:
            print(u"Durch meine geschlossenen Augenlider spüre ich das Sonnenlicht durch das Fenster auf meine Haut strahlen - elektrisches Licht ist eine Sache aus Zukunftsgeschichten.")
        elif u"diener" in choice or "klingeln" in choice or "rufen" in choice or "wache" in choice or "bardin" in choice or "untergebene" in choice or u"brülle" in choice or "frage" in choice:
            #In dem Fall soll ein Text aufkommen ,der nach Dienern ruft
            Furniture.text_rufen(self.user.name)
        elif u"truhe" in choice or u"schatz" in choice or u"schatztruhe" in choice or u"kiste" in choice or  u"schatzkiste" in choice:
                if self.user.events["falkenplatte_gefunden"] == False:
                    Furniture.text_truhe("unfound")
                    self.user.events["falkenplatte_gefunden"] = True
                    self.user.invlist.append(u"falkenstein")
                    self.build(1)
                else:
                    Furniture.text_truhe("found")
        else:
            log(u"Some choice while still in bed has been matched.", "error")