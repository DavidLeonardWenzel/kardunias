#!/usr/bin/env python
# -*- coding: utf-8 -*-
import source.misc as misc
#Importiert misc, damit die write Funktion mit jedem String aufgerufen wird, welcher ein "Enter" verlangt, um dem Spieler zu verdeutlichen, das noch kein Input angenommen wird

def give_help(events):
    #Diese Texte überprüfen anhand der user events, was als nächstes zu lesen ist bzw. schon erreicht wurde
    #Je nach dem gibt es einen hoffentlich helfenden Text, was als nächstes zu tun ist:
    if events["ist_aufgestanden"] == False:
        print(u"Das Bett ist sehr gemütlich, aber bevor ich irgendetwas machen kann, sollte ich 'aufstehen'!")
    elif events["daemontuer_offen"] == False:
        print(u"Am besten gehe ich mal zur 'Tür' und SCHREIE SIE AN INDEM ICH ALLES GROß SCHREIBE!")
    elif events['falkenplatte_gefunden'] == False and events['morgenplatte_gefunden']== False and events['zeichenplatte_gefunden'] == False:
        print(u"Warum lässt mich das Gefühl nicht los, dass ich mich mal in meinem Schlafsaal 'umschauen' sollte, um sicherzugehen, dass dort noch meine Schatztruhe ist?")
    elif events['falkenplatte_gefunden'] == False and events['morgenplatte_gefunden']== True and events['zeichenplatte_gefunden'] == True:
        print(u"Warum lässt mich das Gefühl nicht los, dass ich mich mal in meinem Schlafsaal 'umschauen' sollte, um sicherzugehen, dass dort noch meine Schatztruhe ist?")
    elif events['falkenplatte_gefunden'] == True and events['morgenplatte_gefunden']== False and events['zeichenplatte_gefunden'] == False:
        print(u"Huch! War da etwa in meinem Ostflügel etwas an meinem Fenster locker?! Das 'Fenster' sollte ich mal unter die Lupe nehmen.")
    elif events['falkenplatte_gefunden'] == True and events['morgenplatte_gefunden']== True and events['zeichenplatte_gefunden'] == False and events["gab_hummus"] == False:
        print(u"Ich sollte wirklich mit Nabu, meinem Berater, reden. Er sagt, er ist hungrig und will Hummus nach Omas Art? Aber dafür muss ich den 'Sesam' und 'Hummus' aus der Küche 'kombinieren'.")
    elif events['falkenplatte_gefunden'] == False and events['morgenplatte_gefunden']== False and events['zeichenplatte_gefunden'] == True:
        print(u"Warum lässt mich das Gefühl nicht los, dass ich mich mal in meinem Schlafsaal 'umschauen' sollte, um sicherzugehen, dass dort noch meine Schatztruhe ist?")
    elif events['falkenplatte_gefunden'] == True and events['morgenplatte_gefunden']== False and events['zeichenplatte_gefunden'] == True:
        print(u"Huch! War da etwa in meinem Ostflügel etwas an meinem Fenster locker?! Das 'Fenster' sollte ich mal unter die Lupe nehmen.")
    elif events["steintuer_offen"] == False:
        print(u"Mich lässt die Schatztruhe in Nabus Zimmer einfach nicht los. Diese Steinplatten überall muss man doch nach und nach 'kombinieren' können, um durch diese steinerne Tür im Westflügel zu kommen.")
    elif events["schalter_66_flipped"] == False:
        print(u"Ich glaube in der Bibliothek muss irgendwo ein Mechanismus sein mit dem ich weiter westlich in die Bibliothek vordringen kann.")
    elif events["nabu_vertrieben"]==False:
        print(u"Irgendwo in dieser Bibliothek muss jemand wissen, was mit meinem Königreich passiert ist. Ich weiß es einfach. Ich werde nicht aufhören dort alles abzulaufen bis ich weiß was hier vor sich geht.")
    elif events["barriere_down"]==False:
        print(u"Die Wachen wissen bestimmt, wie man durch die Barriere im Süden der Wachkammer kommt. Sie alle versuchen mir etwas Bestimmtes zu sagen... Oder hab ich gerade etwa eine göttliche Eingebung?")
        insta = misc.yes_no(u"Weiß ich etwa, wie man diese Barriere durchbricht? (Ja oder Nein?)")
        if insta == True:
            print(u"Natürlich! Ich spüre es! Die Wachen versuchen, mir etwas zu sagen... \nDie Dame sagt 'ALLE'. Der Bierliebhaber sagt 'LIEBEN'. Und der Fanatiker sagt 'MARDUK'.\nZusammengesetzt ist das Passwort für die Barriere also:\n")
            misc.countdown(2)
            print("'ALLE LIEBEN MARDUK'")
    else:
        print(u"Da weiß sogar der Entwickler nicht mehr weiter. Da hilft nur noch Glück.")