#!/usr/bin/env python
# -*- coding: utf-8 -*-

from source.dev_tools import log

import time
import random

import source.type_parser as parse
import xml.etree.ElementTree as ET
import re


def countdown(count, rise=False, fastrise=False):
    #Diese Funktion wird mit einem Integer, der normalerweise Sekunden darstellt, aufgerufen. 
    #So viele Sekunden "verzögert" sich etwas oder man fügt rise hinzu, sodass sie leere Strings printet
    #das schafft den effekt, dass etwas aufsteigt
    #mit fastrise soll man etwas schnell nach oben scrollen lassen können, dann ist die Zahl nur 1/10 Sekunden
    if rise:
       while count>=0:
         count-=1
         time.sleep(1)
         print("")
    elif fastrise: 
       while count>=0:
         count-=1
         time.sleep(0.1)
         print("\n")
    else: 
       while count>=0:
         count-=1
         time.sleep(1)

def yes_no(ja_nein_frage):
    #ich brauche immer wieder ja/nein Abfragen für verschiedene Aspekte, deshalb hier eine Funktion, die man mit einer Frage (bzw. einem String) aufrufen kann
    #Sie returned immer einen bool, bei positiver Antwort True, sonst False
    print(ja_nein_frage)
    user_said=input()
    possible = ["ja","yes","jup","yarp","yeah","joar","jaja","ok", "okay", "nope","narp","nei","ney","ne","nein", "noe"]
    antworten= parse.check_input(user_said,possible)
    #check_input ist eine Funktion, die den input und eine mitgelieferte Zielliste, welche alle legalen Ziele darstellt, die der Input sein darf
    positive=possible[:9]
    negative=possible[9:]
    antwortstatus="unerkannt"
    #Wenn es eine erkannte Antwort gab:
    if len(antworten)>0:
       for antwort in antworten:
            if antwortstatus=="unerkannt":
            #solange die Antwort nicht gefunden wurde, gehe ins nächste Listenelement, sonst nicht (es könnte jau ja yes sein)
            #die Kontrollstruktur sollte überflüssig sein
                if antwort in negative:
                    antwortstatus="erkannt"
                    return False
                elif antwort in positive:
                    antwortstatus=="erkannt"
                    return True
                else:
                    #die else sollte eigentlich nie passieren, dann wurde vergessen einer möglichen Antwort eine Aufgabe zuzuweisen
                    log(u"Antwort war nicht leer, also in der Zielliste, aber nicht in einer erwarteten Liste, nämlich:", "error")
                    log("... " + str(antwort), "error")
    else:
       #falls nichts gefunden wurde, hat der Spieler etwas unerwartetes eingegeben, die Frage soll wiederholt werden
       print(u"\nEntschuldigung - versuch bitte, es anders auszudrücken\n")
       return yes_no(ja_nein_frage)

def skip():
    #soll einfach den Bildschirm 'resetten', indem 15 newlines gleichzeitig geprinted werden. Wird für "Animations" Effekte in Kämpfen verwendet
    print("\n"*15)

def write(text):
    #Modifizierte print funktion: Jeder Text der geprintet werden soll, soll danach \n und Enter verlangen, bevor weiter gespielt wird 
    print(text)
    print("(ENTER um fortzufahren)")
    input()
    #x=input() #code(sys.stdin.encoding or locale.getpreferredencoding(True))
    #print("Got: \n",x)
def dialog(username, npc_name, step, choices):
    #Diese Funktion wird mit dem Namen des gewünschten NPCs aufgerufen, welche dann aus der NPC XML die Dialogstrings zieht
    #step wird benötigt, damit die Dialog Funktion an gewünschter Stelle neu aufgerufen werden kann, wenn z.B. eine ungültige Eingabe kam
    #darüber hinaus zählt step die "Tiefe" des Dialogbaums
    #die Liste choices enthält alle gewählten Optionen, sodass der "Gesprächspfad" bei erneutem aufrufen wieder entlang gegangen werden kann
    tree = ET.parse('data/npc.xml')
    root = tree.getroot()
    #auf erster Ebene wird zunächst das relevante Kind (den zugehörigen NPC) gefunden
    for npc in root:
        if npc_name == npc.tag:
            if step==0:
                #Hier wird der String des NPC (Der Text des ersten Kindes) gefunden und angezeigt 
                print(npc_name.replace("_", " ")+":\t"+npc[0].text)
                num=1
                #Die Dialogmöglichkeiten sind alle Kinder dieser Antwort
                for dialog_option in npc[0]:
                    print("\t"+"["+str(num)+"]"+" "+dialog_option.text)
                    num+=1
                #Als nächstes kann der Spieler eine entsprechende Zahl angeben, was mit einem regEx geprüft wird, der nur einstellige Zahlen erlaubt
                #außerdem wird überprüft, ob die Zahl nicht höher als die Möglichkeiten war
                user_said=input()
                print("")
                #die Auswahl muss in einen int wert umgewandelt werden können, weshalb spaces gänzlich entfernt werden
                user_said=user_said.replace(" ", "")
                if re.match(r"(?<!\S)\d(?!\S)", user_said) and int(user_said)!=0 and int(user_said)<= num-1:
                #der Dialogpfad wird in "choices" gespeichert, es stellt in einer Liste dar, welche Optionen der Nutzer traf
                    choices.append(int(user_said))
                    return dialog(username, npc_name, 1, choices)
                else:
                    print(u"Bitte eine der Zahlen angeben\n")
                    return dialog(username, npc_name, step, choices)
            #Diese Steps funktionieren fast wie die obigen, gehen aber für alles einen Schritt tiefer in der XML
            if step==1: 
                print(username+":\t"+npc[0][choices[0]-1].text)
                print(npc_name.replace("_", " ")+":\t"+npc[0][choices[0]-1][0].text)
                num=1
                if npc[0][choices[0]-1][0].findall("dialogue_choice") != []:
                #dieser Abgleich sorgt dafür, das wir nur weitere Dialogmöglichkeiten suchen, wenn es weitere in der XML gibt
                    for dialog_option in npc[0][choices[0]-1][0]:
                        print("\t"+"["+str(num)+"]"+": "+dialog_option.text)
                        num+=1
                    user_said=input()
                    print("")
                    user_said=user_said.replace(" ", "")
                    if re.match("(?<!\S)\d(?!\S)", user_said) and int(user_said)!=0 and int(user_said)<= num-1:
                        choices.append(int(user_said))
                        return dialog(username, npc_name, 2, choices)
                    else:
                        print(u"Bitte eine der Zahlen angeben")
                        return dialog(username, npc_name, step, choices)
            #alle übrigen steps wiederholen sich, gehen nur jeweils eine Ebene tiefer mithilfe der "choices" Liste
            elif step==2:
                print(username+":\t"+npc[0][choices[0]-1][0][choices[1]-1].text)
                print(npc_name.replace("_", " ")+":\t"+npc[0][choices[0]-1][0][choices[1]-1][0].text)
                if npc[0][choices[0]-1][0][choices[1]-1][0].findall("dialogue_choice") != []:
                    num=1
                    for dialog_option in npc[0][choices[0]-1][0][choices[1]-1][0]:
                        print("\t"+"["+str(num)+"]"+": "+dialog_option.text)
                        num+=1
                    user_said=input()
                    #print("\n")
                    user_said=user_said.replace(" ", "")
                    if re.match("(?<!\S)\d(?!\S)", user_said) and int(user_said)!=0 and int(user_said)<= num-1:
                        choices.append(int(user_said))
                        return dialog(username, npc_name, 3, choices)
                    else:
                        print(u"Bitte eine der Zahlen angeben")
                        return dialog(username, npc_name, step, choices)
            elif step==3:
                print(username+":\t"+npc[0][choices[0]-1][0][choices[1]-1][0][choices[2]-1].text)
                print(npc_name.replace("_", " ")+":\t"+npc[0][choices[0]-1][0][choices[1]-1][0][choices[2]-1][0].text)
                for child in npc[0][choices[0]-1][0][choices[1]-1][0][choices[2]-1][0]:
                    print(child.text)
                
                if npc[0][choices[0]-1][0][choices[1]-1][0][choices[2]-1][0].findall("dialogue_choice") != []:    
                    num=1
                    for dialog_option in npc[0][choices[0]-1][0][choices[1]-1][0][choices[2]-1][0]:
                        print("\t"+"["+str(num)+"]"+": "+dialog_option.text)
                        num+=1
                    user_said=input()
                    print("")
                    user_said=user_said.replace(" ", "")
                    if re.match("(?<!\S)\d(?!\S)", user_said) and int(user_said)!=0 and int(user_said)<= num-1:
                        choices.append(int(user_said))
                        return dialog(username, npc_name, 4, choices)
                    else:
                        print(u"Bitte eine der Zahlen angeben")
                        return dialog(username, npc_name, step, choices)
            elif step==4:
                #print("David:\t"+npc[0][choices[0]-1][0][choices[1]-1][0][choices[2]-1].text)
                print(npc_name.replace("_", " ")+":\t"+npc[0][choices[0]-1][0][choices[1]-1][0][choices[2]-1][0][choices[3]-1][0].text)
                if npc[0][choices[0]-1][0][choices[1]-1][0][choices[2]-1][0][choices[3]-1][0].findall("dialogue_choice") != []:    
                    num=1
                    for dialog_option in npc[0][choices[0]-1][0][choices[2]-1][0][choices[3]-1][0]:
                        print("\t"+"["+str(num)+"]"+": "+dialog_option.text)
                        num+=1
                    user_said=input()
                    print("")
                    user_said=user_said.replace(" ", "")
                    if re.match("(?<!\S)\d(?!\S)", user_said) and int(user_said)!=0 and int(user_said)<= num-1:
                        choices.append(int(user_said))
                        return dialog(username, npc_name, 5, choices)
                    else:
                        print(u"Bitte eine der Zahlen angeben")
                        return dialog(username, npc_name, step, choices)
            else:
                return 0
