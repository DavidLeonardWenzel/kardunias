#!/usr/bin/env python
# -*- coding: utf-8 -*-


from source.dev_tools import log
import random
import codecs
import pandas as pd
from source.classes.mobclass import Mob
import source.misc as misc
import re
#global erreichbares dictionary, welche für die Schikane benötigt wird:
beleidigungs_dict = {
    1:"Luftikus",
    2:"Primat",
    3:"Pilzbefallener",    
    4:"Brandopfer",          
    5:"Rosinenhirn",          
    6:"Intelligenzallergiker",
    7:"Furzknoten",          
    8:"Reeperbahneinheimischer",
    9:"Lackaffe",          
    10:"Seifenoperzuschauer" , 
    11:"Assembly-Fanatiker",  
    12:"RegExParser",          
    13:"Prequel-Trilogie-Fan", 
    14:"Pixelfehler",         
    15:"Fluffelball",         
    16:"Drecksschleuder" ,     
    17:"Milbentransportmittel",
    18:"Kuscheltier",
    19:"Nichtsnutz" , 
    20:"Bazillenknoten",
    21:"Dreckeimerersatz",
    22:"Student",
    23:"Hanswurst",
    24:"Wollsau",
    25:"Waschlappen",
    26:"Snob",
    27:"Billigteil",
    28:"Flohmarkterwerb",
    29:"Stumpfhirn", 
    30:"Stachelbiest"}
#Generelle Kampf Funktion, wird von der Engine mit dem Playerobjekt und zufälligem Gegner aufgerufen
#Sie gibt immer ein Player Objekt zurück, welches die neuen Statuswerte nach dem Kampf und ggf. nach einem 
#Levelaufstieg hat, sodass der Spieler andere HP oder EXP nach dem Kampf hat
def battle(player, mob):
    if mob == None: #Wenn die Funktion None als Monster übergibt bekommt, soll kein Kampf, sondern ein
                    #Skill Lvl Up passieren (passiert wenn in Truhen Schätze, die stats erhöhen, gefunden werden)
        check_skill_up(player)
    #1. Speichern originale Werte der stat, da diese sich im Kampf temporär verändern können:  
    else: 
        original_rh = player.rh
        original_sc = player.sc
        original_wh = player.wh

    #2. Gegner Objekt laden (siehe classes/mobclass.py)
        mob = mob.lower() #falls es ausversehen groß geschrieben übergeben wurde
        mob_obj = load_mob(mob) #ruft die loadmob Funktion auf, welche ein Gegner Objekt für den Kampf erstellt

    #3.In einer großen Schleife bis entweder die Leben des Gegners oder des Spielers HP 0 sind, finder der Kampf statt:
        mob_wkcur = mob_obj.wk #Zu beginn ist die "current" HP die maximale
        while mob_wkcur > 0 and player.stat != "dead":
        #3.1. HUD malen, skip lässt den Bildschirm nach oben springen (viele \n), render_hud stellt Werte und Ascii Art in kleinen Boxen dar
            misc.skip()
            render_hud(player, mob_obj, mob_wkcur, 0)
            misc.write("{x} ist dran!".format(x=mob_obj.name[:1].upper()+mob_obj.name[1:]))#Anzeigen, wer dran ist mit Großbuchstaben am Anfang
        #3.2.Gegner Runde: Der angriff wird mit randint zufällig aus der Liste der möglichen angriffe ausgewählt
            mobresult = attack(mob_obj)
            #mobresult returned immer einen eine Liste mit Schadenswert, Spielerstatuswert (z.B. Gift) und eigenen Status (z.B. Angriff aufladen!)
            dmg, statuschange, specialchange = mobresult[0], mobresult[1], mobresult[2] #Diese werden hier einzeln "entpackt"
        #3.3 Schaden am Spieler berechnen 
            player.hpcur = player.hpcur - dmg #wenn der DMG Wert des Monsters negativ ist, heilt sich der Spieler
            #3.3.1 Nebenbei HUD aktualisieren, um aktuelle Werte anzuzeigen
            misc.skip()
            render_hud(player, mob_obj, mob_wkcur, 0)
            #3.3.2 Statusänderungen abhandeln
            if statuschange != "":
                player.stat = statuschange
            if specialchange != "":
                mob_obj.special= specialchange
            #Wenn der Spieler eingeschläfert wurde, soll die Chance, dass er aufsteht etwa 60 % sein
            if player.stat == "schlafend":
                aufstehchance = random.randint(1,10)
                if aufstehchance >= 7:
                    misc.write(u"Jetzt könntest du etwas machen! . . . Aber du schläfst.")
                else:
                    misc.write(u"Du bist aufgewacht!")
                    player.stat = "Gesund"
            elif player.stat =="vergiftet":
                misc.write("Das Gift strömt durch deinen Körper! - 1 Leben")
                player.hpcur=player.hpcur-1
            elif player.stat=="intuition":
                player.rh = int(player.rh*1.2) #20%er Anstieg der Rheotorik, wird durch "int" auf den nächstniedrigen int gerundet
                player.wh = int(player.wh*1.3) #30%er Anstieg der Weisheit, "" ""
                player.sc = int(player.sc*1.3) #30%er Anstieg der Schikane, "" ""
                player.stat ="Gesund"
            elif player.stat=="nachschlagen":
                player.rh = int(player.rh*1.5) #50%
                player.wh = int(player.wh*1.7) #70%
                player.sc = int(player.sc*1.7) #70%
                player.stat ="Gesund"
            elif player.stat=="wiki":
                player.rh = int(player.rh*1.7) #70%
                player.wh = int(player.wh*2)   #100%
                player.sc = int(player.sc*2)   #100%
                player.stat ="Gesund"
                #In jedem Fall heilt es Gift, in dem es den Status auf gesund setzt

            #3.3.3 danach überprüfen,  ob der Spieler kein Leben mehr hat:
            if player.stat== "schlafend" and player.hpcur >=0:
                misc.write("ZzZ")
                dmg, heal, statuschange = [0,0,""]
                #Statt der eigenen Spielrunde sind die Schadens und Statuswerte leer, wenn man schläft
            elif player.hpcur >=1: #wenn man nicht schläft und mehr als 0 HP hat, ist man dran, und sucht sich in choice einen Skill aus
                choice = playerround(player)
                #nachdem ein Skill übergeben wurde, kann das dazugehörige Minispiel mit dieser Wahl gestartet werden:
                playerresult = minigames(player, mob_obj, choice) #Die Funktion benötigt das Playerobjekt, Monsterobjekt, und die Auswahl des Spielers
                #Sie returned einen Schadenswert, Heilwert und mögliche Statusveränderungen
                dmg, heal, statuschange = playerresult[0], playerresult[1], playerresult[2] 
            else: #Wenn man keine HP mehr hat, kommt ein kleiner Text und man startet wieder am Anfang der jeweiligen Map:
                misc.write("Dir wird schwarz vor Augen, und du fällst vornüber auf den Boden...")
                #ich printe einige "..."" und lasses den Spieler 3x enter drücken, um die niederlage deutlich zu machen
                print("...")
                misc.write("")
                print("...")
                misc.write("")
                print("...")
                misc.write("")
                misc.countdown(1)
                #die stats resetten:
                player.hpcur = player.hpmax
                player.rh = original_rh
                player.sc = original_sc
                player.wh = original_wh
                if player.map == 1:
                    #an diesen Startpunkten pro Karte soll der Spieler respawnen:
                    player.x = 6
                    player.y = 2
                elif player.map == 2:
                    player.x = 4
                    player.y = 7
                elif player.map == 3:
                    player.x = 5
                    player.y = 1
                player.stat = "dead" #mit dem stat "dead wird in der Engine überprüft, ob "build" die neuen Koordinaten laden soll 
                return player
            #3.4 Schaden am Gegner und Heilungen berechnen
            mob_wkcur = mob_wkcur - dmg
            if mob_wkcur <= 0:
                mob_wkcur=0
            player.hpcur = player.hpcur + heal
            #Überprüfen, ob Max HP erreicht wurde:
            if player.hpcur >= player.hpmax:
                player.hpcur = player.hpmax
            if statuschange!= "":
                player.stat=statuschange
            #3.5. Kampfanimation - HUD malen mit verändertem Gegnerbild
            if dmg != 0: #nur wenn tatsächlich schaden gemacht wurde ,soll die 'animation' erscheinen
                render_hud(player, mob_obj, mob_wkcur, 1)
                misc.countdown(0.5)
     
        #4.1 status effekte und buffs sollen am ende des Kampfes resettet werden
        player.stat="Gesund"
        player.rh = original_rh
        player.sc = original_sc
        player.wh = original_wh
        #Wenn der Spieler geringe Leben hat, soll ein Warnhinweis erscheinen:
        if player.hpcur <=9:
            misc.write(u"Ich fühle mich sehr schwach... vielleicht sollte ich mich in meinem Bett oder einem Stuhl ausruhen...")
        #4.2 Berechnen ob Stufenanstieg möglich ist. Falls ja Stufenanstiegsfunktion aufrufen
        player.exp = player.exp + mob_obj.exp
        misc.write("Geschafft! {x} Erfahrung erhalten!".format(x=mob_obj.exp))
        check_lvl_up(player)
        #5.Spieler Objekt zurückgeben
    return player

#Funktion, welche das MobObjekt mithilfe des Pandas Moduls lädt:
def load_mob(name):
    mob_data = pd.read_csv('data/mob_stats.csv', index_col=0, sep='\t')
    #Ein tryexcept Block versucht, die Zeile mit dem ausgewählten Monster zu finden:
    try:
        relevant_row = mob_data[mob_data["name"] == name.lower()]
    except KeyError: #Fehlermeldung, wenn das nicht klappt:
        log("No mob data found for given name.", "error")
        log("... given name was: {x}".format(x=name), "error")
        log("... all possible names in data sheet: {x}".format(x=mob_data["name"]), "error")
    #Ein weiterer tryexcept block lädt dann die einzelnen Werte aus der Zeile, welche für das Objekt benötigt werden
    try:
        myatk = []
        for elem in relevant_row["attacken"]:
            myatk.append(elem)
        #die attacken sind derzeit in folgender Form: ["hier stehen alle attacken"], wird deshalb gesplittet
        myatk=myatk[0].split()
        myrh_def = int(relevant_row["rh_def"])
        mysc_def = int(relevant_row["sc_def"])
        mywh_def = int(relevant_row["wh_def"])
        myexp = int(relevant_row["exp"])
        mydesc = u""
        for elem in relevant_row["desc"]:
            mydesc = elem
        mywk = int(relevant_row["wk"])
        mynoneffective = []
        for elem in relevant_row["abprall_sc"]:
            numbers=elem.split()
            for number in numbers:
                mynoneffective.append(beleidigungs_dict[int(number)])
        myokeffective = []
        for elem in relevant_row["mittel_sc"]:
            numbers=elem.split()
            for number in numbers:
                myokeffective.append(beleidigungs_dict[int(number)])
        myeffective = []
        for elem in relevant_row["effective_sc"]:
            numbers=elem.split()
            for number in numbers:
                myeffective.append(beleidigungs_dict[int(number)])
        myart = ""
        for elem in relevant_row["art"]:
            myart = str(elem)
        #mynoneffective, myokeffective, myeffective sind Zahlen, welche mithilfe des obigen dictionairys zu tatsächlichen Beleidigungen gemapped werden können, 
        #welche verschieden Effektiv für das jeweilige Monster sind
        myspecial="" #Special wird nur im Kampf gebraucht und ist zu Beginn immer leerer String
        mob = Mob(name.lower(), myatk, myrh_def, mysc_def, mywh_def, myexp, mydesc, mywk, mynoneffective, myokeffective, myeffective, myart, myspecial)
        return mob
    except KeyError: #die Fehlermeldung, falls das nicht klappt:
        log("something went wrong while loading Mob.", "error")
        quit()

#Funktoin, welche eine Art HUD malen soll. Sie benötigt Spielerobjekt und 3 Attribute des Monsters, seinen Namne, HP und ob er gerade Schaden genommen hat
def render_hud(player,mob,mob_wkcur,pic_index):
    #0.Bildschirm resetten
    misc.skip()#printet (20*"\n")
    #1.Baue box mit Gegner Infos
    hud_mob = u"""
     _________________________________________________________________________
    |
    |\t{w}
    |\tWillenskraft: {x}\{y}
    |
    |\t{z}
    |_________________________________________________________________________
    """.format(w=mob.name.title(), x=mob_wkcur, y=mob.wk, z=mob.desc)
    print(hud_mob)

    #2.Printe Gegner Bild abhängig von pic_index (ob er getroffen wurde oder nicht) 
    #Zunächst wird die .txt Datei geöffnet, die einzelnen Bilder wurden immer am % Zeichen gesplittet, welches nie zum malen benutzt wurde
    data = u""
    try:
        f = codecs.open(mob.art, encoding="utf-8")
        data = f.read()
        f.close()
    except IOError:
        log("could not load mob art", "error")
        quit()
    splitted = data.split('%')
    print(splitted[pic_index])

    #3.Baue Box mit Player Infos und möglichen Skills
    hud_player = u"""
    _________________________________________________________________________
    |
    |\t{w}
    |\tLeben: {x}\{y}
    |
    |\tMögliche Fähigkeiten:
    |\t{z} 
    |_________________________________________________________________________
    """.format(w=player.name.title(), x=player.hpcur, y=player.hpmax, z=player.possible_skills()[0])
    print(hud_player)

def attack(mob_obj):
    zufallszahl=random.randint(0,1) #alle gegner haben 2 attacken in einer Liste, hier wird diese zufällig ausgewählt
    attackname=mob_obj.atk[zufallszahl]
    dmg, statuschange, specialchange = 0, "", "" #Unabhängig ob diese Werte durch die Attacke verändert werden, werden sie returned, daher erstelle ich sie zu Beginn
    #Scoops Angriffe:
    if attackname=="kulleraugen":
        print("Scoop setzt Kulleraugen ein!")
        misc.write("Der Scoop schaut dich mit riesigen Kulleraugen an...\nBricht Herzen, aber kein Leben!")
    elif attackname=="rumkugeln":
        print("Scoop setzt Rumkugeln ein!")
        misc.write(u"Der Scoop rollt sich ein und kullert über den Boden. Huch! Jetzt ist er gegen dich gestoßen.")
        misc.write("- 1 Leben")
        dmg = 1 
    #Books Angriffe:
    elif attackname=="novellenzauber":
        print("Book setzt Novellenzauber ein")
        misc.write("Es macht dich ganz betroffen.")
        misc.write("-2 Leben")
        dmg = 2
    elif attackname=="lesung":
        print("Book setzt Lesung ein!")
        misc.write("Da schlafen selbst die interessiertesten Literaturliebhaber ein.")
        einschlafchance = random.randint(1,10)
        if einschlafchance >= 5:
            misc.write(u"Du bist eingeschlafen...")
            statuschange="schlafend"
        else:
            misc.write(u"Du konntest deine Augen gerade noch so offenhalten.")
    #Wines angriffe
    elif attackname=="auslaufen":
        print("Wine setzt Auslaufen ein")
        misc.write("Der Wine läuft über den guten Teppich aus! Macht ganz betroffen!")
        misc.write("-2 Leben")
        dmg = 2
    elif attackname=="gratisprobe":
        print("Wine setzt Gratisprobe ein")
        misc.write("Der Wine lässt etwas von sich kosten und lässt dich das kosten - Du wurdest vergiftet!")
        statuschange="vergiftet"
    #nabus angriffe
    elif attackname=="verdammniszauber":
        print(u"Nabu setzt Verdammniszauber ein")
        misc.write(u"Der Verdamnisszauber zehrt an deinem Leben.")
        misc.write("- 4 Leben")
        dmg = 4
    elif attackname=="nachrichten":
        misc.write(u"Nabu setzt Nachrichten ein")
        nachrichtenchance = random.randint(1,10)
        if nachrichtenchance >=4:
            misc.write(u"...es sind keine Guten!")
            misc.write(u" - 6 Leben")
            dmg = 6
        elif nachrichtenchance <=4:
            misc.write(u"...es sind erfreuliche Nachrichten!")
            misc.write(u" + 4 Leben")
            dmg=-4
    elif attackname=="wumme":
        misc.write(u"Macey setzt Wumme ein!")
        if mob_obj.special == "":
            misc.write(u"Der Morgenstern haut zu!")
            misc.write(u"- 5 Leben")
            dmg = 5
        elif mob_obj.special=="ausgeholt":
            misc.write(u"Der Morgenstern haut kräftig zu!")
            misc.write(u"- 10 Leben!")
            dmg = 10
            specialchange="normal"
    elif attackname=="ausholen":
        misc.write(u"Macey setzt Ausholen ein!")
        misc.write(u"Gleich wird's richtig schmerzhaft...")
        specialchange="ausgeholt"
    elif attackname=="giftgas":
        misc.write("Clouds setzt Giftgas sein!")
        misc.write(u"Du wurdest vergiftet!")
        statuschange="vergiftet"
    elif attackname=="niederschlag":
        misc.write(u"Clouds setzt Niederschlag ein!")
        misc.write(u"Du bist gänzlich durchnässt!")
        misc.write(u"...")
        rainchance = random.randint(1,10)
        if rainchance <=3:
            misc.write(u"Das bisschen Regen hat noch keinem geschadet.")
        else:
            misc.write(u"Dich packt das Fieber! - 6 Leben")
            dmg = 6
    elif attackname=="sturzflug":
        misc.write(u"Winged Scoop setzt Sturzflug ein!")
        misc.write(u"Es stürzt auf dich!")
        misc.write(u"- 6 Leben!")
        dmg = 6
    elif attackname=="gegenwind":
        misc.write(u"Winged Scoop setzt Gegenwind ein!")
        misc.write(u"Du genießt die Brise...")
    elif attackname=="inferno":
        misc.write("Marduk setzt Inferno ein!")
        misc.write(u"Dir wird bedeutend heißer als angenehm wäre!")
        misc.write(u"- 10 Leben!")
        dmg = 10
    elif attackname=="gottfunken":
        misc.write(u"Marduk setzt Götterfunken ein!")
        misc.write(u"Die Melodie ist wunderschön...")
        misc.write(u"Plötzlich durchfährt dich ein großer Schmerz!")
        rand_dmg=random.randint(8,15)
        misc.write(u"- {x} Leben!".format(x=rand_dmg))
        dmg = rand_dmg
    else:
        log("Tried to execute attack {x}, which is not in the attack func".format(x=attackname), "error")
    return [dmg, statuschange, specialchange] #sodass schaden, status und special wieder hochgereicht wird

#Skill auswählen
def playerround(player):
    #diese Funktion nimmt eine Eingabe entgegen, welche einem Skill entspricht
    print("Was möchtest du einsetzen? (Bitte eine der Zahlen angeben)")
    user_said=input()
    user_said=user_said.replace(" ", "")
    #die zahl soll kleiner oder gleich der Möglichkeiten sein (z.B. 2 wenn man 2 Skills besitzt, und eine einstellige Zahl und nicht 0 sein
    #das wird mit diesem RegEx und den 2 Bedingungen sichergestellt:
    if re.match("(?<!\S)\d(?!\S)", user_said) and int(user_said)!=0 and int(user_said)<=len(player.possible_skills()[1]):
        user_said=int(user_said)#der Wert wird immer in einen int umgewandelt, da er als string ankommt
        misc.write(u"{x} ausgewählt!".format(x=player.possible_skills()[1][user_said-1])) #Die Auswahl wird dem Spieler angezeigt
        return player.possible_skills()[1][user_said-1]
        #da der attack_list Index bei 0 beginnt, wird 1 von der eingetippten Zahl abgezogen
    else:
        #wenn der regex nicht greift oder die zahl zu niedrig/hoch ist, wird die Abfrage wiederholt
        print(u"Bitte eine der Zahlen angeben!")
        return playerround(player)

#Die einzelnen verschiedenen möglichen Skills werden hier abgehandelt
def minigames(player, mob_obj, choice):
    #Die minigames der Fähigkeiten returnen einen Schadenswert, Heilwert und mögliche Statusveränderungen
    #wie in der Gegnerrunde setze ich alle Werte, die returned werden, erstmal auf 0/leerer String
    dmg, heal, statuschange=0,0,""
    #Wenn sich für Rhetorik entschieden wurde, muss einer der möglichen Zaubersprüche mithilfe von randint ausgewählt werden
    if choice in [u'Suggerieren', u"Überzeugen", u"Rede halten"]:
        base_strings = [
        u"Ich werde eine Mauer bauen, und ich werde ein fremdes Volk dazu bringen, zu zahlen",
        u"Alle Menschen, die in meinem Land leben, sind stolz darauf zu sagen: Ich bin ein Lokaler",
        u"Der Tag mag kommen, da der Mut unseres Volkes erlischt. Doch dieser Tag ist noch fern",
        u"Ich habe einen Traum, dass sich eines Tages diese Nation erheben wird",
        u"Lege deine Schwerter und deinen Hass nieder. Wir sind alle eins",
        u"Derjenige, der frei von Schuld ist, soll den ersten Stein werfen",
        u"Wir sollten nicht gegeneinander, sondern miteinander arbeiten",
        u"Vertrau mir wenn ich dir sage, man kann nicht jedem vertrauen",
        u"Auge um Auge und die ganze Welt wird blind sein",
        u"Du bereust es jetzt schon dich mit mir angelegt zu haben",
        u"Wahrhaft siegt wer nicht streitet",
        u"Die Wahrheit kommt mit wenigen Worten aus"]
        aktueller_spruch=base_strings[random.randint(0, len(base_strings)-1)]
        scrambled_spruch=""
        #um das korrekte Abtippen der Rede schwer zu gestalten, wird zufällig pro Buchstabe space und casing verändert
        for buchstabe in aktueller_spruch:
            zufallszahl=random.randint(0,2)
            if zufallszahl == 0:
                scrambled_spruch = scrambled_spruch+buchstabe.lower()
            elif zufallszahl == 1:
                scrambled_spruch = scrambled_spruch+buchstabe.upper()
            elif zufallszahl == 2: 
                scrambled_spruch = scrambled_spruch+" "+buchstabe
        misc.write(u"Du spürst die Nervosität vor deiner Rede anschwellen. Sortiere deine Worte in die natürliche Ordnung, um dein Gegenüber zu überzeugen.")
        print(scrambled_spruch)
        user_said = input()
        if user_said.lower() == aktueller_spruch.lower(): #groß und kleinschreibung soll nicht relevant sein, was der Spieler nicht weiß
            print("Korrekt! Die Rede kommt optimal an!")
            #an dieser Stelle erfolgt die Berechnung, die Formeln der einzelnen Skills sind auch in dev/ideas beschrieben
            if choice ==u"Suggerieren":
                rand_base = random.randint(8,11)
                dmg = int((rand_base + player.rh - mob_obj.rh_def)/3)
                misc.write(u"Dein Gegenüber ist sehr ergriffen und verliert den Willen gegen dich zu kämpfen.\n{x} Willenskraft abgezogen!".format(x=dmg))                
            elif choice == u"Überzeugen":
                rand_base = random.randint(14,17)
                dmg = int((rand_base + (player.rh) - mob_obj.rh_def)/3)
                misc.write(u"Dein Gegenüber ist sehr ergriffen und verliert den Willen gegen dich zu kämpfen.\n{x} Willenskraft abgezogen!".format(x=dmg))
            elif choice == u"Rede halten":
                dmg = int((25 + (player.rh) - mob_obj.rh_def)/3)
                misc.write(u"Dein Gegenüber ist sehr ergriffen und verliert den Willen gegen dich zu kämpfen.\n{x} Willenskraft abgezogen!".format(x=dmg))
        else:
            print(u"Falsch! Die Rede kommt nicht optimal an")
            if choice ==u"Suggerieren":
                rand_base = random.randint(2,5)
                dmg = int((rand_base + player.rh - mob_obj.rh_def)/3)
                misc.write(u"Dein Gegenüber ist etwas ergriffen und verliert den Willen gegen dich zu kämpfen.\n{x} Willenskraft abgezogen!".format(x=dmg))         
            elif choice ==u"Überzeugen":
                rand_base = random.randint(8,11)
                dmg = int((rand_base + player.rh - mob_obj.rh_def)/3)
                misc.write(u"Dein Gegenüber ist etwas ergriffen und verliert den Willen gegen dich zu kämpfen.\n{x} Willenskraft abgezogen!".format(x=dmg))
            elif choice == u"Rede halten":
                rand_base = random.randint(16,20)
                dmg = int((rand_base + (player.rh) - mob_obj.rh_def)/3)
                misc.write(u"Dein Gegenüber ist etwas ergriffen und verliert den Willen gegen dich zu kämpfen.\n{x} Willenskraft abgezogen!".format(x=dmg))             
    #Die defensiven Rhetorikskills setzen den Spielerstatus um und verändern so in der Hauptkampffunktion temporär die stats
    elif choice == u'Intuition':
        misc.write(u"Deine Intution verrät dir einige Schwächen deines Gegenübers! Rhetorik, Schikane und Weisheit wurden ehöht!")
        statuschange="intuition"
    elif choice == u"Nachschlagen":
        misc.write(u"Deine Enzyklopädie verrät dir einige Schwächen deines Gegenübers! Rhetorik, Schikane und Weisheit wurden stark ehöht!")
        statuschange="nachschlagen"
    elif choice ==u"Wikipedia nutzen":
        misc.write(u"Wikipedia verrät dir einige Schwächen deines Gegenübers! Rhetorik, Schikane und Weisheit wurden extrem ehöht!")
        statuschange="wiki"
    #Die offensiven Schikane Skills:
    elif choice in [u"Trick-17", u"Tyrannisieren",u"Wutrede halten"]:       
        misc.write(u"Der König überlegt, welche Beleidigung am ehesten trifft...")
        #Wenn durch einen der def. Skills mehr Beleidigungen möglich sind, wird das hier hochgesetzt:
        if player.stat == "loaded4":
            anzahl = 8
        elif player.stat == "loaded8":
            anzahl = 12
        elif player.stat == "loaded12":
            anzahl = 16
        else:
            anzahl = 4
        temp_dict={}
        for num, elem in enumerate(range(anzahl)): 
            #Es wird bis zu der Anzahl der zu ladenden Beleidigungen mithilfe von range eine zufällige aus dem globalen dict oben ausgewählt
            #enumerate erlaubt, mitzuzählen, so dass die Nummer dem Spieler angezeigt werden kann
            num+=1
            rand_beleid = random.randint(1,30)
            print("["+str(num)+"] "+beleidigungs_dict[rand_beleid])
            temp_dict[num]=beleidigungs_dict[rand_beleid]
            #Nun wurde ein temporäres, zufälliges dict der Beleidigungen erstellt, und eine kann ausgesucht werden
        print(u"Welche möchtest du wählen? (Bitte eine der Zahlen angeben)")
        user_said=input()
        user_said=user_said.replace(" ", "")
        if re.match("(?<!\S)\d(?!\S)*", user_said) and int(user_said)!=0 and int(user_said)<=len(temp_dict):
            beleid = temp_dict[int(user_said)]   
            misc.write(u"{x} ausgewählt!".format(x=beleid))
            #Diese darf nur eine mehrere Zahlen haben und nicht höher als das erstellte dictionairy sein
        else: 
            beleid = "" #Wenn keine gültige Eingabe getroffen wird, wird es als ungültig gewertet und nicht wiederholt sondern:
        if beleid == "":
            misc.write(u"Das war keine gültige Zahl - du verhaspelst dich komplett")
            misc.write(u"Eine peinliche Stille erfüllt den Raum . . .")
        elif choice == u"Trick-17":
            #Nun kann mit der ausgewählten Beleidigung und der Skillstufe berechnet werden, wie viel Schaden gemacht wird
            if beleid in mob_obj.effective_sc:
                misc.write("{x}!!! Das sitzt richtig tief!".format(x=beleid))
                dmg = int((player.sc - mob_obj.sc_def) / 2)
                misc.write(u"Dein Gegenüber verliert fast den ganzen Kampfgeist... {x} Willenskraft abgezogen!".format(x=dmg))
            #mitteleffektiv/normal
            elif beleid in mob_obj.okeffective_sc:
                misc.write("{x}!!! Das sitzt.".format(x=beleid))
                dmg = int((player.sc - mob_obj.sc_def) / 3)
                misc.write(u"Dein Gegenüber verliert etwas Kampfgeist... {x} Willenskraft abgezogen!".format(x=dmg))
            #uneffektiv:
            elif beleid in mob_obj.noneffective_sc:
                misc.write("{x}!!! Das stört wohl garnicht....".format(x=beleid))
                misc.write(u"Dein Gegenüber verliert gar keinen Kampfgeist")
                #Schaden bleibt 0, wenn es nich sitzt
            else:
                log("Die Beleidigung wurde nirgendswo beim Gegner gefunden", "error")
        elif choice == u"Tyrannisieren":
            #Effektiv:
            if beleid in mob_obj.effective_sc:
                misc.write("{x}!!! Das sitzt richtig tief!".format(x=beleid))
                dmg = int((player.sc - mob_obj.sc_def) / 1.5)
                misc.write(u"Dein Gegenüber verliert fast den ganzen Kampfgeist... {x} Willenskraft abgezogen!".format(x=dmg))
            #mitteleffektiv/normal
            elif beleid in mob_obj.okeffective_sc:
                misc.write("{x}!!! Das sitzt.".format(x=beleid))
                dmg = int((player.sc - mob_obj.sc_def) / 2)
                misc.write(u"Dein Gegenüber verliert etwas Kampfgeist... {x} Willenskraft abgezogen!".format(x=dmg))
            #uneffektiv:
            elif beleid in mob_obj.noneffective_sc:
                misc.write("{x}!!! Das stört wohl garnicht....".format(x=beleid))
                misc.write(u"Dein Gegenüber verliert gar keinen Kampfgeist")
            else:
                print("ERROR: Die Beleidigung wurde nirgendswo beim Gegner gefunden")
        elif choice ==u"Wutrede halten":
            #Effektiv:
            if beleid in mob_obj.effective_sc:
                misc.write("{x}!!! Das sitzt richtig tief!".format(x=choice))
                dmg = (player.sc - mob_obj.sc_def)
                misc.write(u"Dein Gegenüber verliert fast den ganzen Kampfgeist... {x} Willenskraft abgezogen!".format(x=dmg))
            #mitteleffektiv/normal
            elif beleid in mob_obj.okeffective_sc:
                misc.write("{x}!!! Das sitzt.".format(x=choice))
                dmg = int((player.sc - mob_obj.sc_def) / 1.5)
                misc.write(u"Dein Gegenüber verliert etwas Kampfgeist... {x} Willenskraft abgezogen!".format(x=dmg))
            #uneffektiv:
            elif beleid in mob_obj.noneffective_sc:
                misc.write("{x}!!! Das stört wohl garnicht....".format(x=choice))
                misc.write(u"Dein Gegenüber verliert gar keinen Kampfgeist")
            else:  #wenn nichts davon der Fall ist, ist in den Mob-Daten wahrscheinlich eine Zahl vergessen worden
                log("Die Beleidigung wurde nirgendswo beim Gegner gefunden", "error")
    #Die defensiven Schikane Skills, setzen jeweils einen Status welcher die offensiven Versionen stärkt und heilt
    elif choice == u"32-Heb-Auf":
        statuschange="loaded4"
        heal = 4
        misc.write(u"\"Kennst du 32-Heb-auf?\" fragt der König und lässt sein Kartendeck lässig fallen.")
        misc.write(u"Die nächste Runde fallen dir 4 Beleidigungen mehr ein, und du heilst dich um 4 Leben!")
    elif choice == u"Posieren":
        statuschange="loaded8"
        heal = 6
        misc.write(u"Der König nimmt eine lässige Pose ein und lässt die Schultern locker.")
        misc.write(u"Die nächste Runde fallen dir 8 Beleidigungen mehr ein, und du heilst dich um 6 Leben!")
    elif choice == u"Sibirischer Doppelbluff":
        statuschange="loaded12"
        heal = 8
        misc.write(u"Der König wendet den sagenumwobenen, galaktischen, sibirischen Doppelbluff an. Das sah niemand kommen!")
        misc.write(u"Die nächste Runde fallen dir 12 Beleidigungen mehr ein, und du heilst dich um 8 Leben!")
    #die offensiven Weisheit Skills:
    elif choice in [u"Demoralis", u"Anima Vincere", u"Leckerli hergeben"]:
        #In einem zufälligen Zauberspruch müssen zufällige Vokale ausgetauscht werden
        #Ob das richtig gemacht wird, wird mithilfe der replace funktion überprüft
        base_strings = [
        u"Sei geistig klein, dein Wille mein",
        u"Erst eins, dann zwei, dann wirst du frei",
        u"Keine Trille Eigenwille",
        u"Ganz ohne Geist, komplett ohne Sinn",
        u"Du bist mein, mein Schatz",
        u"Baldig hast du keine Lust mehr",
        u"Aus dem Blick, aus dem Kampf",
        u"Wingardium Leviosa du Poser",
        u"Schere Stein Papier du bist nicht mehr hier",
        u"Probiere mal Alt F Vier"]
        aktueller_spruch=base_strings[random.randint(0, len(base_strings)-1)]
        vokale=["a", "i", "e", "o"]
        rand_voc_1 = vokale[random.randint(0,3)]
        rand_voc_2 = vokale[random.randint(0,3)]
        #Wenn es der gleiche Vokal ist, wird sooft 'neu gewürfelt' bis es ein anderer ist, da Vokale mit sich selbst austauschen langweilig wäre
        while rand_voc_2 == rand_voc_1:
            rand_voc_2 = vokale[random.randint(0,3)]
        misc.write("Der König lenkt das Chaos der Welt zu seinen Gunsten und wirkt Magie!")
        misc.write("Das Chaos verlangt, alle {x} zu {y} werden zu lassen.".format(x=rand_voc_1, y=rand_voc_2))
        print("Sage die folgende Formel auf, aber beachte den Willen des Chaos:\n{y}".format(y=aktueller_spruch))
        user_said = input()
        if user_said.lower() == aktueller_spruch.lower().replace(rand_voc_1, rand_voc_2):
            misc.write("Der Spruch wurde richtig aufgesagt!")
            if choice == u"Demoralis":
                dmg = int((int(player.wh*1.2) - mob_obj.wh_def) / 2) #die ints sind wie immer zum runterrunden
            elif choice ==u"Anima Vincere":
                dmg = int((int(player.wh*1.4) - mob_obj.wh_def) / 2)
            elif choice ==u"Leckerli hergeben":
                dmg = int((int(player.wh*1.5) - mob_obj.wh_def) / 2)
        else:
            misc.write("Der Spruch ging wohl daneben...")
            misc.write("Es wäre gewesen: {x}".format(x=aktueller_spruch.lower().replace(rand_voc_1, rand_voc_2)))
            if choice == u"Demoralis":
                pre_dmg = (int(player.wh*0.5) - mob_obj.wh_def)
                if pre_dmg >= 1: #falls gegner hohe whdef haben , soll nicht durch 0 geteilt werden, daher wird jeweils vorher überprüft ob es höher ist
                    dmg = int(pre_dmg / 2)
            elif choice ==u"Anima Vincere":
                pre_dmg = (int(player.wh*0.7) - mob_obj.wh_def)
                if pre_dmg >= 1: #falls gegner hohe whdef haben , soll nicht durch 0 geteilt werden
                    dmg = int(pre_dmg / 2)                
            elif choice ==u"Leckerlie hergeben":
                pre_dmg = (int(player.wh) - mob_obj.wh_def)
                if pre_dmg >= 1: #falls gegner hohe whdef haben , soll nicht durch 0 geteilt werden
                    dmg = int(pre_dmg / 2)
        misc.write(u"Die Willenskraft des Gegenübers wurde dezimiert! {x} Willenskraft abgezogen!".format(x=dmg))
    #Die defensiven Weisheit Skills:
    elif choice ==u"Abrakapflaster":
        heal = int(player.hpmax * 0.3)#heilt 30 % des Gesamtlebens
        misc.write(u"Du wirkst Abrakaplaster. {x} Leben geheilt!".format(x=heal))
    elif choice ==u"3xSchwarzer-1.-Hilfe-Kasten":
        heal = int(player.hpmax * 0.5)#heilt 50 % des Gesamtlebens
        misc.write(u"Du nutzt den 1. Hilfe Kasten - magisch! {x} Leben geheilt!".format(x=heal))
    elif choice ==u"Simsala-Wundheilzauber wirken":
        heal = int(player.hpmax * 0.6)#heilt 60 % des Gesamtlebens
        misc.write(u"Du wirkst einen echten Wundheilzauber! {x} Leben geheilt!".format(x=heal))
    else:  
        log("Unbekannter Skill " + str(choice) + " " + str(type(choice)), "error")

    return [dmg, heal, statuschange]

#Stufenanstiegsfunktion
def check_lvl_up(player):
    #vergleicht lvl und erfahrung nach dem kampf, so dass bei ausreichender Erfahrung die lvlup funktion aufgerufen wird
    #Beim ersten LvlUp folgt eine kurze Erläuterung der Fähigkeiten in diesem Spiel
    if player.lvl == 9 and player.exp >= 800:
        misc.write("Level Up! Du bist nun LvL 10!")
        do_lvl_up(player)
    elif player.lvl == 8 and player.exp >= 700:
        misc.write("Level Up! Du bist nun LvL 9!")
        do_lvl_up(player)
    elif player.lvl == 7 and player.exp >= 600:
        misc.write("Level Up! Du bist nun LvL 8!")
        do_lvl_up(player)
    elif player.lvl == 6 and player.exp >= 500:
        misc.write("Level Up! Du bist nun LvL 7!")
        do_lvl_up(player)
    elif player.lvl == 5 and player.exp >= 400:
        misc.write("Level Up! Du bist nun LvL 6!")
        do_lvl_up(player)
    elif player.lvl == 4 and player.exp >= 300:
        misc.write("Level Up! Du bist nun LvL 5!")
        do_lvl_up(player)
    elif player.lvl == 3 and player.exp >= 200:
        misc.write("Level Up! Du bist nun LvL 4!")
        do_lvl_up(player)
    elif player.lvl == 2 and player.exp >= 100:
        misc.write("Level Up! Du bist nun LvL 3!")
        do_lvl_up(player)
    elif player.lvl == 1 and player.exp >=50:
        misc.write("Level Up! Du bist nun LvL 2!")
        misc.write(u"Es folgt eine kurze Einleitung zu den erlernbaren Fähigkeiten.\nRhetorik:\nDeine Wortgewandheit. Hilft dir, deine Feinde zu überzeugen, sodass sie dir wohlgesonnen sind. Für Redner - Erfordert das Erkennen von Wortgrenzen!")
        misc.write(u"Schikane:\nDeine Fähigkeit andere niederzumachen und so deinen Willen durchzusetzen! Für Knobler! Erfordert seine Gegner und Pointen gut zu kennen!")
        misc.write(u"Weisheit:\nDeine Fähigkeit die natürliche Ordnung der Welt durch Magie zu ändern! Für Gehirnakrobaten! Erfordert Buchstaben geschickt auszuwechseln!")
        misc.write(u"Was davon kann König {x} besonders gut? (Eins auswählen)".format(x=player.name[:1].upper()+player.name[1:]))
        print("[1] Rhetorik [2] Schikane [3] Weisheit")
        user_said=input()
        #die Auswahl muss in einen int wert umwandelbar sein, weshalb Spaces entfernt werden
        user_said=user_said.replace(" ", "")
        if re.match("(?<!\S)\d(?!\S)", user_said) and int(user_said)!=0 and int(user_said)<=3:
            if int(user_said)==1:
                player.rh = player.rh+6
                misc.write(u"Rhetorik! Exzellente Wahl! Deine Reden werden alle Feinde fernhalten!\nDeine Rhetorik Fähigkeit stieg um 6!")
            elif int(user_said)==2:
                player.sc = player.sc+6
                misc.write(u"Schikane! Vortreffliche Wahl! Deine Beleidigungen werden deine Feinde in die Flucht schlagen!\nDeine Schikane Fähigkeit stieg um 6!")
            elif int(user_said)==3:
                player.wh = player.wh+6
                misc.write(u"Weisheit! Vorzügliche Wahl! Deine Magie wird deine Feinde in die Knie zwingen! \nDeine Weisheit Fähigkeit stieg um 6!")
            do_lvl_up(player)
        else:
            misc.write(u"Bitte eine der Zahlen auswählen!")
            return check_lvl_up(player)
    else:
        print("Du hast nun {x} EXP\n".format(x=player.exp))

def do_lvl_up(player): 
    #diese Funktion führt das Level Up aus, in dem es Werte in zufälliger Höhe ansteigen lässt
    #zusätzlich darf der Spieler noch 3 Punkte selber verteilen
    player.lvl = player.lvl+1
    hp_inc = random.randint(3,6)
    rh_inc = random.randint(2,4)
    sc_inc = random.randint(2,4)
    wh_inc = random.randint(2,4)
    print(u"Dein Leben stieg um {x}!".format(x=hp_inc))
    print(u"Deine Rhetorik Fähigkeiten stiegen um {x}!".format(x=rh_inc))
    print(u"Deine Schikane Fähigkeiten stiegen um {x}!".format(x=sc_inc))
    print(u"Deine Weisheit Fähigkeiten stiegen um {x}!".format(x=wh_inc))
    misc.write("")
    player.hpmax = player.hpmax + hp_inc
    player.hpcur = player.hpcur + hp_inc #Die akute HP soll auch gesteigert werden
    player.rh = player.rh + rh_inc
    player.sc = player.sc + sc_inc
    player.wh = player.wh + wh_inc
    #Die neuen Werte dem Spieler anzeigen:
    misc.write(u"Du hast jetzt {q}\\{t} Leben! Außerdem hast du {x} Rhetorik, {y} Schikane und {z} Weisheit.\n\nDu kannst nun 3 Skillpunkte verteilen!\nVersuche in Rhetorik, Schikane oder Weisheit jeweils 15/25/35 Punkte zu erreichen, um neue Fähigkeiten freizuschalten.".format(x=player.rh, y=player.sc, z=player.wh, q=player.hpcur, t=player.hpmax))
    for skillpunkt in range(3):
        print(u"[1] Ich möchte einen Skillpunkt in 'Rhetorik' investieren.")
        print(u"[2] Ich möchte einen Skillpunkt in 'Schikane' investieren.")
        print(u"[3] Ich möchte einen Skillpunkt in 'Weisheit' investieren.")
        print(u"[4] Ich möchte einen Skillpunkt in 'Leben' investieren.")
        #Als nächstes kann der Spieler eine entsprechende Zahl angeben, was mit einem regEx geprüft wird, der nur einstellige Zahlen erlaubt
        #außerdem wird überprüft, ob die Zahl nicht höher als die Möglichkeiten war
        user_said=input()
        #die auswahl muss in einen int wert umwandelbar sein, weshalb spaces entfernt werden
        user_said=user_said.replace(" ", "")
        if re.match("(?<!\S)\d(?!\S)", user_said) and int(user_said)!=0 and int(user_said)<=4:
            if int(user_said) == 4:
                misc.write("Dein Leben ist um einen Punkt gestiegen!")
                player.hpmax = player.hpmax + 1
                player.hpcur = player.hpcur + 1
            elif int(user_said) == 1:
                misc.write("Deine Rhetorik ist um einen Punkt gestiegen!")
                player.rh = player.rh + 1
            elif int(user_said) == 2:
                misc.write("Deine Schikane ist um einen Punkt gestiegen!")
                player.sc = player.sc + 1
            elif int(user_said) == 3:
                misc.write("Deine Weisheit ist um einen Punkt gestiegen!")
                player.wh = player.wh + +1
        else:                
            misc.write(u"Bitte eine Zahl zwischen 1 und 4 angeben.")

    #Wenn keiner der fähigkeiten auf 15 ist, kann der Spieler nicht kämpfen, daher wird das hier überprüft und notfalls wiederholt
    if player.rh <=14 and player.wh <=14 and player.sc<=14:
        misc.write("Versuche, mindestens einen Wert auf 15 zu bringen - wir wiederholen das Lvl-Up mal.")
        player.hpmax = 15
        player.hpcur=15
        player.rh=6
        player.wh=6
        player.sc=6 #man kann das theoretisch so lange ausnutzen bis man das optimale erste Level up hat
        do_lvl_up(player)
    #Durch die erhöhten Werte könnte der Spieler neue Skills erlangt haben, daher überprüft lvlup auch die skills
    check_skill_up(player)

def check_skill_up(player):
    if player.rh >= 35: #Die benötigten Werte für verbesserte Fähigkeiten sind immer 15/25/35
        if player.rh_off == 2:
        #hiermit kann ich überprüfen, ob die Fähigkeit neu erlangt wurde oder ob der Spieler vorher schon soviel rh hatte
        #Wenn es neu erreicht wurde, sollen Hinweise zur Benutzung geprintet werden
            misc.write(u"Du hast \"Reden halten\n (besser als Überzeugen!) und \"Wikipedia\" (besser als Nachschlagen!) als Rhetorikfähigkeiten erlernt!\nTippe 'inventar', 'menü' oder deinen Namen, um eine Übersicht deiner Fähigkeiten zu erhalten.\nTippe den Namen einer Fähigkeit, um mehr über sie herauszufinden.")
            player.rh_off=3
            player.rh_def=3
    elif player.rh >= 25:
        if player.rh_off == 1:
            misc.write(u"Du hast \"Überzeugen\" (besser als Suggerieren!) und \"Nachschlagen\" (besser als Intuition!) als Rhetorikfähigkeiten erlernt!\nTippe 'inventar', 'menü' oder deinen Namen, um eine Übersicht deiner Fähigkeiten zu erhalten.\nTippe den Namen einer Fähigkeit, um mehr über sie herauszufinden.")
            player.rh_off=2
            player.rh_def=2
    elif player.rh >= 15:
        if player.rh_off == 0:
            misc.write(u"Du hast \"Suggerieren\" und \"Intuition\" als Rhetorikfähigkeiten erlernt!\nTippe 'inventar', 'menü' oder deinen Namen, um eine Übersicht deiner Fähigkeiten zu erhalten.\nTippe den Namen einer Fähigkeit, um mehr über sie herauszufinden.")
            player.rh_off=1
            player.rh_def=1
    if player.sc >= 35:
        if player.sc_off == 2:
            misc.write(u"Du hast \"Wutrede halten\" (besser als Tyrannisieren!) und \"Sibirischer Doppelbluff\" (besser als Posieren!) als Schikanefähigkeit erlernt!\nTippe 'inventar', 'menü' oder deinen Namen, um eine Übersicht deiner Fähigkeiten zu erhalten.\nTippe den Namen einer Fähigkeit, um mehr über sie herauszufinden.")
            player.sc_off=3
            player.sc_def=3
    elif player.sc >= 25:
        if player.sc_off == 1:
            misc.write(u"Du hast \"Tyrannisieren\" (besser als Trick-17!) und \"Posieren\" (besser als 32-Heb-Auf!) als Schikanefähigkeit erlernt!\nTippe 'inventar', 'menü' oder deinen Namen, um eine Übersicht deiner Fähigkeiten zu erhalten.\nTippe den Namen einer Fähigkeit, um mehr über sie herauszufinden.")
            player.sc_off=2
            player.sc_def=2
    elif player.sc >= 15:
        if player.sc_off == 0:
            misc.write(u"Du hast \"Trick-17\" und \"32-Heb-Auf\" als Schikanefähigkeit erlernt!\nTippe 'inventar', 'menü' oder deinen Namen, um eine Übersicht deiner Fähigkeiten zu erhalten.\nTippe den Namen einer Fähigkeit, um mehr über sie herauszufinden.")
            player.sc_off=1
            player.sc_def=1
    if player.wh >= 35:
        if player.wh_off == 2:
            misc.write(u"Du hast \"Leckerli\" (besser als Anima Vincere!) und \"Simsala-Wundheilzauber\" (besser als 3xSchwarzer-1.-Hilfe-Kasten!) als Weisheitsfähigkeiten erlernt!\nTippe 'inventar', 'menü' oder deinen Namen, um eine Übersicht deiner Fähigkeiten zu erhalten.\nTippe den Namen einer Fähigkeit, um mehr über sie herauszufinden.")
            player.wh_off=3
            player.wh_def=3
    elif player.wh >= 25:
        if player.wh_off == 1:
            misc.write(u"Du hast \"Anima Vincere\" (besser als Demoralis!) und \"3xSchwarzer-1.-Hilfe-Kasten\" (besser als Abrakapflaster!) als Weisheitsfähigkeiten erlernt!\nTippe 'inventar', 'menü' oder deinen Namen, um eine Übersicht deiner Fähigkeiten zu erhalten.\nTippe den Namen einer Fähigkeit, um mehr über sie herauszufinden.")
            player.wh_off=2
            player.wh_def=2
    elif player.wh >= 15:
        if player.wh_off == 0: #hiermit kann ich überprüfen, ob die fähigkeit neu erlangt wird, um hinweise zur nutzung zu geben
            misc.write(u"Du hast \"Demoralis\" und \"Abrakapflaster\" als Weisheitsfähigkeiten erlernt!\nTippe 'inventar', 'menü' oder deinen Namen, um eine Übersicht deiner Fähigkeiten zu erhalten.\nTippe den Namen einer Fähigkeit, um mehr über sie herauszufinden.")
            player.wh_off=1
            player.wh_def=1