'''
    This module provides developer tools which can be switched on for development.
    Always remember to switch them off before releases!!!!
'''
import datetime

# Switch tools on/ off here
developer_tools_on = False

# Select debugging output
log_levels = {
    "verbose": 0,
    "trace": 1,
    "info": 2,
    "debug": 3,
    "warning": 4,
    "error": 5
}
log_level = log_levels["debug"]


def log(message, level="debug"):
    if not developer_tools_on:
        pass
    elif level not in log_levels.keys():
        log("Tried logging to non-existent level \"" + level + "\": " + message)
    elif developer_tools_on and log_levels[level] >= log_level:
        print(datetime.datetime.now().isoformat() +
              " [ " + level.upper()[:7].ljust(7) + " ] " + message)

def set_log_level(level):
    global developer_tools_on
    global log_level
    if type(level) == type(True):
        developer_tools_on = level
    elif level not in log_levels.keys():
        log("Tried setting log level to non-existent level \"" + level + "\"")
    else:
        log_level = log_levels[level]


if __name__ == '__main__':
    log("Logging to info level", "info")
    log("Logging to debug level", "debug")
    log("Logging to error level", "error")

    log_level = log_levels["verbose"]
    log("Logging to info level", "info")
    log("Logging to verbose level", "verbose")

    log("Should fail", "foobar")

    developer_tools_on = False
    log("Should not be displayed")
